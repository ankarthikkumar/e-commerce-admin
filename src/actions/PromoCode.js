import axios from "axios";
import { constant } from '../constants';

export const savePromoCode = (request) => {
    return new Promise((resolve, reject) => {
        axios.post(`${constant.API_ENDPOINT}/promo_code/`,request)
        .then((response) => {
            console.log("savePromoCode save",response.data);
            resolve(response.data)
        })
        .catch((error) => reject(error))
    })
}

export const getPromoCode = () => {
    return new Promise((resolve, reject) => {
        axios.get(`${constant.API_ENDPOINT}/promo_code/`)
        .then((response) => {
            console.log("savePromoCode save",response.data);
            resolve(response.data)
        })
        .catch((error) => reject(error))
    })
}

export const singlePromoCode = (id) => {
    return new Promise((resolve, reject) => {
        axios.get(`${constant.API_ENDPOINT}/promo_code/`+id)
        .then((response) => {
            console.log("singlePromoCode",response.data);
            resolve(response.data)
        })
        .catch((error) => reject(error))
    })
}

export const updatePromocode = (id,request) => {
    return new Promise((resolve, reject) => {
        axios.put(`${constant.API_ENDPOINT}/promo_code/`+id,request)
        .then((response) => {
            console.log("update save",response.data);
            resolve(response.data)
        })
        .catch((error) => reject(error))
    })
}
