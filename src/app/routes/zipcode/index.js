import React from 'react';
import ZipCodeList from "components/Zipcode/ZipCodeList";

import TextField from 'material-ui/TextField';
import Button from 'material-ui/Button';
import Switch from 'material-ui/Switch';
class Zipcode extends React.Component {

    onOptionMenuSelect = event => {
        this.setState({ menuState: true, anchorEl: event.currentTarget });
    };
    handleRequestClose = () => {
        this.setState({ menuState: false });
    };

    constructor() {
        super();
        this.state = {
            anchorEl: undefined,
            menuState: false,
        }
    }

    render() {
        const { anchorEl, menuState } = this.state;
        return (
            <div className="app-wrapper">
            <div style={{width: '100%', height: '100%'}}>
            <ZipCodeList />
            </div>
            </div>
                )
    }
}
export default Zipcode;