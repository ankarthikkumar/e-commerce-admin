import React from 'react';
import CardBox from "components/CardBox/index";
import Button from 'material-ui/Button';
import Input, { InputLabel } from 'material-ui/Input';
import { MenuItem } from 'material-ui/Menu';
import { FormControl, FormHelperText } from 'material-ui/Form';
import Select from 'material-ui/Select'
import SweetAlert from 'react-bootstrap-sweetalert';
import { saveOffer } from '../../actions/Offer'
import { singleOffer } from '../../actions/Offer'
import { updateOffer } from '../../actions/Offer'
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';
import {getProduct} from '../../actions/Product';
import { getSubcategoryByCategory } from '../../actions/Category'
import { getCategory } from '../../actions/Category';
import { awsFileUpload } from '../../actions/fileUpload';
import { CircularProgress } from 'material-ui';
class AddOffer extends React.Component {

    onOptionMenuSelect = event => {
        this.setState({ menuState: true, anchorEl: event.currentTarget });
    };
    handleRequestClose = () => {
        this.setState({ menuState: false });
    };

    handleInputChange = (event) => {
        const value = event.target.value;
        const name = event.target.name;

        this.setState({ [name]: value });
    }
    handleCategoryStatus = (event) => {
        this.setState({ CategoryStatus: !this.state.CategoryStatus });
    }
    onConfirm = (event) => {
        this.setState({ success: false });
        console.log(this.props.props)
        this.props.props.history.push('/app/offer/list');
    }


    async componentDidMount() {
        console.log(this.props.props);
        var id = this.props.props.location.search;
        var current_id = id.split('&id=')[1];
        console.log("current_id", current_id);
        if (current_id != undefined) {
            var response = await singleOffer(current_id);
            var offerData = response.data[0];
            console.log("offerData", offerData)
            const sub_category = await getSubcategoryByCategory(offerData.category);
      
            this.setState({
                offerName : offerData.offerName,
                offerValue : offerData.offerValue,
                offerDescription :offerData.offerDescription,
                startDate :  offerData.startDate.split('T')[0],   
                endDate :   offerData.endDate.split('T')[0],
                category :   offerData.category,
                sub_category :   offerData.sub_category,
                image :   offerData.offerImage,
                is_edit:true,
                offer_id:offerData._id,
                subCategoryList: sub_category.data
            })

        }
         const category = await getCategory();
        const categoryData = category.data;

        this.setState({ categoryList: categoryData });

        const product = await getProduct();
        const productData = product.data;
        console.log(productData);
        this.setState({
            data: productData
        });
        console.log("data ",this.state.data);        
    }


    save = async () => {

        const data = Object.assign({}, { 
            offerName : this.state.offerName,
            offerValue : this.state.offerValue,
            offerDescription :this.state.offerDescription,
            startDate :  this.state.startDate,   
            endDate :   this.state.endDate,
            category :   this.state.category,
            sub_category :   this.state.sub_category,
            offerImage :   this.state.image,
        })
        console.log(data);
        if(this.state.image==""){
            alert("please add offer image");
            return false;
        }else{
      
        var response= "";
        if(this.state.is_edit==false){
            response = await saveOffer(data);
        }else{
            response = await updateOffer(this.state.offer_id ,data);
        }
        console.log("response", response);
        if (response.status == true) {
           this.setState({
               success:true,
               alertText:response.message
           })
        }
        }

    }
    handleInputCategoryChange = async (event) => {
        const value = event.target.value;
        const name = event.target.name;

        this.setState({
            [name]: value
        });
        const sub_category = await getSubcategoryByCategory(value);
        console.log(sub_category.data);
        this.setState({
            subCategoryList: sub_category.data
        });
    }

    constructor() {
        super();
        this.state = {
            anchorEl: undefined,
            menuState: false,
            offerName: '',
            offerDescription: '',
            CouponCode: '',
            type: '',
            startDate: '',
            endDate: '',
            hasError:false,
            data:[],
            categoryList :[],
            subCategoryList :[],
            offerValue:'',
            offer_id:'',
            is_edit:false,
            success:false,
            alertText:'',
            category:'',
            sub_category:'ALL',
            image:'',
            loading:false,
            is_image:false,
            is_upload:false,
            dummy_image:'https://www.premierrollssouth.com/wp-content/uploads/bargain-453486_960_720.png'
            // categoryDescription: '',

        }
    }
    handleselectedFile = async (event) => {
        this.setState({
            is_upload: true,
            loading: true,

        })
        const data = new FormData()
        data.append('media', event.target.files[0])
        const fileupload = await awsFileUpload(data);
        this.setState({
            is_upload: false,
            loading: false,
            image: fileupload.file,
            is_image:true
        })
    }

    render() {
        // console.log("simple fun",this.props);
        // alert("Add Category")
        const { anchorEl, menuState,hasError,data ,loading } = this.state;
        return (
            <div className="dashboard animated slideInUpTiny animation-duration-3">
                <h2>Offer</h2>


                <CardBox styleName="col-lg-12" childrenStyle="d-flex" heading="Add Offer ">
                    <ValidatorForm
                        ref="form"
                        className="col-lg-12"
                        onSubmit={this.save}
                        onError={errors => console.log(errors)}
                    >


                        <div className="col-md-6 col-12">
                            <TextValidator
                                id="name"
                                label="Offer Name"
                                value={this.state.offerName}
                                name="offerName"
                                onChange={this.handleInputChange}
                                fullWidth
                                validators={['required']}
                                errorMessages={['this field is required']}
                            />
                        </div>
                        <div className="col-md-6 col-12">
                            <TextValidator
                                id="name"
                                label="Offer Value"
                                value={this.state.offerValue}
                                name="offerValue"
                                onChange={this.handleInputChange}
                                fullWidth
                                validators={['required']}
                                errorMessages={['this field is required']}
                            />
                        </div>
                        <div className="col-md-6 col-12">
                        <div className="form-group">
                                        <InputLabel htmlFor="age-helper">Super Category </InputLabel>
                                        <Select
                                            value={this.state.category}
                                            fullWidth
                                            name="category"
                                            onChange={this.handleInputCategoryChange}
                                            input={<Input id="age-helper" />}
                                            validators={['required']}
                                            errorMessages={['Super Category field is required']}
                                        >
                                            {this.state.categoryList.map(data => {


                                                return (
                                                    <MenuItem value={data._id}>{data.categoryName}</MenuItem>
                                                )
                                            })}
                                        </Select>
                                    </div>
                        </div>
                        <div className="col-md-6 col-12">
                        <div className="form-group">
                                        <InputLabel htmlFor="sub_cat">Sub Category </InputLabel>
                                        <Select
                                            value={this.state.sub_category}
                                            fullWidth
                                            name="sub_category"
                                            onChange={this.handleInputChange}
                                            input={<Input id="age-helper" />}
                                        >  <MenuItem value="ALL">ALL</MenuItem>
                                            {this.state.subCategoryList.map(data => {

                                                return (
                                                    <MenuItem value={data.subCategoryName}>{data.subCategoryName}</MenuItem>
                                                )
                                            })}
                                        </Select>
                                    </div>
                        </div>
                        <div className="col-md-6 col-12">
                            <TextValidator
                                id="multiline-static"
                                label="Description"
                                multiline
                                rows="4"
                                defaultValue=""
                                value={this.state.offerDescription}
                                name="offerDescription"
                                onChange={this.handleInputChange}
                                fullWidth
                            />
                        </div>
                        <br />
                        <div className="col-md-6 col-12">
                        <label>Date Start</label>
                            <TextValidator
                                id="dateStart"
                                type="date"
                                value={this.state.startDate}
                                name="startDate"
                                onChange={this.handleInputChange}
                                fullWidth
                                validators={['required']}
                                errorMessages={['this field is required']}
                            />
                        </div>

                        <div className="col-md-6 col-12">
                            <label>End Date </label>
                            <TextValidator
                                id="endDate"
                                type="date"
                                value={this.state.endDate}
                                name="endDate"
                                onChange={this.handleInputChange}
                                fullWidth
                                validators={['required']}
                                errorMessages={['this field is required']}
                            />
                        </div>


                        <div className="col-sm-12 col-md-12">
                            <br />
                            <br />
                            <h2>Offer Image</h2>
                            <div className="">
                                <div className="">
                                    <input type="file" accept="image/*" id="image5" name="image5" className="btn btn-sm image_input_style"
                                    />
    
                                    <img className="image" src={this.state.image!=""?this.state.image:this.state.dummy_image}  width="100px"/><br />
                                    {loading && <CircularProgress className="size-30"/>}
                                    <label for="image5" className="custom_file_input">
                                        <span className="pleaseWaitOption" id="photoProof_icon" >
                                            <b><i className="fa fa-spinner fa-spin"></i> &nbsp;Please wait...</b></span>
                                    </label>
                                    <br />
                                    <Button
                                        variant="raised"
                                        component="label"
                                        color="primary"
                                        className="btn btn-primary"
                                    >
                                        Upload File
                                <input type="file" name="image"
                                            onChange={this.handleselectedFile}
                                            style={{ display: "none" }}
                                        />
                                    </Button>
                                </div>
                            </div>
                        </div>
                    
                        <div className="col-md-6 col-12">
                            <Button variant="raised" type="submit" color="primary" className={this.state.is_upload == true ? "hidden" : "jr-btn  text-white"} >Save Offer</Button>
                        </div>
                    </ValidatorForm>
                </CardBox>
                <SweetAlert show={this.state.success} success title="Success!" onConfirm={this.onConfirm}>
                        {this.state.alertText}
                    </SweetAlert>
            </div>
        )
    }
}
export default AddOffer;