import React from 'react';
import {Redirect, Route, Switch} from 'react-router-dom';

import Add from './routes/Add'
import List from './routes/List'

const Offer = ({match}) => (
    
   
    <div className="app-wrapper">
        <Switch>
          
            <Route path={`${match.url}/add`} component={Add}/>
            <Route path={`${match.url}/`} component={List}/>
           
        </Switch>
    </div>
);

export default Offer;