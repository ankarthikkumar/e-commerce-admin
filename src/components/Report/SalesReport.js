import React from 'react';
import Button from 'material-ui/Button';
import Input, { InputLabel } from 'material-ui/Input';
import TextField from 'material-ui/TextField';
import {Link, withRouter} from 'react-router-dom';
import { MenuItem } from 'material-ui/Menu';
import Dialog, { DialogActions, DialogContent, DialogContentText, DialogTitle, } from 'material-ui/Dialog';
import {
    Area,
    AreaChart,
    Bar,
    BarChart,
    Cell,
    Line,
    LineChart,
    Pie,
    PieChart,
    ResponsiveContainer,
    Tooltip,
    XAxis,
    YAxis,
    CartesianGrid,
    Brush
} from 'recharts';
import {
    announcementsNotification,
    appNotification,
    article,
    authors,
    chartData,
    lineChartData,
    marketingData,
    pieChartData
} from './data';
import SweetAlert from 'react-bootstrap-sweetalert';
import { getOrderAdmin,getOrder } from '../../actions/Order';
import {getProduct} from '../../actions/Product';
import ReportBox from 'components/ReportBox/index';
import Select from 'material-ui/Select';
import {getCategory} from '../../actions/Category';
import {reportStatus,reportProduct, reportCategory, reportDate,orderReport,reportData} from '../../actions/Report';
const COLORS = ['#0088FE', '#00C49F', '#FFBB28', '#FF8042'];
// import {ToastsContainer, ToastsStore} from 'react-toasts';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

class SalesReport extends React.Component {

    state = {
        orderData: [],
        histohour: '',
        histoday: '',
        histominute: '',
        success: false,
        admin: {},
        anchorEl: undefined,
        menuState: false,
        buyRate: '',
        sellRate: '',
        buyTransactionFee: '',
        sellTransactionFee: '',
        sendTransactionFee: '',
        stellarAddress: '',
        stellarSeed: '',
        stellarBalance: 0,
        fiatBalance: 0,
        currentRate: 0,
        userCount: 0,
        stripeKey: '',
        copied: false,
        copiedStellarSeed: false,
        open: false,
        fromDate: '',
        toDate:'',
        data:[],
        product:[],
        products:[],
        category:[],
        categoryData:[],
        orderReport:[],
        dateItems:[],
        monthData:[],
        weekData:[],
        yearData:[],
        status:null,
        total_amount:0,
        this_month:0,
        last_month:0,
        this_year:0,
        last_year:0,
        today:0,
        this_week:0,
        last_week:0,

    }
    handleDateChange = (date) => {
        this.setState({selectedDate: date});
    };

    async componentDidMount() {
        const order = await getOrderAdmin();
        const product = await getProduct();
        const report = await orderReport();
        const productData = product.data;
        console.log(productData);
        this.setState({
            data: productData,
            orderData :order.data,
            total_amount:0,
            orderReport:report.data,
            dateItems:report.dateItems,
            monthData:report.data.month,
            weekData:report.data.week,
            yearData:report.data.week,
        });
        const category= await getCategory();
        this.setState({
            categoryData: category.data
        });
        console.log("orderReport",this.state.orderReport.month);
        console.log("dateItems",this.state.dateItems);
        // for (let index = 0; index < (this.state.orderReport.month).length; index++) {
        //     const element = this.state.orderData.month[index];
        //     console.log("element",element);
            
        // }
        this.state.orderReport.month.map((item)=>{
            console.log("item",item._id)
            console.log("item._id.month",item._id.month)
            console.log("this.state.dateItems.current_month",this.state.dateItems.this_month)
            if(item._id.month==this.state.dateItems.this_month){
                console.log("true",item.sum)
                this.setState({
                    this_month:item.sum
                })
            }
            if(item._id.month==this.state.dateItems.last_month){
                console.log("true",item.sum)
                this.setState({
                    last_month:item.sum
                })
            }
        })

        this.state.orderReport.week.map((item)=>{
            console.log("item",item._id)
            console.log("item._id.week",item._id.week)
            console.log("this.state.dateItems.current_week",this.state.dateItems.this_week)
            if(item._id.week==this.state.dateItems.this_week){
                console.log("true",item.sum)
                this.setState({
                    this_week:item.sum
                })
            }
            if(item._id.week==this.state.dateItems.last_week){
                console.log("true",item.sum)
                this.setState({
                    last_week:item.sum
                })
            }
        })
        this.state.orderReport.year.map((item)=>{
            console.log("item",item._id)
            console.log("item._id.year",item._id.year)
            console.log("this.state.dateItems.current_year",this.state.dateItems.this_year)
            if(item._id.year==this.state.dateItems.this_year){
                console.log("true",item.sum)
                this.setState({
                    this_year:item.sum
                })
            }
            if(item._id.year==this.state.dateItems.last_year){
                console.log("true",item.sum)
                this.setState({
                    last_year:item.sum
                })
            }
        })

    }


    handleInputChange = (event) => {
        const value = event.target.value;
        const name = event.target.name;

        this.setState({ [name]: value });
    }

    onConfirm = () => {
        this.setState({ success: false });
    }
    handleClickOpen = (type) => {
        this.setState({ open: type });
    };
    handleRequestClose = (event) => {
        this.setState({ open: false, menuState: false, is_edit: false, zip_code: '' });
    };
    handleDateChange1 = date => {
        this.setState({ fromDate: date });
    };
    

    filterData = async() =>{
      
        if(this.state.status==null && this.state.fromDate =="" && this.state.toDate=="" && this.state.category.length==0 && this.state.products.length==0){
            toast.error("Please select valid filter data!", {
                position: toast.POSITION.TOP_RIGHT
              });
        }else{
        let DBData = Object.assign({},{status:this.state.status,products:this.state.products,category:this.state.category,fromDate:this.state.fromDate,toDate:this.state.toDate})
        console.log(DBData);
        let order  = await reportData(DBData);
        console.log("reportData",order);
        this.setState({
            orderData :order.data,
            total_amount :0
        })
        toast.info( order.data.length +" Data Found!", {
            position: toast.POSITION.TOP_RIGHT
          });
        this.handleRequestClose();
    }
    
    }

    render() {
        const { anchorEl, menuState, fromDate,data,categoryData ,orderData,orderReport} = this.state;
        return (
            <div className="app-wrapper app-wrapper-module" >
                <div className="dashboard animated slideInUpTiny animation-duration-3">
                <Dialog open={this.state.open=="filter"} onClose={this.handleRequestClose}>
                    <DialogTitle>Filter data</DialogTitle>
                    <DialogContent >
                    <div className="row  border"  >
                   
                        <div className="col-lg-6  col-12">

                        <label>From Date</label> <br></br>
                        <TextField
                            margin="normal"
                            type="date"
                            fullWidth
                            name="fromDate"
                            value={fromDate}
                            onChange={this.handleInputChange}
                        /><br></br>
                        
            </div>
              <div className="col-lg-6  col-12">
              <label>To Date</label><br></br>
                        <TextField
                            margin="normal"
                            fullWidth
                            type="date"
                            name="toDate"
                            value={this.state.toDate}
                            onChange={this.handleInputChange}
                        />

                            {/* <Button variant="raised"
                                component="label"
                                color="primary"
                                onClick={()=> this.handleClickOpen('date')}
                                className="btn btn-primary"> Sales By Date</Button> */}
                        </div>

                        <br></br>
                        <div className="col-lg-12  col-12">
                        <label>Select Product</label> <br></br>
                        <Select
                                            value={this.state.products}
                                            fullWidth
                                            name="products"
                                            label="Select product"
                                            onChange={this.handleInputChange}
                                            input={<Input id="products" />}
                                            multiple
                                            validators={['required']}
                                            errorMessages={['this field is required']}
                                        >
                                        <MenuItem>All Products</MenuItem>
                                            {data.map(product => {
                                                return (
                                                    <MenuItem value={product._id}>
                                                    <img src={product.productImage[0].image} height="30px" width="30px"/> &nbsp; {product.prodName}</MenuItem>
                                                )
                                            })}
                                        </Select>
                            {/* <Button variant="raised"
                                component="label"
                                color="primary"
                                onClick={()=> this.handleClickOpen('product')}
                                className="btn btn-primary"> Sales By Product</Button> */}
                        </div>
                        <br></br>
                        <div className="col-lg-12  col-12">

                        <label>Select  Category</label> <br></br>
                        <Select
                                            value={this.state.category}
                                            fullWidth
                                            name="category"
                                            label="Select Category"
                                            onChange={this.handleInputChange}
                                            input={<Input id="category" />}
                                            validators={['required']}
                                            multiple
                                            errorMessages={['this field is required']}
                                        >
                                            {categoryData.map(categoryy => {
                                                return (
                                                    <MenuItem value={categoryy._id}>
                                                    <img src={categoryy.categoryImage} height="30px" width="30px"/> &nbsp; {categoryy.categoryName}</MenuItem>
                                                )
                                            })}
                                        </Select>

                            {/* <Button variant="raised"
                                component="label"
                                color="primary"
                                onClick={()=> this.handleClickOpen('category')}
                                className="btn btn-primary"> Sales By Category</Button> */}
                        </div>
                        <br></br>
                        <div className="col-lg-12  col-12">
                        <label>Select Status</label> <br></br>
                       
                       <Select
                                   value={this.state.status}
                                   fullWidth
                                   input={<Input id="type" />}
                                   name="status"
                                   onChange={this.handleInputChange}>
                                     <MenuItem value={0}><img src="https://oticaoutlet.online/wp-content/plugins/woocommerce-order-tracker/assets/images/placed.png" height="30px" width="30px"/>&nbsp; Order Placed</MenuItem>
                                   <MenuItem value={2}> <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ3yQlB1HIZ1SgjQu8-NVUutZlEfbSqyNfABJ-G5-kItSXbbsfe" height="30px" width="30px"/> &nbsp; Order Approved</MenuItem>
                                   <MenuItem value={3}> <img src="https://cdn.connox.com/m/100107/183648/media/Hilfeseiten-2014/icons/versand-lieferung.jpg" height="30px" width="30px"/> &nbsp; Packed</MenuItem>
                                   <MenuItem value={4}> <img src="http://www.midwestdocumentshredding.com/wp-content/uploads/2013/08/delivery-256.png" height="30px" width="30px"/> &nbsp; Shipped</MenuItem>
                                   <MenuItem value={5}> <img src="https://img.icons8.com/color/1600/shipped.png" height="30px" width="30px"/> &nbsp; Delivered</MenuItem>
                                   <MenuItem value={1}> <img src="http://d2dzjyo4yc2sta.cloudfront.net/?url=images.pitchero.com%2Fui%2F347083%2F1377848325_0.jpg&w=820&t=fit&q=85" height="30px" width="30px"/>&nbsp; Cancelled</MenuItem>
                               </Select>
                               <br></br>
                               <div class="center-align">
                                </div>
                        </div>
                    </div>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleRequestClose} color="secondary">
                            Close
                        </Button>
                        <Button onClick={this.filterData} color="primary">
                            Filter
                        </Button>
                    </DialogActions>
                </Dialog>
                    <br />
                    <br />
                    <br />
                    <div className="row">
                    <div className="col-lg-12  col-12">
                    <h2>Sales Report <Button variant="raised" className="bg-primary text-white right" onClick={()=> this.handleClickOpen('filter')} >Filter Data</Button></h2>
                    </div>
                        <div className="col-lg-3  col-12">
                            <ReportBox
                                styleName="bg-teal accent-4 text-white font-size-report"
                                title={`$`+ Number(orderReport.today).toFixed(2)}
                                detail="Today Earnings"
                                subHeadingColor="text-white"
                            >
                                <BarChart data={chartData} maxBarSize={7}
                                    margin={{ left: 0, right: 10, top: 10, bottom: 10 }}>
                                    <Bar dataKey='amt' fill='white' />
                                </BarChart>
                              
                            </ReportBox>
                            <br/>
                        </div>

                        <div className="col-lg-3  col-12">
                            <ReportBox
                                styleName="bg-red text-white"
                                title={`$`+ Number(this.state.this_month).toFixed(2)}
                                detail="This Month earnings"
                                subHeadingColor="text-white"
                            >
                                <PieChart>
                                    <Pie dataKey="amt" data={pieChartData} cx="50%" cy="50%" innerRadius={30}
                                        outerRadius={45}
                                        fill="#ffc658" />
                                    <Tooltip />
                                </PieChart>
                            </ReportBox>
                        </div>

                        <div className="col-lg-3  col-12">
                            <ReportBox
                                styleName="bg-purple text-white"
                                title={`$`+Number(this.state.this_week).toFixed(2)}
                                detail="This Week Earnings"
                                subHeadingColor="text-white"
                            >
                                <LineChart data={lineChartData} margin={{ left: 5, right: 10, top: 0, bottom: 0 }}>
                                    <Line dataKey='amt' stroke='white' />
                                </LineChart>
                            </ReportBox>
                        </div>
                        <div className="col-lg-3  col-12">
                            <ReportBox
                                styleName="bg-red text-white"
                                  title={`$`+ Number(this.state.this_year).toFixed(2)}
                                detail="This Year Earnings"
                                subHeadingColor="text-white"
                            >
                                <PieChart onMouseEnter={this.onPieEnter}>
                                    <Pie dataKey="amt"
                                        data={pieChartData} cx="50%" cy="50%"
                                        innerRadius={30}
                                        outerRadius={45}
                                        fill="#3367d6"
                                        paddingAngle={5}
                                    >
                                        {
                                            pieChartData.map((entry, index) => <Cell key={index}
                                                fill={COLORS[index % COLORS.length]} />)
                                        }
                                    </Pie>
                                </PieChart>
                            </ReportBox>
                        </div>
                    </div>

                    <div className="row">
                    <div className="col-lg-3  col-12">
                            <ReportBox
                                styleName="bg-blue text-white"
                                  title={`$`+ Number(orderReport.yesterday).toFixed(2)}
                                detail="Yesterday Earnings"
                                subHeadingColor="text-white"
                            >
                                <PieChart onMouseEnter={this.onPieEnter}>
                                    <Pie dataKey="amt"
                                        data={pieChartData} cx="50%" cy="50%"
                                        innerRadius={30}
                                        outerRadius={45}
                                        fill="#3367d6"
                                        paddingAngle={5}
                                    >
                                        {
                                            pieChartData.map((entry, index) => <Cell key={index}
                                                fill={COLORS[index % COLORS.length]} />)
                                        }
                                    </Pie>
                                </PieChart>
                            </ReportBox>
                        </div>
                        <div className="col-lg-3  col-12">
                            <ReportBox
                                styleName="bg-green accent-4 text-white"
                                title={`$`+ Number(this.state.last_month).toFixed(2)}
                                detail="Last Month Earnings"
                                subHeadingColor="text-white"
                            >
                                <BarChart data={chartData} maxBarSize={7}
                                    margin={{ left: 0, right: 10, top: 10, bottom: 10 }}>
                                    <Bar dataKey='amt' fill='white' />
                                </BarChart>
                            </ReportBox>
                        </div>

                        <div className="col-lg-3  col-12">
                            <ReportBox
                                styleName="bg-grey text-white"
                                title={`$`+ Number(this.state.last_week).toFixed(2)}
                                detail="Last Week earnings"
                                subHeadingColor="text-white"
                            >
                                <PieChart>
                                    <Pie dataKey="amt" data={pieChartData} cx="50%" cy="50%" innerRadius={30}
                                        outerRadius={45}
                                        fill="#ffc658" />
                                    <Tooltip />
                                </PieChart>
                            </ReportBox>
                        </div>

                        <div className="col-lg-3  col-12">
                            <ReportBox
                                styleName="bg-black text-white"
                                title={`$`+Number(this.state.last_year).toFixed(2)}
                                detail="Last Year Earnings"
                                subHeadingColor="text-white"
                            >
                                <LineChart data={lineChartData} margin={{ left: 5, right: 10, top: 0, bottom: 0 }}>
                                    <Line dataKey='amt' stroke='white' />
                                </LineChart>
                            </ReportBox>
                        </div>
                        
                    </div>
                </div>
                <br/>
                <h2>Recent Orders  <span className="right"> Total Amount : $ {Number(this.state.total_amount).toFixed(2)}</span></h2>
                <br/>
                <table className="table table-hover">
                    <tr>
                        <th>S.No</th>
                        <th>Order Id</th>
                        <th>Customer Name</th>
                        <th>Status</th>
                        <th>Date Added</th>
                        <th>Amount</th>
                        </tr>
                        <tbody>
                           
                            {orderData.length!=0 && orderData.map((data,index)=>{
                                this.state.total_amount += data.total_amount;
                                return(
                                    <tr>
                                       <td>{index+1}</td> 
                                       <td><Link to={`/app/order/single/${data.order_code}`} params={{ id: data.order_code }}><b>{data.order_code}</b></Link></td> 
                                       <td>{data.user.full_name}</td> 
                                       <td>{data.status==0 &&   <b className="text-white badge badge-info">Order Placed</b>} {data.status==1 &&   <b className="text-white badge badge-danger">Order Cancelled</b>} {data.status==2 &&   <b className="text-white badge badge-warning">Order Approved</b>} {data.status==3 &&   <b className="text-white badge badge-warning">Packed</b>}  {data.status==4 &&   <b className="text-white badge badge-success">Shipped</b>}  {data.status==5 &&   <b className="text-white badge badge-success">Delivered</b> } </td> 
                                       <td>{data.created_at}</td> 
                                       <td><b>$ {Number(data.total_amount).toFixed(2)}</b></td> 
                                    </tr>
                                )
                            })

                            }
                            {orderData.length==0 && 
                            <td colSpan="6" align="center"><img src="https://d3j8rxrmmxre17.cloudfront.net/phl-common-images/No-record-found-d.png"/></td>}
                            </tbody>
                    </table>

            </div>

        );
    }
}

export default SalesReport;