import React from 'react';
import {Link, withRouter} from 'react-router-dom';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import keycode from 'keycode';
import Table, {
    TableBody,
    TableCell,
    TableFooter,
    TableHead,
    TablePagination,
    TableRow,
    TableSortLabel,
} from 'material-ui/Table';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';
import Paper from 'material-ui/Paper';
import IconButton from 'material-ui/IconButton';
import Tooltip from 'material-ui/Tooltip';
import DeleteIcon from 'material-ui-icons/Delete';
import {ButtonDropdown, ButtonGroup, DropdownItem, DropdownMenu, DropdownToggle} from 'reactstrap';
import Button from 'material-ui/Button';
import {getProduct} from '../../actions/Product';
import {getOrder} from '../../actions/Order';
let counter = 0;
//index,item._id,item.order_code, item.user.full_name,item.product.prodName, item.created_at,item.total_amount,'Active','View'
function createData(index,ord_id,ord_code, username, product, date,amount,status,action) {
    
    return {id: index+1,ord_id, ord_code, username, product, date,amount,status,action};
}

const columnData = [
    {id: 'SNo',  disablePadding: true, label: 'SNo'},
    {id: 'Order Id',  disablePadding: false, label: 'Order Id'},
    {id: 'Name',  disablePadding: false, label: 'Customer Name'},
    {id: 'Product Name',  disablePadding: false, label: 'Product Name'},
    {id: 'Date Added',  disablePadding: false, label: 'Date Added'},
    {id: 'Amount',  disablePadding: false, label: 'Amount'},
    {id: 'status',  disablePadding: false, label: 'Status'},
    {id: 'action',  disablePadding: false, label: 'Actions'},
];

class DataTableHead extends React.Component {
    static propTypes = {
        numSelected: PropTypes.number.isRequired,
        onRequestSort: PropTypes.func.isRequired,
        onSelectAllClick: PropTypes.func.isRequired,
        order: PropTypes.string.isRequired,
        orderBy: PropTypes.string.isRequired,
        rowCount: PropTypes.number.isRequired,
    };

    createSortHandler = property => event => {
        this.props.onRequestSort(event, property);
    };

    render() {
        const {onSelectAllClick, order, orderBy, numSelected, rowCount} = this.props;

        return (
            <TableHead>
                <TableRow>
                    
                    {columnData.map(column => {
                        return (
                            <TableCell
                           margin="10px"
                                key={column.id}
                                padding={column.disablePadding ? 'default' : 'default'}
                            >
                                <Tooltip
                                    title="Sort"
                                    placement={column.numeric ? 'bottom-end' : 'bottom-start'}
                                    enterDelay={300}
                                >
                                    <TableSortLabel
                                        active={orderBy === column.id}
                                        direction={order}
                                        onClick={this.createSortHandler(column.id)}
                                    >
                                        {column.label}
                                    </TableSortLabel>
                                </Tooltip>
                            </TableCell>
                        );
                    }, this)}
                </TableRow>
            </TableHead>
        );
    }
}


let DataTableToolbar = props => {
    const {numSelected} = props;

    return (
        <Toolbar
            className={classNames("table-header", {
                ["highlight-light"]: numSelected > 0,
            })}
        >
            <div className="title">
                {numSelected > 0 ? (
                    <Typography type="subheading">{numSelected} selected</Typography>
                ) : (
                    <Typography type="title">Order List</Typography>
                )}
            </div>
            <div className="spacer" />
            <div className="actions">
                {numSelected > 0 && (
                    <Tooltip title="Delete">
                        <IconButton aria-label="Delete">
                            <DeleteIcon />
                        </IconButton>
                    </Tooltip>
                )}
            </div>
        </Toolbar>
    );
};

DataTableToolbar.propTypes = {
    numSelected: PropTypes.number.isRequired,
};


class OrderTable extends React.Component {
    handleRequestSort = (event, property) => {
        const orderBy = property;
        let order = 'desc';

        if (this.state.orderBy === property && this.state.order === 'desc') {
            order = 'asc';
        }

        const data =
            order === 'desc'
                ? this.state.data.sort((a, b) => (b[orderBy] < a[orderBy] ? -1 : 1))
                : this.state.data.sort((a, b) => (a[orderBy] < b[orderBy] ? -1 : 1));

        this.setState({data, order, orderBy});
    };
    handleSelectAllClick = (event, checked) => {
        if (checked) {
            this.setState({selected: this.state.data.map(n => n.id)});
            return;
        }
        this.setState({selected: []});
    };
    handleKeyDown = (event, id) => {
        if (keycode(event) === 'space') {
            this.handleClick(event, id);
        }
    };
    
    handleChangePage = (event, page) => {
        this.setState({page});
    };
    handleChangeRowsPerPage = event => {
        this.setState({rowsPerPage: event.target.value});
    };

    
    async componentDidMount() {
      
        const order = await getOrder();
        console.log("order",order);
        const orderData = order.data;
        console.log("orderData",orderData);
        this.setState({
            data: orderData,
            is_loading:false //await orderData.map((item, index) => createData(index,item._id,item.order_code, item.user.full_name,item.product.prodName, item.created_at,item.total_amount,'Active','View')) 
        });
        console.log("data ",this.state.data);        
    }

    isSelected = id => this.state.selected.indexOf(id) !== -1;

    constructor(props, context) {
        super(props, context);

        this.state = {
            order: 'asc',
            orderBy: 'calories',
            selected: [],
            data: [],
            page: 0,
            rowsPerPage: 10,
            is_loading:true,
        };
    }

    render() {
        const {data, order, orderBy, selected, rowsPerPage, page,is_loading } = this.state;

        return (
            <div className="dashboard animated slideInUpTiny animation-duration-3">
            <h2>Order Management</h2>
            
            <Paper>
                
                <DataTableToolbar numSelected={selected.length} />
                <div className="flex-auto">
                    <div className="table-responsive-material">
                    <div className="center-align">  {is_loading && <img src="https://nmsa.dac.gov.in/images/loader.gif" width="100px"></img>}</div> 
                         {!is_loading &&
                        <Table className="">
                            <DataTableHead
                                numSelected={selected.length}
                                order={order}
                                orderBy={orderBy}
                                onSelectAllClick={this.handleSelectAllClick}
                                onRequestSort={this.handleRequestSort}
                                rowCount={data.length}
                            />
                            <TableBody>
                                {data.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((n, index) => {
                                    console.log("index",index)
                                    console.log("n",n)
                                    return (
                                        <TableRow
                                            hover
                                            tabIndex={-1}
                                            key={n.id}
                                        >

                                            <TableCell padding="default">
                                            {index+1}
                                            </TableCell>
                                            <TableCell padding="default">{n.order_code}</TableCell>
                                            <TableCell  padding="default">{n.user.full_name}</TableCell>
                                            <TableCell  padding="default">{n.OrderDetails[0].productData.map(prod=>{
                                               return(
                                                prod.prodName
                                               )
                                            })
                                            }</TableCell>
                                            <TableCell  padding="default">{n.created_at}</TableCell>
                                            <TableCell  padding="default">{n.total_amount}</TableCell>
                                        <TableCell  padding="default">{n.status==0 &&   <b className="text-white badge badge-info">Order Placed</b>} {n.status==1 &&   <b className="text-white badge badge-danger">Order Cancelled</b>} {n.status==2 &&   <b className="text-white badge badge-warning">Order Approved</b>} {n.status==3 &&   <b className="text-white badge badge-warning">Packed</b>}  {n.status==4 &&   <b className="text-white badge badge-success">Shipped</b>}  {n.status==5 &&   <b className="text-white badge badge-success">Delivered</b> }  </TableCell>
                                            <TableCell  padding="default"><Link to={`/app/order/single/${n.order_code}`} params={{ id: n.order_code }}>View</Link></TableCell>
                                            
                                        </TableRow>
                                    );
                                })}
                            </TableBody>
                            <TableFooter>
                                <TableRow>
                                    <TablePagination
                                        count={data.length}
                                        rowsPerPage={rowsPerPage}
                                        page={page}
                                        onChangePage={this.handleChangePage}
                                        onChangeRowsPerPage={this.handleChangeRowsPerPage}
                                    />
                                </TableRow>
                            </TableFooter>
                        </Table>
                        }
                    </div>
                </div>
            </Paper>
                         
            </div>
        );
    }
}

export default OrderTable;