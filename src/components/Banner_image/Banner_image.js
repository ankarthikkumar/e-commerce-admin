import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import keycode from 'keycode';
import Table, {
    TableBody,
    TableCell,
    TableFooter,
    TableHead,
    TablePagination,
    TableRow,
    TableSortLabel,
} from 'material-ui/Table';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';
import Paper from 'material-ui/Paper';
import IconButton from 'material-ui/IconButton';
import Tooltip from 'material-ui/Tooltip';
import DeleteIcon from 'material-ui-icons/Delete';
import CardBox from "components/CardBox/index";
import { addBannerImage } from '../../actions/BannerImage'
import { getBannerImage } from '../../actions/BannerImage'
import { EditBannerImage } from '../../actions/BannerImage'
import Button from 'material-ui/Button';
import TextField from 'material-ui/TextField';
import SweetAlert from 'react-bootstrap-sweetalert';
import CircularProgress from '@material-ui/core/CircularProgress';
import { awsFileUpload } from '../../actions/fileUpload';
import Dialog, { DialogActions, DialogContent, DialogContentText, DialogTitle, } from 'material-ui/Dialog';
import Menu, { MenuItem } from 'material-ui/Menu';
// import IconButton from 'material-ui/IconButton';
let counter = 0;

function createData(index, _id, code, isDeleted, action) {

    return { id: index + 1, _id, code, isDeleted, action };
}
const ITEM_HEIGHT = 40;
const options = [
    'Update Data',
    'Detailed Log',
    'Statistics',
    'Clear Data',
];


const columnData = [
    { id: 'SNo', disablePadding: true, label: 'SNo' },
    { id: 'Banner name', disablePadding: false, label: 'Banner name' },
    { id: 'Image', disablePadding: false, label: 'Image' },
    { id: 'status', disablePadding: false, label: 'Status' },
    { id: 'action', disablePadding: false, label: 'Actions' },
];

class DataTableHead extends React.Component {
    static propTypes = {
        numSelected: PropTypes.number.isRequired,
        onRequestSort: PropTypes.func.isRequired,
        onSelectAllClick: PropTypes.func.isRequired,
        order: PropTypes.string.isRequired,
        orderBy: PropTypes.string.isRequired,
        rowCount: PropTypes.number.isRequired,
    };

    createSortHandler = property => event => {
        this.props.onRequestSort(event, property);
    };



    render() {
        const { onSelectAllClick, order, orderBy, numSelected, rowCount } = this.props;

        return (
            <TableHead>
                <TableRow>

                    {columnData.map(column => {
                        return (
                            <TableCell
                                margin="10px"
                                key={column.id}
                                padding={column.disablePadding ? 'default' : 'default'}
                            >
                                <Tooltip
                                    title="Sort"
                                    placement={column.numeric ? 'bottom-end' : 'bottom-start'}
                                    enterDelay={300}
                                >
                                    <TableSortLabel
                                        active={orderBy === column.id}
                                        direction={order}
                                        onClick={this.createSortHandler(column.id)}
                                    >
                                        {column.label}
                                    </TableSortLabel>
                                </Tooltip>
                            </TableCell>
                        );
                    }, this)}
                </TableRow>
            </TableHead>
        );
    }
}


let DataTableToolbar = props => {
    const { numSelected } = props;

    return (
        <Toolbar
            className={classNames("table-header", {
                ["highlight-light"]: numSelected > 0,
            })}
        >
            <div className="title">
                {numSelected > 0 ? (
                    <Typography type="subheading">{numSelected} selected</Typography>
                ) : (
                        <Typography type="title">Banner Image List   </Typography>
                    )}
            </div>
            <div className="spacer" />
            <div className="actions">
                {numSelected > 0 && (
                    <Tooltip title="Delete">
                        <IconButton aria-label="Delete">
                            <DeleteIcon />
                        </IconButton>
                    </Tooltip>
                )}
            </div>
        </Toolbar>
    );
};

DataTableToolbar.propTypes = {
    numSelected: PropTypes.number.isRequired,
};


class Banner_image extends React.Component {

    handleRequestSort = (event, property) => {
        const orderBy = property;
        let order = 'desc';

        if (this.state.orderBy === property && this.state.order === 'desc') {
            order = 'asc';
        }

        const data =
            order === 'desc'
                ? this.state.data.sort((a, b) => (b[orderBy] < a[orderBy] ? -1 : 1))
                : this.state.data.sort((a, b) => (a[orderBy] < b[orderBy] ? -1 : 1));

        this.setState({ data, order, orderBy });
    };
    handleSelectAllClick = (event, checked) => {
        if (checked) {
            this.setState({ selected: this.state.data.map(n => n.id) });
            return;
        }
        this.setState({ selected: [] });
    };
    handleKeyDown = (event, id) => {
        if (keycode(event) === 'space') {
            this.handleClick(event, id);
        }
    };

    handleChangePage = (event, page) => {
        this.setState({ page });
    };
    handleChangeRowsPerPage = event => {
        this.setState({ rowsPerPage: event.target.value });
    };

    async componentDidMount() {

        this.loadData();
        setInterval(() => this.loadData(), 60000);
    }
    async loadData() {
        const banner = await getBannerImage();
        const bannerData = banner.data;
        this.setState({
            data:bannerData // ,await bannerData.map((item, index) => createData(index, item._id, item.code, item.isDeleted, 'edit'))
        });
    }
    

    isSelected = id => this.state.selected.indexOf(id) !== -1;

    constructor(props, context) {
        super(props, context);

        this.state = {
            order: 'desc',
            orderBy: 'id',
            selected: [],
            data: [],
            page: 0,
            rowsPerPage: 10,
            heading: '',
            anchorEl: undefined,
            menuState: false,
            is_edit: true,
            imageId: '',
            currentImage: [],
            loading :false, 
            image:'',
            dummy_image:'http://www.maheshwarischolar.org/wp-content/uploads/2017/08/banner.jpg',
            is_image:false,
            success:false,
            is_edit:false,
        };
    }


    handleInputChange = (event) => {
        // console.log(event.target, event.target.value, event.target.name);
        const value = event.target.value;
        const name = event.target.name;
        this.setState({
            [name]: value
        });
    }



    saveImage = async () => {
        var data = Object.assign({}, { heading: this.state.heading,image:this.state.image })
        console.log("data",data);
        if (this.state.is_edit == false) {
            
           
            var response = await addBannerImage(data);
            if (response.status == true) {
                alert("data Saved");
                this.setState({success:true,heading:'',image:''})
                
                this.handleRequestClose();
                this.loadData();
            }
        } else {
            var response = await EditBannerImage(this.state.imageId, data);
            console.log("response", response);
            if (response.status == true) {
                this.setState({ is_edit: false,success:true,heading:'',image:'' });
                this.handleRequestClose();
                this.loadData();

            }
        }
    }

    handleClickOpen = () => {
        console.log(this.props.props)
        // this.props.props.history.push('/app/dashboard');
        this.setState({ open: true });
    };
    handleRequestClose = (event) => {
        this.setState({ open: false, menuState: false ,heading:'',image:'',is_edit:false});
    };
    onOptionMenuSelect = (n, event) => {
        this.setState({ menuState: true, anchorEl: event.currentTarget, currentImage: n });
    };

    handleEdit = (event) => {
        console.log("Edit", this.state.currentImage);
        var currentImage = this.state.currentImage;
        this.state.heading = currentImage.heading;
        this.state.image = currentImage.image;
        this.setState({ menuState: false, is_edit: true });
        this.setState({ imageId: currentImage._id });
        this.handleClickOpen();
    };
    onConfirm = () => {
        this.setState({success: false});
    }

    handleDelete = async (event) => {
        var currentImage = this.state.currentImage;
        var action = currentImage.isDeleted == 1 ? 0 : 1;
        this.setState({ menuState: false, anchorEl: event.currentTarget });
        var data = Object.assign({}, { isDeleted: action })
        var response = await EditBannerImage(currentImage._id, data);

        if (response.status == true) {
           
            this.setState({ is_edit: false,success:true });
            this.handleRequestClose();
            this.loadData();

        }
    };
    handleselectedFile = async (event) => {
        this.setState({
            is_upload: true,
            loading: true,

        })
        const data = new FormData()
        data.append('media', event.target.files[0])
        const fileupload = await awsFileUpload(data);
        this.setState({
            is_upload: false,
            loading: false,
            image: fileupload.file,
            is_image:true
        })
    }

    render() {
        const { menuState, anchorEl, handleRequestClose, data, order, orderBy, selected, rowsPerPage, page, is_show_id ,loading} = this.state;

        return (

            <div className="dashboard animated slideInUpTiny animation-duration-3"> <br></br>
                <h2>Banner Image Management  <Button variant="raised" className="bg-primary text-white right" onClick={this.handleClickOpen}>Add Banner Image</Button></h2>
                <br />

                <Dialog open={this.state.open} onClose={this.handleRequestClose} >
                    <DialogTitle>Save Banner Image</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            Add a Banner Image. Which is Show in Web.
                        </DialogContentText>
                        <TextField
                            autoFocus
                            margin="dense"
                            id="name"
                            name="heading"
                            value={this.state.heading}
                            onChange={this.handleInputChange}
                            label="Banner Name"
                            type="text"
                            fullWidth
                        />

                        <div className="col-sm-12 col-md-12">
                            <br />
                            <br />
                            <h2>Banner Image</h2>
                            <div className="">
                                <div className="">
                                    <input type="file" accept="image/*" id="image5" name="image5" className="btn btn-sm image_input_style"
                                    />
    
                                    <img className="image" src={this.state.image!=""?this.state.image:this.state.dummy_image} /><br />
                                    {loading && <CircularProgress className="size-30"/>}
                                    <label for="image5" className="custom_file_input">
                                        <span className="pleaseWaitOption" id="photoProof_icon" >
                                            <b><i className="fa fa-spinner fa-spin"></i> &nbsp;Please wait...</b></span>
                                    </label>
                                    <br />
                                    <Button
                                        variant="raised"
                                        component="label"
                                        color="primary"
                                        className="btn btn-primary"
                                    >
                                        Upload File
                                <input type="file" name="image"
                                            onChange={this.handleselectedFile}
                                            style={{ display: "none" }}
                                        />
                                    </Button>
                                </div>
                            </div>
                        </div>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleRequestClose} color="secondary">
                            Close
                        </Button>
                       
                        
                        <Button onClick={this.saveImage} color="primary" className={this.state.is_upload == true ? "hidden" : ""}>
                            Save
                        </Button>
                    </DialogActions>
                </Dialog>
                <Paper>
                    <DataTableToolbar numSelected={selected.length} />
                    <div className="flex-auto">
                        <div className="table-responsive-material">
                            <Table className="">
                                <DataTableHead
                                    numSelected={selected.length}
                                    order={order}
                                    orderBy={orderBy}
                                    onSelectAllClick={this.handleSelectAllClick}
                                    onRequestSort={this.handleRequestSort}
                                    rowCount={data.length}
                                />
                                <TableBody>
                                    {data.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((n,index) => {
                                        return (
                                            <TableRow
                                                hover

                                                onKeyDown={event => this.handleKeyDown(event, n.id)}

                                                tabIndex={-1}
                                                key={n.id}
                                            >
                                                <TableCell padding="default">
                                                    {index+1}
                                                </TableCell>
                                                <TableCell padding="default">{n.heading}</TableCell>
                                                <TableCell padding="default"><img src={n.image} width="150px"/></TableCell>
                                                <TableCell padding="default">{n.isDeleted == 1 ? 'Deactiveted' : 'Active'}</TableCell>
                                                <TableCell padding="default">
                                                    <IconButton className="size-30" onClick={this.onOptionMenuSelect.bind(this, n)}>
                                                        <i className="zmdi zmdi-more-vert" /></IconButton>
                                                    <Menu
                                                        Menu id="long-menu"
                                                        anchorEl={anchorEl}
                                                        open={menuState}
                                                        onClose={handleRequestClose}
                                                        style={{ maxHeight: ITEM_HEIGHT * 4.5 }}
                                                        MenuListProps={{
                                                            style: {
                                                                width: 150,
                                                                paddingTop: 0,
                                                                paddingBottom: 0
                                                            },
                                                        }}>
                                                        <MenuItem key='Edit' onClick={this.handleEdit.bind(this, n)}>
                                                            Edit
                                                            </MenuItem>,
                                                            <MenuItem key='Delete' onClick={this.handleDelete.bind(this)}>
                                                            Change Status
                                                            </MenuItem>
                                                        <MenuItem key='close' onClick={this.handleRequestClose.bind(this)} >
                                                            Close
                                                            </MenuItem>,
                                                    </Menu></TableCell>
                                            </TableRow>
                                        );
                                    })}
                                </TableBody>
                                <TableFooter>
                                    <TableRow>
                                        <TablePagination
                                            count={data.length}
                                            rowsPerPage={rowsPerPage}
                                            page={page}
                                            onChangePage={this.handleChangePage}
                                            onChangeRowsPerPage={this.handleChangeRowsPerPage}
                                        />
                                    </TableRow>
                                </TableFooter>
                            </Table>
                        </div>
                    </div>
                </Paper>
                <SweetAlert show={this.state.success} success title="Success!" onConfirm={this.onConfirm}>
                        Banner Image Saved Sucussfully!!
                    </SweetAlert>            
            </div>

        );
    }
}

export default Banner_image;