import React from 'react';
import AddSuperCategory from "components/SuperCategory/addSuperCategory";

import TextField from 'material-ui/TextField';
import Button from 'material-ui/Button';
import Switch from 'material-ui/Switch';
class AddCategory extends React.Component {

    onOptionMenuSelect = event => {
        this.setState({ menuState: true, anchorEl: event.currentTarget });
    };
    handleRequestClose = () => {
        this.setState({ menuState: false });
    };

    constructor() {
        super();
        this.state = {
            anchorEl: undefined,
            menuState: false,
        }
    }

    simple = () => {
        console.log("testing")
    }
    render() {
        // alert("Add Category")
        const { anchorEl, menuState } = this.state;
        return (
            <div style={{width: '100%', height: '100%'}}>
            <AddSuperCategory props={this.props}/>
            </div>
                )
    }
}
export default AddCategory;