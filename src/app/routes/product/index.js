import React from 'react';
import {Redirect, Route, Switch} from 'react-router-dom';

import NewProduct from './routes/NewProduct'
import ProductLists from './routes/ProductLists'
import SingleProducts from './routes/SingleProducts'

const Product = ({match}) => (
    <div className="app-wrapper">
        <Switch>
            
            <Route path={`${match.url}/add`} component={NewProduct}/>
            <Route path={`${match.url}/list`} component={ProductLists}/>
            <Route path={`${match.url}/single/:id`} component={SingleProducts}/>
           
        </Switch>
    </div>
);

export default Product;