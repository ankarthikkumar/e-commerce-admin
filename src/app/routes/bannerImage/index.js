import React from 'react';
import Banner_image from "components/Banner_image/Banner_image";

import TextField from 'material-ui/TextField';
import Button from 'material-ui/Button';
import Switch from 'material-ui/Switch';
class BannerImage extends React.Component {

    onOptionMenuSelect = event => {
        this.setState({ menuState: true, anchorEl: event.currentTarget });
    };
    handleRequestClose = () => {
        this.setState({ menuState: false });
    };

    constructor() {
        super();
        this.state = {
            anchorEl: undefined,
            menuState: false,
        }
    }

    render() {
        const { anchorEl, menuState } = this.state;
        return (
            <div className="app-wrapper">
            <div style={{width: '100%', height: '100%'}}>
            <Banner_image props={this.props}/>
            </div>
            </div>
                )
    }
}
export default BannerImage;