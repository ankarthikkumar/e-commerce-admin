import React from 'react';
import {Redirect, Route, Switch} from 'react-router-dom';

import AddCategory from './routes/addCategory'
import Category from './routes/Category'

const SuperCategory = ({match}) => (
    
   
    <div className="app-wrapper">
        <Switch>
          
            <Route path={`${match.url}/add`} component={AddCategory}/>
            <Route path={`${match.url}/list`} component={Category}/>
           
        </Switch>
    </div>
);

export default SuperCategory;