import axios from "axios";
import { constant } from '../constants';

export const getCategory = () => {
    return new Promise((resolve, reject) => {
        axios.get(`${constant.API_ENDPOINT}/category`)
        .then((response) => {
            console.log("category",response.data);
            resolve(response.data)
        })
        .catch((error) => reject(error))
    })
}

export const saveCategory = (request) => {
    return new Promise((resolve, reject) => {
        axios.post(`${constant.API_ENDPOINT}/category`,request)
        .then((response) => {
            console.log("category save",response.data);
            resolve(response.data)
        })
        .catch((error) => reject(error))
    })
}

export const updateCategory = (id,request) => {
    return new Promise((resolve, reject) => {
        axios.put(`${constant.API_ENDPOINT}/category/`+id,request)
        .then((response) => {
            console.log("category save",response.data);
            resolve(response.data)
        })
        .catch((error) => reject(error))
    })
}

export const getSingleCategory = (id) => {
    console.log(id)
    return new Promise((resolve, reject) => {
        axios.get(`${constant.API_ENDPOINT}/category/`+id)
        .then((response) => {
            console.log("category save",response.data);
            resolve(response.data)
        })
        .catch((error) => reject(error))
    })
}


export const getSingleSubCategory = (id) => {
    console.log(id)
    return new Promise((resolve, reject) => {
        axios.get(`${constant.API_ENDPOINT}/sub_category/`+id)
        .then((response) => {
            console.log("sub_category",response.data);
            resolve(response.data)
        })
        .catch((error) => reject(error))
    })
}



export const getSubCategory = () => {
    return new Promise((resolve, reject) => {
        axios.get(`${constant.API_ENDPOINT}/sub_category`)
        .then((response) => {
            console.log("category",response.data);
            resolve(response.data)
        })
        .catch((error) => reject(error))
    })
}
export const saveSubCategory = (request) => {
    return new Promise((resolve, reject) => {
        axios.post(`${constant.API_ENDPOINT}/sub_category`,request)
        .then((response) => {
            console.log("category save",response.data);
            resolve(response.data)
        })
        .catch((error) => reject(error))
    })
}

export const getSubcategoryByCategory = (request) => {
    return new Promise((resolve, reject) => {
        axios.get(`${constant.API_ENDPOINT}/getSubCategoryByCategory/`+request)
        .then((response) => {
            console.log("Sub",response.data);
            resolve(response.data)
        })
        .catch((error) => reject(error))
    })
}

export const updateSubCategory = (id,request) => {
    return new Promise((resolve, reject) => {
        axios.put(`${constant.API_ENDPOINT}/sub_category/`+id,request)
        .then((response) => {
            console.log("sub_category Update",response.data);
            resolve(response.data)
        })
        .catch((error) => reject(error))
    })
}



