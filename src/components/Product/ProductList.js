import React from 'react';
import {Link, withRouter} from 'react-router-dom';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import keycode from 'keycode';
import Table, {
    TableBody,
    TableCell,
    TableFooter,
    TableHead,
    TablePagination,
    TableRow,
    TableSortLabel,
} from 'material-ui/Table';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';
import Paper from 'material-ui/Paper';
import IconButton from 'material-ui/IconButton';
import Tooltip from 'material-ui/Tooltip';
import DeleteIcon from 'material-ui-icons/Delete';
import {ButtonDropdown, ButtonGroup, DropdownItem, DropdownMenu, DropdownToggle} from 'reactstrap';
import Button from 'material-ui/Button';
import SweetAlert from 'react-bootstrap-sweetalert';
import {getProduct, Disable_product} from '../../actions/Product';
import Menu, { MenuItem } from 'material-ui/Menu';
let counter = 0;
const ITEM_HEIGHT = 40;
function createData(index,p_id,pro_id, name, category, sub_cat,price,quantity, image,status) {
    
    return {id: index+1,p_id, pro_id, name, category, sub_cat,price,quantity, image,status};
}

const columnData = [
    {id: 'SNo',  disablePadding: true,width:'', label: 'SNo'},
    {id: 'SKUID',  disablePadding: false, width:'', label: 'SKU ID'},
    {id: 'Name',  disablePadding: false, width:'', label: 'Name'},
    {id: 'Category',  disablePadding: false, width:'', label: 'Category'},
    {id: 'Sub Category',  disablePadding: false, width:'', label: 'Sub Category'},
    // {id: 'price',  disablePadding: false, width:'', label: 'Price'},
    {id: 'Quantity',  disablePadding: false, width:'', label: 'Quantity'},
    // {id: 'Image',  disablePadding: false, width:'', label: 'Image'},
    {id: 'status',  disablePadding: false, width:'', label: 'Status'},
    {id: 'action',  disablePadding: false, width:'14%', label: 'Actions'},
];

class DataTableHead extends React.Component {
    static propTypes = {
        numSelected: PropTypes.number.isRequired,
        onRequestSort: PropTypes.func.isRequired,
        onSelectAllClick: PropTypes.func.isRequired,
        order: PropTypes.string.isRequired,
        orderBy: PropTypes.string.isRequired,
        rowCount: PropTypes.number.isRequired,
    };

    createSortHandler = property => event => {
        this.props.onRequestSort(event, property);
    };

    render() {
        const {onSelectAllClick, order, orderBy, numSelected, rowCount} = this.props;

        return (
            <TableHead>
                <TableRow>
                    
                    {columnData.map(column => {
                        return (
                            <TableCell
                           margin="10px"
                                key={column.id}
                                padding={column.disablePadding ? 'default' : 'default'}
                                style={{width : column.width}}
                            >
                                <Tooltip
                                    title="Sort"
                                    placement={column.numeric ? 'bottom-end' : 'bottom-start'}
                                    enterDelay={300}
                                >
                                    <TableSortLabel
                                        active={orderBy === column.id}
                                        direction={order}
                                        onClick={this.createSortHandler(column.id)}
                                    >
                                        {column.label}
                                    </TableSortLabel>
                                </Tooltip>
                            </TableCell>
                        );
                    }, this)}
                </TableRow>
            </TableHead>
        );
    }
}


let DataTableToolbar = props => {
    const {numSelected} = props;

    return (
        <Toolbar
            className={classNames("table-header", {
                ["highlight-light"]: numSelected > 0,
            })}
        >
            <div className="title">
                {numSelected > 0 ? (
                    <Typography type="subheading">{numSelected} selected</Typography>
                ) : (
                    <Typography type="title">Product List</Typography>
                )}
            </div>
            <div className="spacer" />
            <div className="actions">
                {numSelected > 0 && (
                    <Tooltip title="Delete">
                        <IconButton aria-label="Delete">
                            <DeleteIcon />
                        </IconButton>
                    </Tooltip>
                )}
            </div>
        </Toolbar>
    );
};

DataTableToolbar.propTypes = {
    numSelected: PropTypes.number.isRequired,
};


class ProductList extends React.Component {
    handleRequestSort = (event, property) => {
        const orderBy = property;
        let order = 'desc';

        if (this.state.orderBy === property && this.state.order === 'desc') {
            order = 'asc';
        }

        const data =
            order === 'desc'
                ? this.state.data.sort((a, b) => (b[orderBy] < a[orderBy] ? -1 : 1))
                : this.state.data.sort((a, b) => (a[orderBy] < b[orderBy] ? -1 : 1));

        this.setState({data, order, orderBy});
    };
    handleSelectAllClick = (event, checked) => {
        if (checked) {
            this.setState({selected: this.state.data.map(n => n.id)});
            return;
        }
        this.setState({selected: []});
    };
    handleKeyDown = (event, id) => {
        if (keycode(event) === 'space') {
            this.handleClick(event, id);
        }
    };
    
    handleChangePage = (event, page) => {
        this.setState({page});
    };
    handleChangeRowsPerPage = event => {
        this.setState({rowsPerPage: event.target.value});
    };

    onConfirm = () => {
        this.setState({success: false});
    }

    
    async componentDidMount() {
              this.loadData();
    }
    async loadData(){
        const product = await getProduct();
        // console.log("product in component",product.data);
        const productData = product.data;
        this.setState({
            data: await productData.map((item, index) => createData(index,item._id,item.product_code, item.prodName, item.category.categoryName,item.subCategory.subCategoryName,item.prodPrice,item.prodStock,item.productImage[0].image,item.isDeleted)) ,
            is_loading:false,
        });
        // console.log("data ",this.state.data);  
    }
    onOptionMenuSelect = (n,event) => {
        this.setState({ menuState: true, anchorEl: event.currentTarget,productId:n.p_id});
    };
   

    disableProducts = async (data) => {
        var action = data.status ==1 ? 0 :1;
        var dbUpdate = Object.assign({}, { isDeleted: action})
        var response = await Disable_product(data.p_id,dbUpdate);
        if (response.status == true) {
            this.setState({is_loading:true,success:true});
            this.loadData();
        }
    };
    handleRequestClose = (event) => {
        this.setState({ open: false,menuState:false, anchorEl: event.currentTarget });
    };

  

    isSelected = id => this.state.selected.indexOf(id) !== -1;

    constructor(props, context) {
        super(props, context);

        this.state = {
            order: 'asc',
            orderBy: 'calories',
            selected: [],
            data: [],
            page: 0,
            rowsPerPage: 10,
            anchorEl:false,
            menuState:false,
            productId:'',
            is_loading:true,
            success:false,
        };
    }

    render() {
        const {data, order, orderBy, selected, rowsPerPage, page,anchorEl ,menuState,handleRequestClose,is_loading} = this.state;

        return (
            <div className="dashboard animated slideInUpTiny animation-duration-3">
            <h2>Product Management< Link to="/app/product/add" ><Button variant="raised" className="bg-primary text-white right" >Add Product</Button></Link> </h2>
            <br/>
            <Paper>
                <DataTableToolbar numSelected={selected.length} />
                <div className="flex-auto">
                    <div className="table-responsive-material">
                    <div className="center-align">  {is_loading && <img src="https://nmsa.dac.gov.in/images/loader.gif" width="100px"></img>}</div> 
                         {!is_loading &&
                        <Table className="">
                            <DataTableHead
                                numSelected={selected.length}
                                order={order}
                                orderBy={orderBy}
                                onSelectAllClick={this.handleSelectAllClick}
                                onRequestSort={this.handleRequestSort}
                                rowCount={data.length}
                            />
                            <TableBody>
                                {data.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map(n => {
                                    return (
                                        <TableRow
                                            hover
                                            
                                            onKeyDown={event => this.handleKeyDown(event, n.id)}
                                            
                                            tabIndex={-1}
                                            key={n.id}
                                        >

                                            <TableCell padding="default">
                                            {n.id}
                                            </TableCell>
                                            <TableCell padding="default">{n.pro_id}</TableCell>
                                            <TableCell  padding="default">{n.name}</TableCell>
                                            <TableCell  padding="default">{n.category}</TableCell>
                                            <TableCell  padding="default">{n.sub_cat}</TableCell>
                                            {/* <TableCell  padding="default">{n.price}</TableCell> */}
                                            <TableCell  padding="default">{n.quantity}</TableCell>
                                            {/* <TableCell  padding="default"><img src={n.image} width="25px" height="25px"></img></TableCell> */}
                                            <TableCell  padding="default">{n.status==true?'Disabled':'Active'}</TableCell>
                                            <TableCell  padding="default" tyle={{width: '5%'}}>   <Link to={`/app/product/single/${n.p_id}`} params={{ id: n.p_id}}>
                                            <IconButton className="size-30" >
                                                        <i className="zmdi custom_icon_size zmdi-eye" /></IconButton></Link>  
                                                    &nbsp;
                                                    <Link to={`/app/product/add?action=edit&id=${n.p_id}`} params={{ id: n.p_id,action:"edit" }}> <IconButton className="size-30" >
                                                        <i className="zmdi custom_icon_size zmdi-edit" /></IconButton></Link>
                                                        &nbsp;
                                                        <IconButton className="size-30" onClick={()=> this.disableProducts (n)} >
                                                        <i className="zmdi custom_icon_size zmdi-lock" /></IconButton>

                                                    </TableCell>
                                            
                                        </TableRow>
                                    );
                                })}
                            </TableBody>
                            <TableFooter>
                                <TableRow>
                                    <TablePagination
                                        count={data.length}
                                        rowsPerPage={rowsPerPage}
                                        page={page}
                                        onChangePage={this.handleChangePage}
                                        onChangeRowsPerPage={this.handleChangeRowsPerPage}
                                    />
                                </TableRow>
                            </TableFooter>
                        </Table>
                         }
                    </div>
                </div>
            </Paper>
            <SweetAlert show={this.state.success} success title="Success!" onConfirm={this.onConfirm}>
                        Product Status Changes Sucussfully!!
                    </SweetAlert>
            </div>
        );
    }
}

export default ProductList;