import axios from "axios";
import { constant } from '../constants';

export const addBannerImage = (request) => {
    return new Promise((resolve, reject) => {
        axios.post(`${constant.API_ENDPOINT}/slider_image`,request)
        .then((response) => {
            console.log("zip_code",response.data);
            resolve(response.data)
        })
        .catch((error) => reject(error))
    })
}
export const getBannerImage = () => {
    return new Promise((resolve, reject) => {
        axios.get(`${constant.API_ENDPOINT}/slider_image`)
        .then((response) => {
            // console.log("zip_code",response.data);
            resolve(response.data)
        })
        .catch((error) => reject(error))
    })
}

export const EditBannerImage = (id,request) => {
    return new Promise((resolve, reject) => {
        axios.put(`${constant.API_ENDPOINT}/slider_image/`+id,request)
        .then((response) => {
            console.log("EditBannerImage",response.data);
            resolve(response.data)
        })
        .catch((error) => reject(error))
    })
}



