import React from 'react';
import CardBox from "components/CardBox/index";


import Button from 'material-ui/Button';
import Switch from 'material-ui/Switch';
import Input, { InputLabel } from 'material-ui/Input';
import { MenuItem } from 'material-ui/Menu';
import { FormControl, FormHelperText } from 'material-ui/Form';
import Select from 'material-ui/Select';
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';
import { saveCustomer } from '../../actions/Customer'
import { getCountryCodeList,getSingleUser,UpdateCustomer } from '../../actions/Customer'
import SweetAlert from 'react-bootstrap-sweetalert';
import CircularProgress from '@material-ui/core/CircularProgress';
import { awsFileUpload } from '../../actions/fileUpload';

class AddCustomer extends React.Component {

    onOptionMenuSelect = event => {
        this.setState({ menuState: true, anchorEl: event.currentTarget });
    };
    handleRequestClose = () => {
        this.setState({ menuState: false });
    };

    
    handleselectedFile = async (event) => {
        this.setState({
            is_upload: true,
            loading:true
        })
        const data = new FormData()
        data.append('media', event.target.files[0])
        console.log("--------------------------------------------------");
        const fileupload = await awsFileUpload(data);
        console.log(fileupload);
        console.log("--------------------------------------------------");
        this.setState({
            is_upload: false,
            loading:false,
            profile_picture: fileupload.file,
        })
        console.log("this.state",this.state);
    }
    async componentDidMount() {

        console.log(this.props.props);
        var id = this.props.props.location.search;
        var current_id = id.split('&id=')[1];
        console.log("current_id", current_id);
        if (current_id != undefined) {
            var response = await getSingleUser(current_id);
            var CustomerData = response.data;
            console.log("categoryData", CustomerData)
            var Mobile = CustomerData.mobile_number.split(CustomerData.country_code)[1];
            this.setState({ 
                password:CustomerData.password,
                is_edit: true, 
                customer_id: current_id,
                full_name: CustomerData.full_name,
                country_code: CustomerData.country_code,
                email: CustomerData.email,
                mobile_number:Mobile,
                address_line_1: CustomerData.address_line_1,
                address_line_2: CustomerData.address_line_2,
                profile_picture: CustomerData.profile_picture
            });

        }

        const country = await getCountryCodeList();
        const countryData= country.data;
        this.setState({
            countryCodeList: countryData
        });
    }



    saveCustomer = async () => {
        if(this.state.country_code==""){
            alert("please Add Country Code");
        }else{
            const data = Object.assign({}, {
                full_name: this.state.full_name,
                country_code: this.state.country_code,
                email: this.state.email,
                mobile_number: this.state.country_code + "" + this.state.mobile_number,
                address_line_1: this.state.address_line_1,
                address_line_2: this.state.address_line_2,
                profile_picture: this.state.profile_picture
            });
            var res="";
            if(this.state.is_edit==false){
                data.password= this.state.password;
                console.log('data-> save',data);
                 res = await saveCustomer(data);
            }else{
                console.log('data-> update',data);
                res = await UpdateCustomer(this.state.customer_id,data);
            }
        if(res.status==true){
         this.setState({success:true});
        }
    }
    };

    onConfirm = () => {
        this.setState({success: false});
        console.log(this.props.props)
        this.props.props.history.push('/app/customer/list');
    }

    handleInputChange = (event) => {
        const value = event.target.value;
        const name = event.target.name;

        this.setState({ [name]: value });
    }

    constructor() {
        super();
        this.state = {
            anchorEl: undefined,
            menuState: false,
            password: '',
            full_name: '',
            country_code: '1',
            email: '',
            mobile_number: '',
            address_line_1: '',
            address_line_2: '',
            profile_picture:'https://cresttechnosoft.com/wp-content/uploads/2018/04/no-photo-profile.png?x57459',
            loading :false,
            is_upload:false,
            success: false,
            countryCodeList:[],
            is_edit:false,
            customer_id:''
        }
    }

    render() {
        // alert("Add Category")
        const { anchorEl, menuState,loading  } = this.state;
        return (
            <div className="dashboard animated slideInUpTiny animation-duration-3">
                <h2>Customers Management</h2>


                <CardBox styleName="col-lg-12" childrenStyle="d-flex" heading="Add Customer ">
                    <ValidatorForm
                        ref="form"
                        className="col-lg-8"
                        onSubmit={this.saveCustomer}
                        onError={errors => console.log(errors)}
                    >

                        <div className="col-md-12 col-12">
                            <TextValidator

                                label="Customer Name"
                                value={this.state.full_name}
                                margin="normal"
                                name="full_name"
                                onChange={this.handleInputChange}
                                validators={['required']}
                                errorMessages={['Customer Name  field is required']}
                                fullWidth
                            />
                        </div>
                        <div className="col-md-12 col-12">
                            <TextValidator

                                label="Email"
                                value={this.state.email}
                                margin="normal"
                                name="email"
                                onChange={this.handleInputChange}
                                validators={['required', 'isEmail']}
                                errorMessages={['Email field is required', 'Enter valid Email Adddress']}
                                fullWidth
                            />
                        </div>
                        <div className="col-md-12 col-12"><br />
                            <InputLabel htmlFor="">Country Code</InputLabel>
                            <Select
                                value={this.state.country_code}
                                fullWidth
                                onChange={this.handleInputChange}
                                name="country_code"
                                validators={['required']}
                                errorMessages={['country code this field is required']}
                                label="Country Code"
                                input={<Input id="age-helper" />}
                            >
                                {this.state.countryCodeList.map((n)=>{
                                    return (
                                <MenuItem value={n.dial_code}>{n.name} {n.dial_code}</MenuItem>
                                    )
                            })}
                            </Select>
                        </div>
                        <div className="col-md-12 col-12">
                            <TextValidator
                                validators={['required']}
                                errorMessages={['Mobile Number field is required']}
                                label="Mobile Number"
                                defaultValue=""
                                onChange={this.handleInputChange}
                                name="mobile_number"
                                margin="normal"
                                value={this.state.mobile_number}
                                fullWidth
                            />
                        </div>
                        <div className="col-md-12 col-12">
                            <TextValidator

                                label="Address Line 1"
                                value={this.state.address_line_1}
                                margin="normal"
                                onChange={this.handleInputChange}
                                name="address_line_1"
                                validators={['required']}
                                errorMessages={['Address Line 1 field is required']}
                                fullWidth
                            />
                        </div>

                        <div className="col-md-12 col-12">
                            <TextValidator

                                label="Address Line 2"
                                value={this.state.address_line_2}
                                margin="normal"
                                onChange={this.handleInputChange}
                                name="address_line_2"
                                fullWidth
                            />
                        </div>

                        <div className="col-md-12 col-12">
                            <TextValidator

                                type="password"
                                label="Password"
                                name="password"
                                disabled={this.state.is_edit}
                                value={this.state.password}
                                margin="normal"
                                onChange={this.handleInputChange}
                                validators={['required']}
                                errorMessages={['Password field is required']}
                                fullWidth
                            />
                        </div>





                           <div className="col-sm-4 col-md-4 custom_width">
                        <br/>
                        <br/>
                        <br/>
                            <div className="custom_width">
                                <div className="">
                                    <input type="file" accept="image/*" id="image5" name="image5" className="btn btn-sm image_input_style"
                                    />

                                    <img className="col-md-12" src={this.state.profile_picture} /><br />
                                    <label for="image5" className="custom_file_input">
                                        <span className="pleaseWaitOption" id="photoProof_icon" >
                                            <b><i className="fa fa-spinner fa-spin"></i> &nbsp;Please wait...</b></span>
                                        </label>
                                        <br/>

                            <Button
                                variant="raised"
                                component="label"
                                color="primary"
                                className="btn btn-primary"

                            >
                                Upload File
                                <input type="file" name="profile_picture" 
                                         onChange={this.handleselectedFile} 
                                        style={{ display: "none" }}
                                    />
                            </Button>
                                </div>
                            </div>
                        </div>
                        <br />
                        <div className="col-md-12 col-12">
                        {loading && <CircularProgress/>}
                            <Button variant="raised" type="submit" color="primary" className={this.state.is_upload == true ? "hidden" : "jr-btn  text-white"}>Save Customer</Button>
                        </div>
                    </ValidatorForm>
                </CardBox>
                <SweetAlert show={this.state.success} success title="Success!" onConfirm={this.onConfirm}>
                        Customer Saved Sucussfully!!
                    </SweetAlert> 
            </div>
        )
    }
}
export default AddCustomer;