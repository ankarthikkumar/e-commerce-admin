import React from 'react';
import Button from 'material-ui/Button';


const Footer = () => {
        return (
            <footer className="app-footer">
                <div className="d-flex flex-row justify-content-between">
                    <div>
                        <span> Copyright @ GopuffClone &copy; 2019</span>

                    </div>
                    <div>
                        
                    </div>
                </div>
            </footer>
        );
    }
;

export default Footer;
