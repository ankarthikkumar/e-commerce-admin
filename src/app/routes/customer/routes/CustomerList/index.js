import React from 'react';
import CustomerList from "components/Customers/CustomerList";

class NewCustomer extends React.Component {

    onOptionMenuSelect = event => {
        this.setState({ menuState: true, anchorEl: event.currentTarget });
    };
    handleRequestClose = () => {
        this.setState({ menuState: false });
    };

    constructor() {
        super();
        this.state = {
            anchorEl: undefined,
            menuState: false,
        }
    }

    render() {
        const { anchorEl, menuState } = this.state;
        return (
            <div style={{width: '100%', height: '100%'}}>
            <CustomerList />
            </div>
                )
    }
}
export default NewCustomer;