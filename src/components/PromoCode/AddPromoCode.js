import React from 'react';
import CardBox from "components/CardBox/index";
import Button from 'material-ui/Button';
import Input, { InputLabel } from 'material-ui/Input';
import { MenuItem } from 'material-ui/Menu';
import { FormControl, FormHelperText } from 'material-ui/Form';
import Select from 'material-ui/Select'
import SweetAlert from 'react-bootstrap-sweetalert';
import { savePromoCode } from '../../actions/PromoCode'
import { singlePromoCode } from '../../actions/PromoCode'
import { updatePromocode } from '../../actions/PromoCode'
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';
import {getProduct} from '../../actions/Product';
class AddPromoCode extends React.Component {

    onOptionMenuSelect = event => {
        this.setState({ menuState: true, anchorEl: event.currentTarget });
    };
    handleRequestClose = () => {
        this.setState({ menuState: false });
    };

    handleInputChange = (event) => {
        const value = event.target.value;
        const name = event.target.name;

        this.setState({ [name]: value });
    }
    handleCategoryStatus = (event) => {
        this.setState({ CategoryStatus: !this.state.CategoryStatus });
    }
    onConfirm = (event) => {
        this.setState({ success: false });
        console.log(this.props.props)
        this.props.props.history.push('/app/promocode/list');
    }


    async componentDidMount() {
        console.log(this.props.props);
        var id = this.props.props.location.search;
        var current_id = id.split('&id=')[1];
        console.log("current_id", current_id);
        if (current_id != undefined) {
            var response = await singlePromoCode(current_id);
            var CouponData = response.data[0];
            console.log("CouponData", CouponData)
            this.setState({
                CouponName : CouponData.CouponName,
                CouponValue : CouponData.CouponValue,
                CouponDescription :CouponData.CouponDescription,
                CouponCode :CouponData.  CouponCode,  
                type : CouponData.type,
                startDate :  CouponData.startDate,   
                endDate :   CouponData.endDate,
                perUserCount :  CouponData.perUserCount,
                perCouponCount :CouponData.perCouponCount,
                products:CouponData.products,
                is_edit:true,
                promocode_id:CouponData._id
            })

        }

        const product = await getProduct();
        const productData = product.data;
        console.log(productData);
        this.setState({
            data: productData
        });
        console.log("data ",this.state.data);        
    }


    savePromoCode = async () => {

        const data = Object.assign({}, { 
            CouponName : this.state.CouponName,
            CouponValue : this.state.CouponValue,
            CouponDescription :this.state.CouponDescription,
            CouponCode :this.state.  CouponCode,  
            type : this.state.type,
            startDate :  this.state.startDate,   
            endDate :   this.state.endDate,
            perUserCount :  this.state.perUserCount,
            perCouponCount :this.state.perCouponCount,
            products:this.state.products
        })
        console.log(data);
        var response= "";
        if(this.state.is_edit==false){
            response = await savePromoCode(data);
        }else{
            response = await updatePromocode(this.state.promocode_id ,data);
        }
        console.log("response", response);
        if (response.status == true) {
           this.setState({
               success:true,
               alertText:response.message
           })
        }
        console.log(this.state.products);
        if (!this.state.type) {
            this.setState({ hasError: true });
          }

    }

    constructor() {
        super();
        this.state = {
            anchorEl: undefined,
            menuState: false,
            CouponName: '',
            CouponDescription: '',
            CouponCode: '',
            type: '',
            startDate: '',
            endDate: '',
            perUserCount: '',
            perCouponCount: '',
            hasError:false,
            data:[],
            products :[],
            CouponValue:'',
            promocode_id:'',
            is_edit:false,
            success:false,
            alertText:'',
            // categoryDescription: '',

        }
    }

    render() {
        // console.log("simple fun",this.props);
        // alert("Add Category")
        const { anchorEl, menuState,hasError,data  } = this.state;
        return (
            <div className="dashboard animated slideInUpTiny animation-duration-3">
                <h2>Promocode</h2>


                <CardBox styleName="col-lg-12" childrenStyle="d-flex" heading="Add Promocode ">
                    <ValidatorForm
                        ref="form"
                        className="col-lg-12"
                        onSubmit={this.savePromoCode}
                        onError={errors => console.log(errors)}
                    >


                        <div className="col-md-6 col-12">
                            <TextValidator
                                id="name"
                                label="Coupon Name"
                                value={this.state.CouponName}
                                name="CouponName"
                                onChange={this.handleInputChange}
                                fullWidth
                                validators={['required']}
                                errorMessages={['this field is required']}
                            />
                        </div>
                        <div className="col-md-6 col-12">
                            <TextValidator
                                id="name"
                                label="Coupon Value"
                                value={this.state.CouponValue}
                                name="CouponValue"
                                onChange={this.handleInputChange}
                                fullWidth
                                validators={['required']}
                                errorMessages={['this field is required']}
                            />
                        </div>
                        <div className="col-md-6 col-12">
                            <TextValidator
                                id="name"
                                label="Coupon Code"
                                value={this.state.CouponCode}
                                name="CouponCode"
                                onChange={this.handleInputChange}
                                fullWidth
                                validators={['required']}
                                errorMessages={['this field is required']}
                            />
                        </div>
                        <br />
                        <div className="col-md-6 col-12">
                            <InputLabel htmlFor="type">Type </InputLabel>
                            <Select
                                value={this.state.type}
                                fullWidth
                                input={<Input id="type" />}
                                name="type"

                                onChange={this.handleInputChange}>
                                <MenuItem value="1">Percentage</MenuItem>
                                <MenuItem value="2">Fixed</MenuItem>
                            </Select>
                            {hasError && <FormHelperText className="error">This is required!</FormHelperText>}
                        </div>

                        <div className="col-md-6 col-12">
                        <label>Date Start</label>
                            <TextValidator
                                id="dateStart"
                                type="date"
                                value={this.state.startDate}
                                name="startDate"
                                onChange={this.handleInputChange}
                                fullWidth
                                validators={['required']}
                                errorMessages={['this field is required']}
                            />
                        </div>

                        <div className="col-md-6 col-12">
                            <label>End Date </label>
                            <TextValidator
                                id="endDate"
                                type="date"
                                value={this.state.endDate}
                                name="endDate"
                                onChange={this.handleInputChange}
                                fullWidth
                                validators={['required']}
                                errorMessages={['this field is required']}
                            />
                        </div>

                        <div className="col-md-6 col-12">
                            <TextValidator
                                id="perCouponCount"
                                label="Use Per Coupon"
                                value={this.state.perCouponCount}
                                name="perCouponCount"
                                onChange={this.handleInputChange}
                                fullWidth
                                validators={['required']}
                                errorMessages={['this field is required']}
                            />
                        </div>


                        <div className="col-md-6 col-12">
                            <TextValidator
                                id="perUserCount"
                                label="Coupon Per User"
                                value={this.state.perUserCount}
                                name="perUserCount"
                                onChange={this.handleInputChange}
                                fullWidth
                                validators={['required']}
                                errorMessages={['this field is required']}
                            />
                        </div>

                        <div className="col-md-6 col-12">
                        <InputLabel htmlFor="products">Select Products </InputLabel>
                        <Select
                                            value={this.state.products}
                                            fullWidth
                                            name="products"
                                            label="Select Products"
                                            onChange={this.handleInputChange}
                                            input={<Input id="products" />}
                                            validators={['required']}
                                            multiple
                                            errorMessages={['this field is required']}
                                        >
                                            {data.map(prodduct => {
                                                return (
                                                    <MenuItem value={prodduct._id}>{prodduct.prodName}</MenuItem>
                                                )
                                            })}
                                        </Select>
                        </div>
                        <div className="col-md-6 col-12">

                            <TextValidator
                                id="multiline-static"
                                label="Description"
                                multiline
                                rows="4"
                                defaultValue=""
                                value={this.state.CouponDescription}
                                name="CouponDescription"
                                onChange={this.handleInputChange}
                                fullWidth

                            />
                        </div>

                        <div className="col-md-6 col-12">
                            <Button variant="raised" type="submit" color="primary" className="jr-btn  text-white" >Save Promocode</Button>
                        </div>
                    </ValidatorForm>
                </CardBox>
                <SweetAlert show={this.state.success} success title="Success!" onConfirm={this.onConfirm}>
                        {this.state.alertText}
                    </SweetAlert>
            </div>
        )
    }
}
export default AddPromoCode;