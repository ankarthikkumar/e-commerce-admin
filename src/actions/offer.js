import axios from "axios";
import { constant } from '../constants';

export const saveOffer = (request) => {
    return new Promise((resolve, reject) => {
        axios.post(`${constant.API_ENDPOINT}/offer/`,request)
        .then((response) => {
            console.log("saveOffer save",response.data);
            resolve(response.data)
        })
        .catch((error) => reject(error))
    })
}

export const getOffer = () => {
    return new Promise((resolve, reject) => {
        axios.get(`${constant.API_ENDPOINT}/offer/`)
        .then((response) => {
            console.log("saveOffer save",response.data);
            resolve(response.data)
        })
        .catch((error) => reject(error))
    })
}

export const singleOffer = (id) => {
    return new Promise((resolve, reject) => {
        axios.get(`${constant.API_ENDPOINT}/offer/`+id)
        .then((response) => {
            console.log("singleOffer",response.data);
            resolve(response.data)
        })
        .catch((error) => reject(error))
    })
}

export const updateOffer = (id,request) => {
    return new Promise((resolve, reject) => {
        axios.put(`${constant.API_ENDPOINT}/offer/`+id,request)
        .then((response) => {
            console.log("update save",response.data);
            resolve(response.data)
        })
        .catch((error) => reject(error))
    })
}
