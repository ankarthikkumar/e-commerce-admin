import axios from "axios";
import { constant } from '../constants';

export const getOrder = (request) => {
    return new Promise((resolve, reject) => {
        axios.get(`${constant.API_ENDPOINT}/order`)
        .then((response) => {
            console.log("order",response.data);
            resolve(response.data)
        })
        .catch((error) => reject(error))
    })
}
export const getOrderAdmin = (request) => {
    return new Promise((resolve, reject) => {
        axios.get(`${constant.API_ENDPOINT}/getOrderAdmin`)
        .then((response) => {
            console.log("order",response.data);
            resolve(response.data)
        })
        .catch((error) => reject(error))
    })
}
export const getOrderById = (code) => {
    return new Promise((resolve, reject) => {
        axios.get(`${constant.API_ENDPOINT}/order/`+code)
        .then((response) => {
            console.log(`${constant.API_ENDPOINT}/order/`+code);
            console.log("single order res",response.data);
            resolve(response.data)
        })
        .catch((error) => reject(error))
    })
}
export const getOrderByUser = (id) => {
    return new Promise((resolve, reject) => {
        axios.get(`${constant.API_ENDPOINT}/orderUser/`+id)
        .then((response) => {
            console.log(`${constant.API_ENDPOINT}/orderUser/`+id);
            console.log("single order res",response.data);
            resolve(response.data)
        })
        .catch((error) => reject(error))
    })
}

export const UpdateOrderStatus = (id,request) => {
    return new Promise((resolve, reject) => {
        axios.put(`${constant.API_ENDPOINT}/order/`+id,request)
        .then((response) => {
            console.log("Update",response.data);
            resolve(response.data)
        })
        .catch((error) => reject(error))
    })
}
export const FilterOrderData = (id,request) => {
    return new Promise((resolve, reject) => {
        axios.put(`${constant.API_ENDPOINT}/order/`+id,request)
        .then((response) => {
            console.log("Update",response.data);
            resolve(response.data)
        })
        .catch((error) => reject(error))
    })
}

