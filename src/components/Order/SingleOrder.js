import React, { Component } from 'react';
import PropTypes from 'prop-types';
import AppBar from 'material-ui/AppBar';
import {Link, withRouter} from 'react-router-dom';
import Tabs, { Tab } from 'material-ui/Tabs';
import CardBox from "components/CardBox/index";
import Select from 'material-ui/Select'
import Input, { InputLabel } from 'material-ui/Input';
import { MenuItem } from 'material-ui/Menu';
import Button from 'material-ui/Button';
import { getOrderById } from '../../actions/Order';
import { UpdateOrderStatus } from '../../actions/Order';
import SweetAlert from 'react-bootstrap-sweetalert';

function TabContainer(props) {
    console.log("props", props);
    return (
        <CardBox styleName="col-lg-12" childrenStyle="d-flex" >
            <div style={{ padding: 20 }}>
                {props.children}
            </div>
        </CardBox>
    );
}

TabContainer.propTypes = {
    children: PropTypes.node.isRequired,
};

class SingleOrder extends Component {
    state = {
        value: 0,
        data: [],
        category: [],
        subCategory: [],
        unit: [],
        total_value: 0,
        OrderDetails: [],
        address: [],
        status:false,
        save_status:false,
        is_loading:true,
        customerData:[]
    };
    handleInputChange = (event) => {
        const value = event.target.value;
        const name = event.target.name;

        this.setState({ [name]: value });
    }
    updateCategory = async () => {
        const data  = Object.assign({}, { status:this.state.status});
        console.log(data);
        console.log(this.state.data._id);

        const status  = await UpdateOrderStatus(this.state.data._id,data);
        console.log(status);
        if(status.status==true){
          this.setState({
            save_status:true
          })
        }
    }

    handleChange = (event, value) => {
        console.log(value);
        this.setState({ value });
    };
    async componentDidMount() {
        const order = await getOrderById(this.props.id);
        console.log("single product in component", order.data[0]);
        this.setState({
            data: order.data[0],
            address: order.data[0].address,
            OrderDetails: order.data[0].OrderDetails,
            status:order.data[0].status,
            customerData : order.data[0].user,
            is_loading:false
        });
        console.log("OrderDetails", this.state.OrderDetails)
        console.log("thi state", this.state)
        order.data.map(n => {
            this.setState({
                total_value: parseFloat(n.total_amount) + parseFloat(this.state.total_value)
                
            });
        });
    }
    onConfirm = () => {
        this.setState({save_status: false});
    }


    render() {
        const { data, value, category, subCategory, unit, OrderDetails,is_loading } = this.state;

        return (

            <div>
               
                <AppBar className="bg-primary" position="static">
                    <Tabs value={value} onChange={this.handleChange} scrollable scrollButtons="on">
                        <Tab className="tab" label="Order Details" />
                        <Tab className="tab" label="Shipping Address" />
                        <Tab className="tab" label="Order Status" />
                        <Tab className="tab" label="User Details" />
                    </Tabs>
                </AppBar>
                {value === 0 &&
                    <TabContainer>
                       <div className="center-align">  {is_loading && <img src="https://nmsa.dac.gov.in/images/loader.gif" width="100px"></img>}</div> 
                         {!is_loading &&
                        <div class="order center-align order_table">

                {this.state.status==0 &&   <b className="text-white badge badge-info lg">Order Placed</b>} 
                {this.state.status==1 &&   <b className="text-white badge badge-danger">Order Cancelled</b>} 
                {this.state.status==2 &&   <b className="text-white badge badge-warning">Order Approved</b>} 
                {this.state.status==3 &&   <b className="text-white badge badge-warning">Packed</b>}  
                {this.state.status==4 &&   <b className="text-white badge badge-success lg">Shipped</b>}  
                {this.state.status==5 &&   <b className="text-white badge badge-success">Delivered</b> } 

                            <div> <h2 className="left">ORDER ID  : {this.props.id}</h2>   <h2 className="right">Amount: $ {Number(this.state.total_value).toFixed(2)}</h2> <h2 className="center-align">Shipping: $ {Number(data.shippingCharge).toFixed(2)}</h2></div>
                            <table className="order_table " align="center">
                                <tr>
                                    <th>Product ID</th>
                                    <th>Product name</th>
                                    <th>Product Price</th>
                                    <th>Product Quantity</th>
                                    <th>Sub Total</th>
                                </tr>
                                
                                {OrderDetails.map((n, index) => {
                                    console.log(n);

                                    return (
                                        <tr>
                                            <td>{n.productData[0].product_code}</td>
                                            <td>{n.productData[0].prodName}</td>
                                            <td>{n.price}</td>
                                            <td>{n.quantity}</td>
                                            <td>$ {n.sub_total}</td>
                                        </tr>
                                    )

                                })}

                            </table>
                        </div>
                         }
                    </TabContainer>}
                {value === 1 &&
                    <TabContainer>
                        <div >
                            <h2 className="left">Shipping Address</h2>
                            <br></br>
                            <br></br>
                            {this.state.address.address!="" &&
                            <div className="border">
                                <div>Address: {this.state.address.address}</div> <br />
                                <div>Additional Details : {this.state.address.additionalDetails}</div> <br />
                                <div>ZipCode : {this.state.address.zipcode}</div>
                            </div>
                            }


                        </div>
                    </TabContainer>}
                {value === 2 &&
                    <TabContainer>
                          <div className="col-md-12 col-12 border">
                        <div className="row " >

                           <div> <h2>Update Status <br/></h2></div>
                            
                                <Select
                                    value={this.state.status}
                                    fullWidth
                                    input={<Input id="type" />}
                                    name="status"
                                    onChange={this.handleInputChange}>
                                      <MenuItem value={0}><img src="https://oticaoutlet.online/wp-content/plugins/woocommerce-order-tracker/assets/images/placed.png" width="30px"/>&nbsp; Order Placed</MenuItem>
                                    <MenuItem value={2}> <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ3yQlB1HIZ1SgjQu8-NVUutZlEfbSqyNfABJ-G5-kItSXbbsfe" width="30px"/> &nbsp; Order Approved</MenuItem>
                                    <MenuItem value={3}> <img src="https://cdn.connox.com/m/100107/183648/media/Hilfeseiten-2014/icons/versand-lieferung.jpg" width="30px"/> &nbsp; Packed</MenuItem>
                                    <MenuItem value={4}> <img src="http://www.midwestdocumentshredding.com/wp-content/uploads/2013/08/delivery-256.png" width="30px"/> &nbsp; Shipped</MenuItem>
                                    <MenuItem value={5}> <img src="https://img.icons8.com/color/1600/shipped.png" width="30px"/> &nbsp; Delivered</MenuItem>
                                    <MenuItem value={1}> <img src="http://d2dzjyo4yc2sta.cloudfront.net/?url=images.pitchero.com%2Fui%2F347083%2F1377848325_0.jpg&w=820&t=fit&q=85" width="30px"/>&nbsp; Cancelled</MenuItem>
                                </Select>
                        </div>
                        <div className="col-md-12 col-12">
                            <Button variant="raised" type="submit" color="primary" className="jr-btn  text-white" onClick={this.updateCategory} >Update Status</Button>
                        </div>
                        </div>
                    </TabContainer>}
                    {value === 3 &&
                    <TabContainer>
                       <div className=""> 

                       <div className=" font-class   col-md-12">
                                <div>Customer ID: &nbsp; <Link to={`/app/customer/single?id=${this.state.customerData._id}`} params={{ id: this.state.customerData._id}}> {this.state.customerData.user_id}</Link></div>
                            </div><br/>
                       <div className=" font-class   col-md-12">
                                <div>Customer Name: &nbsp; {this.state.customerData.full_name}</div>
                            </div><br/>
                            <div className=" font-class   col-md-12">
                                <div>Customer Mobile: &nbsp; {this.state.customerData.mobile_number}</div>
                            </div><br/>
                            <div className=" font-class   col-md-12">
                                <div>Profile picture: <br/><br/><img src={this.state.customerData.profile_picture}/></div>
                            </div>
                        </div>
                    </TabContainer>}

                    <SweetAlert show={this.state.save_status} success title="Success!" onConfirm={this.onConfirm}>
                        Order Status has been Updated. 
                    </SweetAlert>
    
            </div>

        );
    }
}

export default SingleOrder;