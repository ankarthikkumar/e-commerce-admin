import React from 'react';
import CardBox from "components/CardBox/index";
import PropTypes from 'prop-types';
import TextField from 'material-ui/TextField';
import Button from 'material-ui/Button';
import RaisedButton from 'material-ui/Button';
import Switch from 'material-ui/Switch';
import { saveCategory } from '../../actions/Category';
import { updateCategory } from '../../actions/Category';
import { getSingleCategory } from '../../actions/Category';
import { awsFileUpload } from '../../actions/fileUpload';
import CircularProgress from '@material-ui/core/CircularProgress';
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';
import SweetAlert from 'react-bootstrap-sweetalert';
class AddSuperCategory extends React.Component {

    onOptionMenuSelect = event => {
        this.setState({ menuState: true, anchorEl: event.currentTarget });
    };
    handleRequestClose = () => {
        this.setState({ menuState: false });
    };
    handleselectedFile = async event =>  {
        this.setState({
            is_upload: true,
            loading : true,
        })
        console.log(event.target.value);
        const data = new FormData()
        data.append('media', event.target.files[0])
        console.log("data",data);
        console.log("--------------------------------------------------");
        const fileupload  = await awsFileUpload (data);
        console.log(fileupload);
        console.log("--------------------------------------------------");
        this.setState({
            is_upload: false,
            loading :false,
            CategoryImage : fileupload.file
        })
      }

     async componentDidMount() {
        var id = this.props.props.location.search;
        var current_id= id.split('&id=')[1];
        console.log("current_id",current_id);
        if(current_id!=undefined){
            var response = await getSingleCategory(current_id);
            var categoryData = response.data[0];
            console.log("categoryData",categoryData)
            this.setState({ is_edit: true, categoryId : current_id , categoryName:categoryData.categoryName, categoryDescription:categoryData.categoryDescription,CategoryImage:categoryData.categoryImage,CategoryStatus:categoryData.categoryStatus});
           
        }
        console.log(this.state);
        // console.log(this.props.search, this.props);
    }

    handleInputChange = (event) => {
        const value = event.target.value;
        const name = event.target.name;

        this.setState({ [name]: value });
    }
    handleCategoryStatus = (event) => {
        this.setState({ CategoryStatus: !this.state.CategoryStatus });
    }
    saveCategory = async () => {
        const data = Object.assign({}, { categoryImage:this.state.CategoryImage,categoryName: this.state.categoryName, CategoryStatus: this.state.CategoryStatus, categoryDescription: this.state.categoryDescription })
     
        if(this.state.CategoryImage==""){
            alert("please upload image");
            return false
        }
        console.log(data);
        if(this.state.is_edit==false){
        var response = await saveCategory(data);
        }else{
            var response = await updateCategory(this.state.categoryId,data);
        }
        console.log("response", response);
        if (response.status == true) {
            this.setState({
                success:true
            });
        }
    }
    onConfirm = (event) => {
        this.setState({ success: false });
        console.log(this.props.props)
        this.props.props.history.push('/app/super_category/list');
    }

    constructor() {
        super();
        this.state = {
            anchorEl: undefined,
            menuState: false,
            categoryName: '',
            CategoryImage: '',
            CategoryStatus: false,
            categoryDescription: '',
            is_upload:false,
            is_edit:false,
            categoryId:'',
            loading :false,
            success:false

        }
    }

    render() {
        // console.log("simple fun",this.props);
        // alert("Add Category")
        const { anchorEl, menuState ,loading} = this.state;
        return (
            <div className="dashboard animated slideInUpTiny animation-duration-3">
                <h2>Super Category</h2>


                <CardBox styleName="col-lg-12" childrenStyle="d-flex" heading="Add Super Category ">
                    <ValidatorForm
                        ref="form"
                        className="col-lg-12"
                        entype="multipart/form-data"
                        onSubmit={this.saveCategory}
                        onError={errors => console.log(errors)}
               
                    >


                        <div className="col-md-12 col-12">
                            <TextValidator
                                id="name"
                                label="Category Name"
                                value={this.state.categoryName}
                                name="categoryName"
                                onChange={this.handleInputChange}
                                fullWidth
                                validators={['required']}
                                errorMessages={['this field is required']}
                            />
                        </div>

                        <div className="col-md-12 col-12">

                            <TextValidator
                                id="multiline-static"
                                label="Description"
                                multiline
                                rows="4"
                                defaultValue=""
                                value={this.state.categoryDescription}
                                name="categoryDescription"
                                onChange={this.handleInputChange}
                                fullWidth

                            />
                        </div>
                        <div className="col-md-12 col-12">
                            <label className="mtop">Category Status</label>
                            <Switch
                                classes={{
                                    checked: 'text-primary',
                                    bar: 'bg-primary',
                                }}
                                
                                label="Super Category Status"
                                checked={this.state.CategoryStatus}
                                aria-label="Super Category Status"
                                value={this.state.CategoryStatus}
                                name="CategoryStatus"
                                onChange={this.handleCategoryStatus}

                            />
                        </div>
                        <br />
                        <div className="col-md-12 col-12">
                            <label>Category  Featured Image</label>
                            <Button
                                variant="raised"
                                component="label"
                                color="primary"
                                className="custom-btn"
                                
                            >
                                Upload File
  <input type="file" name="categoryImage" onChange={this.handleselectedFile}  style={{ display: "none" }}
                                validators={['required']}
                                errorMessages={['this field is required']}  />
                            </Button>
                            <br />


                        </div>
                        <div className="col-md-4 col-12">
                        <br/>
                        <img src={this.state.CategoryImage} className="col-md-12"></img>
                        </div>
                        <div className="col-md-12 col-12">
                        {loading && <CircularProgress/>}
                            <Button variant="raised" type="submit" color="primary" className={this.state.is_upload==true ? "hidden" : "jr-btn  text-white"}>Save Category</Button>
                        </div>
                    </ValidatorForm>
                </CardBox>
                <SweetAlert show={this.state.success} success title="Success!" onConfirm={this.onConfirm}>
                        Super Category Category Saved Sucussfully
                    </SweetAlert>
            </div>
        )
    }
}
export default AddSuperCategory;