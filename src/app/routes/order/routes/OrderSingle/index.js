import React from 'react';
import SingleOrder from "components/Order/SingleOrder";

class OrderSingle extends React.Component {

    onOptionMenuSelect = event => {
        this.setState({ menuState: true, anchorEl: event.currentTarget });
    };
    handleRequestClose = () => {
        this.setState({ menuState: false });
    };

    constructor() {
        super();
        this.state = {
            anchorEl: undefined,
            menuState: false,
        }
    }

    render() {
      
        const { anchorEl, menuState } = this.state;
        return (
            <div style={{width: '100%', height: '100%'}}>
            <SingleOrder id={this.props.match.params.id} />
            </div>
                )
    }
}
          
export default OrderSingle;