import React from 'react';
import Button from 'material-ui/Button';
import Input, { InputLabel } from 'material-ui/Input';
import TextField from 'material-ui/TextField';
import {Link, withRouter} from 'react-router-dom';
import { MenuItem } from 'material-ui/Menu';
import Dialog, { DialogActions, DialogContent, DialogContentText, DialogTitle, } from 'material-ui/Dialog';
import { DatePicker } from 'material-ui-pickers';
// import DateFnsUtils from '@date-io/date-fns';
import { MuiPickersUtilsProvider } from 'material-ui-pickers';
import {
    Area,
    AreaChart,
    Bar,
    BarChart,
    Cell,
    Line,
    LineChart,
    Pie,
    PieChart,
    ResponsiveContainer,
    Tooltip,
    XAxis,
    YAxis,
    CartesianGrid,
    Brush
} from 'recharts';
import {
    announcementsNotification,
    appNotification,
    article,
    authors,
    chartData,
    lineChartData,
    marketingData,
    pieChartData
} from './data';
import SweetAlert from 'react-bootstrap-sweetalert';
import { getOrderAdmin } from '../../actions/Order';
import {getProduct} from '../../actions/Product';
import ReportBox from 'components/ReportBox/index';
import Select from 'material-ui/Select';
import {getCategory} from '../../actions/Category'
const COLORS = ['#0088FE', '#00C49F', '#FFBB28', '#FF8042'];

class FinancialReport extends React.Component {

    state = {
        orderData: [],
        histohour: '',
        histoday: '',
        histominute: '',
        success: false,
        admin: {},
        anchorEl: undefined,
        menuState: false,
        buyRate: '',
        sellRate: '',
        buyTransactionFee: '',
        sellTransactionFee: '',
        sendTransactionFee: '',
        stellarAddress: '',
        stellarSeed: '',
        stellarBalance: 0,
        fiatBalance: 0,
        currentRate: 0,
        userCount: 0,
        stripeKey: '',
        copied: false,
        copiedStellarSeed: false,
        open: false,
        fromDate: '',
        toDate:'',
        data:[],
        product:'',
        category:'',
        categoryData:[],
        status:""

    }

    async componentDidMount() {
        const order = await getOrderAdmin();
        const product = await getProduct();
        const productData = product.data;
        console.log(productData);
        this.setState({
            data: productData,
            orderData :order.data
        });
        const category= await getCategory();
        this.setState({
            categoryData: category.data
        });
        console.log("categoryData",this.state.categoryData);
    }


    handleInputChange = (event) => {
        const value = event.target.value;
        const name = event.target.name;

        this.setState({ [name]: value });
    }

    onConfirm = () => {
        this.setState({ success: false });
    }
    handleClickOpen = (type) => {
        this.setState({ open: type });
    };
    handleRequestClose = (event) => {
        this.setState({ open: false, menuState: false, is_edit: false, zip_code: '' });
    };
    handleDateChange = date => {
        this.setState({ fromDate: date });
    };

    render() {
        const { anchorEl, menuState, fromDate,data,categoryData ,orderData} = this.state;
        return (
            <div className="app-wrapper app-wrapper-module" >
                <div className="dashboard animated slideInUpTiny animation-duration-3">

                    <div className="row">
                        <div className="col-lg-5  col-12">
                            <ReportBox
                                styleName="bg-teal accent-4 text-white"
                                title={Number(this.state.stellarBalance).toFixed(2)}
                                detail="Total Revenue"
                                subHeadingColor="text-white"
                            >
                                <BarChart data={chartData} maxBarSize={7}
                                    margin={{ left: 0, right: 10, top: 10, bottom: 10 }}>
                                    <Bar dataKey='amt' fill='white' />
                                </BarChart>
                            </ReportBox>
                        </div>
                        <div className="col-lg-5  col-12">
                            <ReportBox
                                styleName="bg-red text-white"
                                title={this.state.fiatBalance}
                                detail="Total earnings"
                                subHeadingColor="text-white"
                            >
                                <PieChart>
                                    <Pie dataKey="amt" data={pieChartData} cx="50%" cy="50%" innerRadius={30}
                                        outerRadius={45}
                                        fill="#ffc658" />
                                    <Tooltip />
                                </PieChart>
                            </ReportBox>
                        </div>

                        <div className="col-lg-5  col-12">
                            <ReportBox
                                styleName="bg-purple text-white"
                                title={this.state.userCount}
                                detail="Last Year Earnings"
                                subHeadingColor="text-white"
                            >
                                <LineChart data={lineChartData} margin={{ left: 5, right: 10, top: 0, bottom: 0 }}>
                                    <Line dataKey='amt' stroke='white' />
                                </LineChart>
                            </ReportBox>
                        </div>
                        <div className="col-lg-5  col-12">
                            <ReportBox
                                styleName="bg-red text-white"
                                title="0"
                                detail="Current Year Earnings"
                                subHeadingColor="text-white"
                            >
                                <PieChart onMouseEnter={this.onPieEnter}>
                                    <Pie dataKey="amt"
                                        data={pieChartData} cx="50%" cy="50%"
                                        innerRadius={30}
                                        outerRadius={45}
                                        fill="#3367d6"
                                        paddingAngle={5}
                                    >
                                        {
                                            pieChartData.map((entry, index) => <Cell key={index}
                                                fill={COLORS[index % COLORS.length]} />)
                                        }
                                    </Pie>
                                </PieChart>
                            </ReportBox>
                        </div>
                    </div>
                </div>
              
            </div>

        );
    }
}

export default FinancialReport;