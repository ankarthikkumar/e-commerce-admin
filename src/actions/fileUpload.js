import axios from "axios";
import { constant } from '../constants';



export const awsFileUpload = (request) => {
    return new Promise((resolve, reject) => {
        axios.post(`${constant.API_ENDPOINT}/fileUpload`,request)
        .then((response) => {
            console.log("FileUpload",response.data);
            resolve(response.data)
        })
        .catch((error) => reject(error))
    })
}


