import axios from "axios";
import { constant } from '../constants';

export const reportDate = (request) => {
    return new Promise((resolve, reject) => {
        axios.post(`${constant.API_ENDPOINT}/report_date`,request)
        .then((response) => {
            console.log("reportDate",response.data);
            resolve(response.data)
        })
        .catch((error) => reject(error))
    })
}
export const reportProduct = (request) => {
    return new Promise((resolve, reject) => {
        axios.post(`${constant.API_ENDPOINT}/report_product`,request)
        .then((response) => {
            console.log("reportProduct",response.data);
            resolve(response.data)
        })
        .catch((error) => reject(error))
    })
}

export const reportCategory = (request) => {
    return new Promise((resolve, reject) => {
        axios.post(`${constant.API_ENDPOINT}/report_category`,request)
        .then((response) => {
            console.log("reportCategory",response.data);
            resolve(response.data)
        })
        .catch((error) => reject(error))
    })
}
export const reportStatus = (request) => {
    return new Promise((resolve, reject) => {
        axios.post(`${constant.API_ENDPOINT}/report_status`,request)
        .then((response) => {
            console.log("reportStatus",response.data);
            resolve(response.data)
        })
        .catch((error) => reject(error))
    })
}
export const reportData = (request) => {
    return new Promise((resolve, reject) => {
        axios.post(`${constant.API_ENDPOINT}/reportData`,request)
        .then((response) => {
            console.log("reportData",response.data);
            resolve(response.data)
        })
        .catch((error) => reject(error))
    })
}
export const orderReport = () => {
    return new Promise((resolve, reject) => {
        axios.get(`${constant.API_ENDPOINT}/orderReport`)
        .then((response) => {
            console.log("orderReport",response.data);
            resolve(response.data)
        })
        .catch((error) => reject(error))
    })
}
