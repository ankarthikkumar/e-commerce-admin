import React from 'react';
import {Route, withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import {toggleCollapsedNav} from 'actions/index';

import Header from 'components/Header/index';
import Sidebar from 'containers/SideNav/index';
import Footer from 'components/Footer';
import Dashboard from './routes/dashboard';
import Customer from './routes/customer';
import SuperCategory from './routes/superCategory';
import SubCategory from './routes/subCategory';
import Product from './routes/product';
import zipcode from './routes/zipcode';
import Order from './routes/order';
import PromoCode from './routes/promoCode';
import bannerImage from './routes/bannerImage';
import Reports from './routes/reports';
import Offer from './routes/offer';
import TimeLine from './routes/timeLine';
import CustomViews from './routes/customViews';
import Widgets from './routes/widgets';
import Tour from '../components/Tour/index';

import {COLLAPSED_DRAWER, FIXED_DRAWER} from 'constants/ActionTypes';
import ColorOption from 'containers/Customizer/ColorOption';
import {isIOS, isMobile} from 'react-device-detect';

class App extends React.Component {
    onToggleCollapsedNav = (e) => {
        const val = !this.props.navCollapsed;
        this.props.toggleCollapsedNav(val);
    };

    render() {
        const {match, drawerType} = this.props;
        const drawerStyle = drawerType.includes(FIXED_DRAWER) ? "fixed-drawer" : drawerType.includes(COLLAPSED_DRAWER) ? "collapsible-drawer" : "mini-drawer";

        //set default height and overflow for iOS mobile Safari 10+ support.
        if (isIOS && isMobile) {
            $('#body').addClass('ios-mobile-view-height')
        }
        else if ($('#body').hasClass('ios-mobile-view-height')) {
            $('#body').removeClass('ios-mobile-view-height')
        }

        return (
            <div className={`app-container ${drawerStyle}`}>
                <Tour/>

                <Sidebar onToggleCollapsedNav={this.onToggleCollapsedNav.bind(this)}/>
                <div className="app-main-container">
                    <div className="app-header">
                        <Header drawerType={drawerType} onToggleCollapsedNav={this.onToggleCollapsedNav}/>
                    </div>

                    <main className="app-main-content-wrapper">
                        <div className="app-main-content">
                            <Route path={`${match.url}/dashboard`} component={Dashboard}/>
                            <Route path={`${match.url}/customer`} component={Customer}/>
                            <Route path={`${match.url}/super_category`} component={SuperCategory}/>
                            <Route path={`${match.url}/sub_category`} component={SubCategory}/>
                            <Route path={`${match.url}/order`} component={Order}/>
                            <Route path={`${match.url}/product`} component={Product}/>
                            <Route path={`${match.url}/zipcode`} component={zipcode}/>
                            <Route path={`${match.url}/offer`} component={Offer}/>
                            <Route path={`${match.url}/banner`} component={bannerImage}/>
                            <Route path={`${match.url}/report`} component={Reports}/>
                            <Route path={`${match.url}/promocode`} component={PromoCode}/>
                           
                        </div>
                        <Footer/>
                    </main>
                </div>
                <ColorOption/>
            </div>
        );
    }
}


const mapStateToProps = ({settings}) => {
    const {navCollapsed, drawerType} = settings;
    return {navCollapsed, drawerType}
};
export default withRouter(connect(mapStateToProps, {toggleCollapsedNav})(App));