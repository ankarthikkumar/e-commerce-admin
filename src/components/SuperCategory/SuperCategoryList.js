import React from 'react';
import {Link, withRouter} from 'react-router-dom';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import keycode from 'keycode';
import SweetAlert from 'react-bootstrap-sweetalert';
import Table, {
    TableBody,
    TableCell,
    TableFooter,
    TableHead,
    TablePagination,
    TableRow,
    TableSortLabel,
} from 'material-ui/Table';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';
import Paper from 'material-ui/Paper';
import IconButton from 'material-ui/IconButton';
import Tooltip from 'material-ui/Tooltip';
import DeleteIcon from 'material-ui-icons/Delete';
import {ButtonDropdown, ButtonGroup, DropdownItem, DropdownMenu, DropdownToggle} from 'reactstrap';
import Button from 'material-ui/Button';
import {getCategory, updateCategory} from '../../actions/Category'
import Menu, { MenuItem } from 'material-ui/Menu';
let counter = 0;

function createData(index,cat_id, c_id,name, sub_cat_cnt, prod_cnt, image,status,action) {
    return {id: index+1, cat_id, c_id,name, sub_cat_cnt, prod_cnt, image,status,action};
}
const ITEM_HEIGHT = 40;
const columnData = [
    {id: 'SNo',  disablePadding: true, label: 'SNo'},
    {id: 'Id',  disablePadding: false, label: 'ID'},
    {id: 'Name',  disablePadding: false, label: 'Category Name'},
    {id: 'Subcategry count',  disablePadding: false, label: 'Subcategory count'},    
    {id: 'Product count',  disablePadding: false, label: 'Product count'},
    {id: 'image',  disablePadding: false, label: 'Image'},
    {id: 'status',  disablePadding: false, label: 'Status'},
    {id: 'action',  disablePadding: false, label: 'Actions'},
];

class DataTableHead extends React.Component {
    static propTypes = {
        numSelected: PropTypes.number.isRequired,
        onRequestSort: PropTypes.func.isRequired,
        onSelectAllClick: PropTypes.func.isRequired,
        order: PropTypes.string.isRequired,
        orderBy: PropTypes.string.isRequired,
        rowCount: PropTypes.number.isRequired,
    };
    createSortHandler = property => event => {
        this.props.onRequestSort(event, property);
    };
    render() {
        const {onSelectAllClick, order, orderBy, numSelected, rowCount} = this.props;
        return (
            <TableHead>
                <TableRow>
                    {columnData.map(column => {
                        return (
                            <TableCell
                           margin="10px"
                                key={column.id}
                                padding={column.disablePadding ? 'default' : 'default'}
                            >
                                <Tooltip
                                    title="Sort"
                                    placement={column.numeric ? 'bottom-end' : 'bottom-start'}
                                    enterDelay={300}
                                >
                                    <TableSortLabel
                                        active={orderBy === column.id}
                                        direction={order}
                                        onClick={this.createSortHandler(column.id)}
                                    >
                                        {column.label}
                                    </TableSortLabel>
                                </Tooltip>
                            </TableCell>
                        );
                    }, this)}
                </TableRow>
            </TableHead>
        );
    }
}


let DataTableToolbar = props => {
    const {numSelected} = props;

    return (
        <Toolbar
            className={classNames("table-header", {
                ["highlight-light"]: numSelected > 0,
            })}
        >
            <div className="title">
                {numSelected > 0 ? (
                    <Typography type="subheading">{numSelected} selected</Typography>
                ) : (
                    <Typography type="title">Super Categories</Typography>
                )}
            </div>
            <div className="spacer" />
            <div className="actions">
                {numSelected > 0 && (
                    <Tooltip title="Delete">
                        <IconButton aria-label="Delete">
                            <DeleteIcon />
                        </IconButton>
                    </Tooltip>
                )}
            </div>
        </Toolbar>
    );
};

DataTableToolbar.propTypes = {
    numSelected: PropTypes.number.isRequired,
};


class SubCategorylist extends React.Component {
    handleRequestSort = (event, property) => {
        const orderBy = property;
        let order = 'desc';

        if (this.state.orderBy === property && this.state.order === 'desc') {
            order = 'asc';
        }

        const data =
            order === 'desc'
                ? this.state.data.sort((a, b) => (b[orderBy] < a[orderBy] ? -1 : 1))
                : this.state.data.sort((a, b) => (a[orderBy] < b[orderBy] ? -1 : 1));

        this.setState({data, order, orderBy});
    };
    handleSelectAllClick = (event, checked) => {
        if (checked) {
            this.setState({selected: this.state.data.map(n => n.id)});
            return;
        }
        this.setState({selected: []});
    };
    handleKeyDown = (event, id) => {
        if (keycode(event) === 'space') {
            this.handleClick(event, id);
        }
    };
    
    handleChangePage = (event, page) => {
        this.setState({page});
    };
    handleChangeRowsPerPage = event => {
        this.setState({rowsPerPage: event.target.value});
    };

    onOptionMenuSelect = (n,event) => {
        this.setState({ menuState: true, anchorEl: event.currentTarget,subCatId:n.c_id});
    };
  

    
    handleRequestClose = (event) => {
        this.setState({ open: false,menuState:false, anchorEl: event.currentTarget });
    };

  
   
    isSelected = id => this.state.selected.indexOf(id) !== -1;

    async componentDidMount() {
       this.loadData();
    }

    loadData  = async() =>{
        const category = await getCategory();
        console.log("category in component",category.data);
        const categoryData = category.data;
        this.setState({
            data: await categoryData.map((item, index) => createData(index,item.category_code,item._id,item.categoryName, item.subCategory.length,item.product_count,item.categoryImage,item.isDeleted,'Edit')) 
        });
    }

    DisableCategory = async (data) => {
        console.log(data);
        var action = data.status ==1 ? 0 :1;
        var dbUpdate = Object.assign({}, { isDeleted: action})
        console.log(dbUpdate);
        var response = await updateCategory(data.c_id,dbUpdate);
        if (response.status == true) {
            this.setState({success:true});
            this.loadData();
        }
    };

    onConfirm = (event) => {
        this.setState({ success: false });
    }


    constructor(props, context) {
        super(props, context);
       
        this.state = {
            order: 'asc',
            orderBy: 'calories',
            selected: [],
            data: [],
            page: 0,
            rowsPerPage: 10,
            subCatId:'',
            success:false
        };
    }

    render() {
        // console.log('this.', this.state.data)
        const {data, order, orderBy, selected, rowsPerPage, page,anchorEl ,menuState,handleRequestClose } = this.state;

        return (
            <div className="dashboard animated slideInUpTiny animation-duration-3">
        <h2>Super Category List < Link to="/app/super_category/add" ><Button variant="raised" className="bg-primary text-white right" >Add Super Category</Button></Link></h2>
            <br/>
            <Paper>
                <DataTableToolbar numSelected={selected.length} />
                <div className="flex-auto">
                    <div className="table-responsive-material">
                        <Table className="">
                            <DataTableHead
                                numSelected={selected.length}
                                order={order}
                                orderBy={orderBy}
                                onSelectAllClick={this.handleSelectAllClick}
                                onRequestSort={this.handleRequestSort}
                                rowCount={data.length}
                            />
                            <TableBody>
                                {data.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map(n => {
                                    return (
                                        <TableRow
                                            hover
                                            
                                            onKeyDown={event => this.handleKeyDown(event, n.id)}
                                            
                                            tabIndex={-1}
                                            key={n.id}
                                        >

                                            <TableCell padding="default">
                                            {n.id}
                                            </TableCell>
                                            <TableCell padding="default">{n.cat_id}</TableCell>
                                            <TableCell  padding="default">{n.name}</TableCell>
                                            <TableCell  padding="default">{n.sub_cat_cnt}</TableCell>
                                            <TableCell  padding="default">{n.prod_cnt}</TableCell>
                                            <TableCell  padding="default"><img src={n.image} width="25px" height="25px"></img></TableCell>
                                            <TableCell  padding="default">{n.status==true?'Deactivated':'Active'}</TableCell>
                                            <TableCell  padding="default">    
                                            {/* <IconButton className="size-30" >
                                                        <i className="zmdi custom_icon_size zmdi-eye" /></IconButton>
                                                    &nbsp; */}
                                                    <Link to={`/app/super_category/add?action=edit&id=${n.c_id}`} params={{ id: n.c_id,action:"edit" }}> <IconButton className="size-30" >
                                                        <i className="zmdi custom_icon_size zmdi-edit" /></IconButton></Link>
                                                        &nbsp;
                                                        <IconButton className="size-30"  onClick={()=> this.DisableCategory(n)}>
                                                        <i className="zmdi custom_icon_size zmdi-lock" /></IconButton>

                                                    </TableCell>
                                            
                                        </TableRow>
                                    );
                                })}
                            </TableBody>
                            <TableFooter>
                                <TableRow>
                                    <TablePagination
                                        count={data.length}
                                        rowsPerPage={rowsPerPage}
                                        page={page}
                                        onChangePage={this.handleChangePage}
                                        onChangeRowsPerPage={this.handleChangeRowsPerPage}
                                    />
                                </TableRow>
                            </TableFooter>
                        </Table>
                    </div>
                </div>
            </Paper>
            <SweetAlert show={this.state.success} success title="Success!" onConfirm={this.onConfirm}>
                        Super Category Status Updated
                    </SweetAlert>
            </div>
        );
    }
}

export default SubCategorylist;