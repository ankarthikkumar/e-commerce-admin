import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import keycode from 'keycode';
import {Link, withRouter} from 'react-router-dom';
import Table, {
    TableBody,
    TableCell,
    TableFooter,
    TableHead,
    TablePagination,
    TableRow,
    TableSortLabel,
} from 'material-ui/Table';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';
import Paper from 'material-ui/Paper';
import IconButton from 'material-ui/IconButton';
import Tooltip from 'material-ui/Tooltip';
import DeleteIcon from 'material-ui-icons/Delete';
import {ButtonDropdown, ButtonGroup, DropdownItem, DropdownMenu, DropdownToggle} from 'reactstrap';
import Button from 'material-ui/Button';
import CardBox from "components/CardBox/index";
import { getUser } from '../../actions/Customer'
let counter = 0;

function createData(index,c_id,cust_id, name, email, mobile, orders,status) {
    
    return {id: index+1, c_id,cust_id, name, email, mobile, orders,status};
}

const columnData = [
    {id: 'SNo',  disablePadding: true, label: 'SNo'},
    {id: 'Customer Id',  disablePadding: false, label: 'ID'},
    {id: 'Customer name',  disablePadding: false, label: 'Customar Name'},
    {id: 'Email',  disablePadding: false, label: 'Email'},
    {id: 'mobile',  disablePadding: false, label: 'Mobile'},
    {id: 'orders',  disablePadding: false, label: 'Total Orders'},
    {id: 'status',  disablePadding: false, label: 'Status'},
    {id: 'action',  disablePadding: false, label: 'Actions'},
];

class DataTableHead extends React.Component {
    static propTypes = {
        numSelected: PropTypes.number.isRequired,
        onRequestSort: PropTypes.func.isRequired,
        onSelectAllClick: PropTypes.func.isRequired,
        order: PropTypes.string.isRequired,
        orderBy: PropTypes.string.isRequired,
        rowCount: PropTypes.number.isRequired,
    };

    createSortHandler = property => event => {
        this.props.onRequestSort(event, property);
    };

    render() {
        const {onSelectAllClick, order, orderBy, numSelected, rowCount} = this.props;

        return (
            <TableHead>
                <TableRow>
                    
                    {columnData.map(column => {
                        return (
                            <TableCell
                           margin="10px"
                                key={column.id}
                                padding={column.disablePadding ? 'default' : 'default'}
                            >
                                <Tooltip
                                    title="Sort"
                                    placement={column.numeric ? 'bottom-end' : 'bottom-start'}
                                    enterDelay={300}
                                >
                                    <TableSortLabel
                                        active={orderBy === column.id}
                                        direction={order}
                                        onClick={this.createSortHandler(column.id)}
                                    >
                                        {column.label}
                                    </TableSortLabel>
                                </Tooltip>
                            </TableCell>
                        );
                    }, this)}
                </TableRow>
            </TableHead>
        );
    }
}


let DataTableToolbar = props => {
    const {numSelected} = props;

    return (
        <Toolbar
            className={classNames("table-header", {
                ["highlight-light"]: numSelected > 0,
            })}
        >
            <div className="title">
                {numSelected > 0 ? (
                    <Typography type="subheading">{numSelected} selected</Typography>
                ) : (
                    <Typography type="title">Customers List</Typography>
                )}
            </div>
            <div className="spacer" />
            <div className="actions">
                {numSelected > 0 && (
                    <Tooltip title="Delete">
                        <IconButton aria-label="Delete">
                            <DeleteIcon />
                        </IconButton>
                    </Tooltip>
                )}
            </div>
        </Toolbar>
    );
};

DataTableToolbar.propTypes = {
    numSelected: PropTypes.number.isRequired,
};


class CustomerList extends React.Component {
    handleRequestSort = (event, property) => {
        const orderBy = property;
        let order = 'desc';

        if (this.state.orderBy === property && this.state.order === 'desc') {
            order = 'asc';
        }

        const data =
            order === 'desc'
                ? this.state.data.sort((a, b) => (b[orderBy] < a[orderBy] ? -1 : 1))
                : this.state.data.sort((a, b) => (a[orderBy] < b[orderBy] ? -1 : 1));

        this.setState({data, order, orderBy});
    };
    handleSelectAllClick = (event, checked) => {
        if (checked) {
            this.setState({selected: this.state.data.map(n => n.id)});
            return;
        }
        this.setState({selected: []});
    };
    handleKeyDown = (event, id) => {
        if (keycode(event) === 'space') {
            this.handleClick(event, id);
        }
    };
    
    handleChangePage = (event, page) => {
        this.setState({page});
    };
    handleChangeRowsPerPage = event => {
        this.setState({rowsPerPage: event.target.value});
    };

    
    async componentDidMount() {
        const user = await getUser();
        console.log("getUser in component",user.data);
        const userData = user.data;
        this.setState({
            data: await userData.map((item, index) => createData(index,item._id,item.user_id,item.full_name, item.email,item.mobile_number,item.order_count,'Active','View')) 
        });
        console.log("data ",this.state.data);        
    }


    isSelected = id => this.state.selected.indexOf(id) !== -1;

    constructor(props, context) {
        super(props, context);

        this.state = {
            order: 'asc',
            orderBy: 'calories',
            selected: [],
            data: [],
            page: 0,
            rowsPerPage: 10,
        };
    }

    render() {
        const {data, order, orderBy, selected, rowsPerPage, page} = this.state;

        return (
            <div className="dashboard animated slideInUpTiny animation-duration-3">
            <h2>Customers Management <Link to="/app/customer/add" ><Button variant="raised" className="bg-primary text-white right" >Add Customer</Button></Link></h2>
            <br/>

          
            <Paper>
                <DataTableToolbar numSelected={selected.length} />
                <div className="flex-auto">
                    <div className="table-responsive-material">
                        <Table className="">
                            <DataTableHead
                                numSelected={selected.length}
                                order={order}
                                orderBy={orderBy}
                                onSelectAllClick={this.handleSelectAllClick}
                                onRequestSort={this.handleRequestSort}
                                rowCount={data.length}
                            />
                            <TableBody>
                                {data.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map(n => {
                                    return (
                                        <TableRow
                                            hover
                                            
                                            onKeyDown={event => this.handleKeyDown(event, n.id)}
                                            
                                            tabIndex={-1}
                                            key={n.id}
                                        >
                                            <TableCell padding="default">
                                            {n.id}
                                            </TableCell>
                                            <TableCell padding="default">{n.cust_id}</TableCell>
                                            <TableCell  padding="default">{n.name}</TableCell>
                                            <TableCell  padding="default">{n.email}</TableCell>
                                            <TableCell  padding="default">{n.mobile}</TableCell>
                                            <TableCell  padding="default">{n.orders}</TableCell>
                                            <TableCell  padding="default">{n.status}</TableCell>
                                            <TableCell> 

                                            <Link to={`/app/customer/single?id=${n.c_id}`} params={{ id: n.c_id}}><IconButton className="size-30" >
                                                        <i className="zmdi custom_icon_size zmdi-eye" /></IconButton></Link>
                                                    &nbsp;
                                                    <Link to={`/app/customer/add?action=edit&id=${n.c_id}`} params={{ id: n.c_id,action:"edit" }}> <IconButton className="size-30" >
                                                        <i className="zmdi custom_icon_size zmdi-edit" /></IconButton></Link>
                                                        &nbsp;
                                                        {/* <IconButton className="size-30" >
                                                        <i className="zmdi custom_icon_size zmdi-lock" /></IconButton> */}
                                            </TableCell>
                                        </TableRow>
                                    );
                                })}
                            </TableBody>
                            <TableFooter>
                                <TableRow>
                                    <TablePagination
                                        count={data.length}
                                        rowsPerPage={rowsPerPage}
                                        page={page}
                                        onChangePage={this.handleChangePage}
                                        onChangeRowsPerPage={this.handleChangeRowsPerPage}
                                    />
                                </TableRow>
                            </TableFooter>
                        </Table>
                    </div>
                </div>
            </Paper>
           
            </div>
        );
    }
}

export default CustomerList;