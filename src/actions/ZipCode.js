import axios from "axios";
import { constant } from '../constants';

export const addZipCode = (request) => {
    return new Promise((resolve, reject) => {
        axios.post(`${constant.API_ENDPOINT}/zipcode`,request)
        .then((response) => {
            console.log("zip_code",response.data);
            resolve(response.data)
        })
        .catch((error) => reject(error))
    })
}
export const getZipCode = () => {
    return new Promise((resolve, reject) => {
        axios.get(`${constant.API_ENDPOINT}/zipcode`)
        .then((response) => {
            // console.log("zip_code",response.data);
            resolve(response.data)
        })
        .catch((error) => reject(error))
    })
}

export const EditZipCode = (id,request) => {
    return new Promise((resolve, reject) => {
        axios.put(`${constant.API_ENDPOINT}/zipcode/`+id,request)
        .then((response) => {
            console.log("EditZipCode",response.data);
            resolve(response.data)
        })
        .catch((error) => reject(error))
    })
}



