import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import keycode from 'keycode';
import Table, {
    TableBody,
    TableCell,
    TableFooter,
    TableHead,
    TablePagination,
    TableRow,
    TableSortLabel,
} from 'material-ui/Table';
import {Link, withRouter} from 'react-router-dom';
import SweetAlert from 'react-bootstrap-sweetalert';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';
import Paper from 'material-ui/Paper';
import IconButton from 'material-ui/IconButton';
import Tooltip from 'material-ui/Tooltip';
import DeleteIcon from 'material-ui-icons/Delete';
import {ButtonDropdown, ButtonGroup, DropdownItem, DropdownMenu, DropdownToggle} from 'reactstrap';
import Button from 'material-ui/Button';
import {getOffer, updateOffer} from '../../actions/Offer'
import Menu, { MenuItem } from 'material-ui/Menu';
let counter = 0;
const ITEM_HEIGHT = 40;
function createData(index,cat_id, name, sub_cat_cnt, prod_cnt, image,status,action) {
    return {id: index+1, cat_id, name, sub_cat_cnt, prod_cnt, image,status,action};
}

const columnData = [
    {id: 'SNo',  disablePadding: true, label: 'SNo'},
    {id: 'name',  disablePadding: false, label: 'Offer Name'},
    {id: 'category',  disablePadding: false, label: 'Super category'},
    {id: 'sub_category',  disablePadding: false, label: 'Sub category'},
    {id: 'discount',  disablePadding: false, label: 'Offer Percetage'},
    {id: 'sdate',  disablePadding: false, label: 'Date Started'},
    {id: 'edate',  disablePadding: false, label: 'Date Ended'},
    {id: 'status',  disablePadding: false, label: 'Status'},
    {id: 'action',  disablePadding: false, label: 'Actions'},
];

class DataTableHead extends React.Component {
    static propTypes = {
        numSelected: PropTypes.number.isRequired,
        onRequestSort: PropTypes.func.isRequired,
        onSelectAllClick: PropTypes.func.isRequired,
        order: PropTypes.string.isRequired,
        orderBy: PropTypes.string.isRequired,
        rowCount: PropTypes.number.isRequired,
    };

    createSortHandler = property => event => {
        this.props.onRequestSort(event, property);
    };

    render() {
        const {onSelectAllClick, order, orderBy, numSelected, rowCount} = this.props;

        return (
            <TableHead>
                <TableRow>
                    
                    {columnData.map(column => {
                        return (
                            <TableCell
                           margin="10px"
                                key={column.id}
                                padding={column.disablePadding ? 'default' : 'default'}
                            >
                                <Tooltip
                                    title="Sort"
                                    placement={column.numeric ? 'bottom-end' : 'bottom-start'}
                                    enterDelay={300}
                                >
                                    <TableSortLabel
                                        active={orderBy === column.id}
                                        direction={order}
                                        onClick={this.createSortHandler(column.id)}
                                    >
                                        {column.label}
                                    </TableSortLabel>
                                </Tooltip>
                            </TableCell>
                        );
                    }, this)}
                </TableRow>
            </TableHead>
        );
    }
}


let DataTableToolbar = props => {
    const {numSelected} = props;

    return (
        <Toolbar
            className={classNames("table-header", {
                ["highlight-light"]: numSelected > 0,
            })}
        >
            <div className="title">
                {numSelected > 0 ? (
                    <Typography type="subheading">{numSelected} selected</Typography>
                ) : (
                    <Typography type="title">Offer Lists</Typography>
                )}
            </div>
            <div className="spacer" />
            <div className="actions">
                {numSelected > 0 && (
                    <Tooltip title="Delete">
                        <IconButton aria-label="Delete">
                            <DeleteIcon />
                        </IconButton>
                    </Tooltip>
                )}
            </div>
        </Toolbar>
    );
};

DataTableToolbar.propTypes = {
    numSelected: PropTypes.number.isRequired,
};


class OfferList extends React.Component {
    handleRequestSort = (event, property) => {
        const orderBy = property;
        let order = 'desc';

        if (this.state.orderBy === property && this.state.order === 'desc') {
            order = 'asc';
        }

        const data =
            order === 'desc'
                ? this.state.data.sort((a, b) => (b[orderBy] < a[orderBy] ? -1 : 1))
                : this.state.data.sort((a, b) => (a[orderBy] < b[orderBy] ? -1 : 1));

        this.setState({data, order, orderBy});
    };
    handleSelectAllClick = (event, checked) => {
        if (checked) {
            this.setState({selected: this.state.data.map(n => n.id)});
            return;
        }
        this.setState({selected: []});
    };
    handleKeyDown = (event, id) => {
        if (keycode(event) === 'space') {
            this.handleClick(event, id);
        }
    };
    
    handleChangePage = (event, page) => {
        this.setState({page});
    };
    handleChangeRowsPerPage = event => {
        this.setState({rowsPerPage: event.target.value});
    };
    isSelected = id => this.state.selected.indexOf(id) !== -1;

    async componentDidMount() {
     this.loadData();      
    }
    async loadData(){
        const code = await getOffer();
        const codeData = code.data;
        this.setState({
            data: codeData
        });
        console.log("data ",this.state.data);        
    }
    
   
    onConfirm = (event) => {
        this.setState({ success: false });
    }

    handleDelete = async (Offer) => {
        console.log("Offer",Offer);
       
        var action = Offer.isDeleted ==1 ? 0 :1;
        var data = Object.assign({}, { isDeleted: action})
        var response = await updateOffer(Offer._id,data);
        if (response.status == true) {
            this.setState({success:true,alertText:response.message});
            this.loadData();
        }
    };
    
    constructor(props, context) {
        super(props, context);
       
        this.state = {
            order: 'asc',
            orderBy: 'calories',
            selected: [],
            data: [],
            page: 0,
            rowsPerPage: 10,
            anchorEl:false,
            menuState :false,
            code:'',
            alertText:'',
            success:false,
        };
    }
    handleRequestClose = (event) => {
        console.log("handleRequestClose");
        this.setState({ open: false,menuState:false, anchorEl: event.currentTarget });
    };

    render() {
        console.log('this.', this.state.data)
        const {data, order, orderBy, selected, rowsPerPage, page,anchorEl,menuState,handleRequestClose } = this.state;

        return (
            <div className="dashboard animated slideInUpTiny animation-duration-3">
            <h2>Offer List <Link to="/app/offer/add" ><Button variant="raised" className="bg-primary text-white right" >Add Offer</Button></Link></h2>
            <br/>
            <Paper>
                <DataTableToolbar numSelected={selected.length} />
                <div className="flex-auto">
                    <div className="table-responsive-material">
                        <Table className="">
                            <DataTableHead
                                numSelected={selected.length}
                                order={order}
                                orderBy={orderBy}
                                onSelectAllClick={this.handleSelectAllClick}
                                onRequestSort={this.handleRequestSort}
                                rowCount={data.length}
                            />
                            <TableBody>
                                {data.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((n,index) => {
                                    console.log("n",n);
                                    return (
                                        <TableRow
                                            hover
                                            
                                            onKeyDown={event => this.handleKeyDown(event, n.id)}
                                            
                                            tabIndex={-1}
                                            key={n.id}
                                        >

                                            <TableCell padding="default">
                                            {index+1}
                                            </TableCell>
                                            <TableCell padding="default">{n.offerName}</TableCell>
                                            <TableCell  padding="default">{n.category.categoryName}</TableCell>
                                            <TableCell  padding="default">{n.sub_category}</TableCell>
                                            <TableCell  padding="default">{n.offerValue}</TableCell>
                                            <TableCell  padding="default">{n.startDate}</TableCell>
                                            <TableCell  padding="default">{n.endDate}</TableCell>
                                            <TableCell  padding="default">{n.isDeleted==1?'Disabled':'Enabled'}</TableCell>
                                            <TableCell  padding="default">
                                                        <Link to={`/app/offer/add?action=edit&id=${n.offer_code}`} params={{ id: n.offer_code,action:"edit" }}> <IconButton className="size-30" >
                                                        <i className="zmdi custom_icon_size zmdi-edit" /></IconButton></Link>
                                                        &nbsp;
                                                        <IconButton className="size-30"  onClick={()=> this.handleDelete(n)}>
                                                        <i className="zmdi custom_icon_size zmdi-lock" /></IconButton>
                                                    </TableCell>
                                            
                                        </TableRow>
                                    );
                                })}
                            </TableBody>
                            <TableFooter>
                                <TableRow>
                                    <TablePagination
                                        count={data.length}
                                        rowsPerPage={rowsPerPage}
                                        page={page}
                                        onChangePage={this.handleChangePage}
                                        onChangeRowsPerPage={this.handleChangeRowsPerPage}
                                    />
                                </TableRow>
                            </TableFooter>
                        </Table>
                    </div>
                </div>
            </Paper>
            <SweetAlert show={this.state.success} success title="Success!" onConfirm={this.onConfirm}>
                        {this.state.alertText}
                    </SweetAlert>
            </div>
        );
    }
}

export default OfferList;