import React, {Component} from 'react';
import {Link, withRouter} from 'react-router-dom';
import Button from 'material-ui/Button';
import 'jquery-slimscroll/jquery.slimscroll.min';


class SideNavContent extends Component {
    componentDidMount() {
        const {history} = this.props;
        const $nav = $(this.nav);
        const slideDuration = 250;

        $nav.slimscroll({
            height: '100%'
        });

        const pathname = `#${history.location.pathname}`;// get current path

        $('ul.nav-menu > li.menu').click(function () {
            const menuLi = this;
            $('ul.nav-menu > li.menu').not(menuLi).removeClass('open');
            $('ul.nav-menu > li.menu ul').not($('ul', menuLi)).slideUp(slideDuration);
            $('> ul', menuLi).slideToggle(slideDuration);
            $(menuLi).toggleClass('open');
        });

        $('ul.sub-menu li').click(function (e) {
            let superSubMenu = $(this).parent();
            if (superSubMenu.parent().hasClass('active')) {
                $('li', superSubMenu).not($(this)).removeClass('active');
            }
            else {
                $('ul.sub-menu li').not($(this)).removeClass('active');
            }

            $(this).toggleClass('active');
            e.stopPropagation();
        });

        const activeLi = $('a[to="' + pathname + '"]');// select current a element
        const activeNav = activeLi.closest('ul'); // select closest ul
        if (activeNav.hasClass('sub-menu')) {
            activeNav.slideDown(slideDuration);
            activeNav.parent().addClass('open');
            activeLi.parent().addClass('active');
        } else {
            activeLi.parent().addClass('open');
        }
    }


    render() {
        return (
            <ul className="nav-menu" ref={(c) => {
                this.nav = c;
            }}>

                <li className="nav-header">Menu</li>

                <li className="menu no-arrow">
                    <Link className="prepend-icon" to="/app/dashboard">
                        <i className="zmdi zmdi-view-dashboard zmdi-hc-fw"/>
                        <span className="nav-text">Dashboard</span>
                        </Link>
                </li>


                <li className="ui_tooltip menu">
                    <Button className="void" href="javascript:void(0)">
                    <i class="zmdi zmdi-accounts-list-alt"/>
                        <span className="nav-text">Customer Management</span>
                    </Button>

                    <ul className="sub-menu">
                        <li>
                            <Link className="prepend-icon" to="/app/customer/add">
                            <i class="zmdi zmdi-plus"></i>
                                <span className="nav-text">Add Customer</span>
                            </Link>
                        </li>

                        <li>
                            <Link className="prepend-icon" to="/app/customer/list">
                            <i class="zmdi zmdi-format-list-bulleted"></i>
                                <span className="nav-text">CustomerList</span>
                            </Link>
                        </li>
                       
                            </ul>
                            </li>


                            

                <li className="menu no-arrow">
                    <Link className="prepend-icon" to="/app/product/add">
                    <i class="zmdi zmdi-plus-square"></i>
                        <span className="nav-text">Add Product</span>
                        </Link>
                </li>

                <li className="ui_tooltip menu">
                    <Button className="void" href="javascript:void(0)">
                    <i class="zmdi zmdi-widgets"></i>
                        <span className="nav-text">Super Category</span>
                    </Button>

                    <ul className="sub-menu">
                        <li>
                            <Link className="prepend-icon" to="/app/super_category/add">
                            <i class="zmdi zmdi-plus"></i>
                                <span className="nav-text">Add Super Category</span>
                            </Link>
                        </li>

                        <li>
                            <Link className="prepend-icon" to="/app/super_category/list">
                            <i class="zmdi zmdi-format-list-bulleted"></i>
                                <span className="nav-text">Super Category List</span>
                            </Link>
                        </li>
                       
                            </ul>
                            </li>

                            <li className="ui_tooltip menu">
                    <Button className="void" href="javascript:void(0)">
                    <i class="zmdi zmdi-view-carousel"></i>
                        <span className="nav-text">Sub Category</span>
                    </Button>

                    <ul className="sub-menu">
                        <li>
                            <Link className="prepend-icon" to="/app/sub_category/add">
                            <i class="zmdi zmdi-plus"></i>
                                <span className="nav-text">Add Sub Category</span>
                            </Link>
                        </li>

                        <li>
                            <Link className="prepend-icon" to="/app/sub_category/list">
                            <i class="zmdi zmdi-format-list-bulleted"></i>
                                <span className="nav-text">Sub Category List</span>
                            </Link>
                        </li>
                       
                            </ul>
                            </li>


                            <li className="menu no-arrow">
                    <Link className="prepend-icon" to="/app/order">
                        <i className="zmdi zmdi-shopping-cart zmdi-hc-fw"/>
                        <span className="nav-text">Order Management</span>
                        </Link>
                </li>

                <li className="menu no-arrow">
                    <Link className="prepend-icon" to="/app/product/list">
                    <i class="zmdi zmdi-mall"></i>
                        <span className="nav-text">Product Management</span>
                        </Link>
                </li>

                <li className="menu no-arrow">
                    <Link className="prepend-icon" to="/app/zipcode">
                        <i className="zmdi zmdi-pin zmdi-hc-fw"/>
                        <span className="nav-text">Zip Codes</span>
                        </Link>
                </li>

                <li className="menu no-arrow">
                    <Link className="prepend-icon" to="/app/offer">
                    <i class="zmdi zmdi-label"></i>
                        <span className="nav-text">Offer Management</span>
                        </Link>
                </li>

                <li className="menu no-arrow">
                    <Link className="prepend-icon" to="/app/banner">
                        <i className="zmdi zmdi-collection-image-o zmdi-hc-fw"/>
                        <span className="nav-text">Banner Management</span>
                        </Link>
                </li>

                <li className="ui_tooltip menu">
                    <Button className="void" href="javascript:void(0)">
                    <i class="zmdi zmdi-chart"></i>
                        <span className="nav-text">Reports</span>
                    </Button>

                    <ul className="sub-menu">
                        <li>
                            <Link className="prepend-icon" to="/app/report/sales">
                            <i class="zmdi zmdi-shopping-basket"></i>
                                <span className="nav-text">Sales Report</span>
                            </Link>
                        </li>

                        {/* <li>
                            <Link className="prepend-icon" to="/app/report/financial">
                            <i class="zmdi zmdi-format-list-bulleted"></i>
                                <span className="nav-text">Financial Report</span>
                            </Link>
                        </li> */}
                       
                            </ul>
                            </li>

                            <li className="ui_tooltip menu">
                    <Button className="void" href="javascript:void(0)">
                    <i class="zmdi zmdi-label-heart"></i>
                        <span className="nav-text">Promo Codes</span>
                    </Button>

                    <ul className="sub-menu">
                        <li>
                            <Link className="prepend-icon" to="/app/promocode/add">
                            <i class="zmdi zmdi-plus"></i>
                                <span className="nav-text">Add New Promo Codes</span>
                            </Link>
                        </li>

                        <li>
                            <Link className="prepend-icon" to="/app/promocode/list">
                            <i class="zmdi zmdi-format-list-bulleted"></i>
                                <span className="nav-text">Promo Codes List</span>
                            </Link>
                        </li>
                       
                            </ul>
                            </li>

               

            </ul>
        );
    }
}

export default withRouter(SideNavContent);
