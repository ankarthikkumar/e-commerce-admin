import axios from "axios";
import { constant } from '../constants';

export const getUnit = () => {
    return new Promise((resolve, reject) => {
        axios.get(`${constant.API_ENDPOINT}/unit`)
        .then((response) => {
            console.log("unit",response.data);
            resolve(response.data)
        })
        .catch((error) => reject(error))
    })
}



