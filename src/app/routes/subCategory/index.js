import React from 'react';
import {Redirect, Route, Switch} from 'react-router-dom';

import AddSubCategory from './routes/addSubCategory'
import SubCategory from './routes/subCategory'

const SuperCategory = ({match}) => (
    
   
    <div className="app-wrapper">
        <Switch>
          
            <Route path={`${match.url}/add`} component={AddSubCategory}/>
            <Route path={`${match.url}/list`} component={SubCategory}/>
           
        </Switch>
    </div>
);

export default SuperCategory;