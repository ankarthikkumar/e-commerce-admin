import React from 'react';
import {Redirect, Route, Switch} from 'react-router-dom';

import Orderlist from './routes/Orderlist'
import OrderSingle from './routes/OrderSingle'

const Order = ({match}) => (
    <div className="app-wrapper">
        <Switch>
        <Redirect exact from={`${match.url}/`} to={`${match.url}/list`}/>
            <Route path={`${match.url}/list`} component={Orderlist}/>
            <Route path={`${match.url}/single/:id`} component={OrderSingle}/>
           
        </Switch>
    </div>
);

export default Order;