import axios from "axios";
import { constant } from '../constants';

export const getProduct = () => {
    return new Promise((resolve, reject) => {
        axios.get(`${constant.API_ENDPOINT}/product`)
        .then((response) => {
            console.log("product",response.data);
            resolve(response.data)
        })
        .catch((error) => reject(error))
    })
}
export const saveProduct = (request) => {
    return new Promise((resolve, reject) => {
        axios.post(`${constant.API_ENDPOINT}/product`,request)
        .then((response) => {
            console.log("product",response.data);
            resolve(response.data)
        })
        .catch((error) => reject(error))
    })
}
export const updateProduct = (id,request) => {
    return new Promise((resolve, reject) => {
        axios.put(`${constant.API_ENDPOINT}/product/`+id,request)
        .then((response) => {
            console.log("product",response.data);
            resolve(response.data)
        })
        .catch((error) => reject(error))
    })
}
export const Disable_product = (id,request) => {
    return new Promise((resolve, reject) => {
        axios.put(`${constant.API_ENDPOINT}/Disable_product/`+id,request)
        .then((response) => {
            console.log("product",response.data);
            resolve(response.data)
        })
        .catch((error) => reject(error))
    })
}
export const getProductbyId = (id) => {
    return new Promise((resolve, reject) => {
        axios.get(`${constant.API_ENDPOINT}/product/`+id)
        .then((response) => {
            console.log("single Product",response.data);
            resolve(response.data)
        })
        .catch((error) => reject(error))
    })
}
