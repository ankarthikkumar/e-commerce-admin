import React from 'react';
import Button from 'material-ui/Button';
import Switch from 'material-ui/Switch';
import SweetAlert from 'react-bootstrap-sweetalert';
import Checkbox from 'material-ui/Checkbox';
import CardBox from "components/CardBox/index";
import Input, { InputLabel } from 'material-ui/Input';
import { MenuItem } from 'material-ui/Menu';
import Select from 'material-ui/Select';
import CKEditor from "react-ckeditor-component";
import { getSubCategory } from '../../actions/Category';
import { getCategory } from '../../actions/Category';
import { saveProduct } from '../../actions/Product';
import { updateProduct } from '../../actions/Product';
import { getProductbyId } from '../../actions/Product';
import { getSubcategoryByCategory } from '../../actions/Category'
import { getUnit } from '../../actions/Unit';
import CircularProgress from '@material-ui/core/CircularProgress';
import { awsFileUpload } from '../../actions/fileUpload';
import { ValidatorForm, TextValidator, SelectValidator } from 'react-material-ui-form-validator';





function createData(id, name) {
    return { id, name };
}

class AddProduct extends React.Component {
    state = {
        activeStep: 0,
        prodName: '',
        prodDescription: '',
        category: '',
        subCategory: '',
        prodPrice: '',
        salePrice: '',
        prodStock: '',
        allowBackOrders: '',
        stockThreshold: '',
        ScheduleFromDate: '',
        ScheduleToDate: '',
        prodStockStatus: '',
        prodSoldQty: '',
        unit: '', //Kg , L 
        prodUnitQuantity: '',// 1 Kg , 2 l
        prodOffer: '',
        height: '',
        length: '',
        width: '',
        weight: '', // in KG
        shippingtype: true,
        shippingCharge: '',
        prodStatus: '',
        dummyImage :'https://www.countrycheeseandmore.com/wp-content/uploads/2017/02/dummy-cat.png',
        image1: '',
        image2: '',
        image3: '',
        image4: '',
        image5: '',
        categoryList: [],
        subCategoryList: [],
        schdule: false,
        unitData: [],
        manageStock:false,
        loading:false,
        is_upload:false,
        isImage:false,
        success:false,
        is_edit:false,
        productId:''

    };

    handleNext = () => {
        const { activeStep } = this.state;
        this.setState({
            activeStep: activeStep + 1,
        });
    };

    handleBack = () => {
        const { activeStep } = this.state;
        this.setState({
            activeStep: activeStep - 1,
        });
    };

    handleReset = () => {
        this.setState({
            activeStep: 0,
        });
    };

    isSchedule = () => {
        this.setState({
            schdule: true,
        });
    };

    CloseSchedule = () => {
        this.setState({
            schdule: false,
        });
    };



    async componentDidMount() {

        var id = this.props.props.location.search;
        var current_id= id.split('&id=')[1];
        console.log("current_id",current_id);
        if(current_id!=undefined){
            var response = await getProductbyId(current_id);
            var ProductData = response.data;
            console.log("ProductData",ProductData)
             this.setState({ is_edit: true, productId : current_id , 
                prodName: ProductData.prodName,
                prodDescription: ProductData.prodDescription,
                category: ProductData.category._id,
                subCategory: ProductData.subCategory._id,
                prodPrice: ProductData.prodPrice,
                salePrice: ProductData.salePrice,
                prodStock: ProductData.prodStock,
                allowBackOrders: parseInt(ProductData.allowBackOrders),
                stockThreshold: ProductData.stockThreshold,
                ScheduleFromDate: ProductData.ScheduleFromDate,
                ScheduleToDate: ProductData.ScheduleToDate,
                prodStockStatus: parseInt(ProductData.prodStockStatus),
                prodSoldQty: ProductData.prodSoldQty,
                unit: ProductData.unit._id,
                prodUnitQuantity: ProductData.prodUnitQuantity,
                prodOffer: ProductData.prodOffer,
                manageStock:ProductData.manageStock=="false"?false:true,
                prodOffer: ProductData.prodOffer,
                height: ProductData.height,
                length: ProductData.length,
                width: ProductData.width,
                weight: ProductData.weight, // 
                shippingtype: ProductData.shippingtype == false ? 0 : 1,
                shippingCharge: ProductData.shippingCharge,
                prodStatus: ProductData.prodStatus,
              
                isImage:true,
                });
                // image1:ProductData.productImage[0].image,
                // image2:ProductData.productImage[1].image,
                // image3:ProductData.productImage[2].image,
                // image4:ProductData.productImage[3].image,
                // image5:ProductData.productImage[4].image,
                for(var i=0;i<ProductData.productImage.length;i++){
                    if(i==0){
                        this.setState({
                            image1:ProductData.productImage[i].image,
                       });
                    }
                    if(i==1){
                        this.setState({
                            image2:ProductData.productImage[i].image,
                       });
                    }
                    if(i==2){
                        this.setState({
                            image3:ProductData.productImage[i].image,
                       });
                    }
                    if(i==3){
                        this.setState({
                            image4:ProductData.productImage[i].image,
                       });
                    }
                    if(i==4){
                        this.setState({
                            image5:ProductData.productImage[i].image,
                       });
                    }
                }
                const sub_category = await getSubcategoryByCategory(ProductData.category._id);
                console.log(sub_category.data);
                this.setState({
                    subCategoryList: sub_category.data
                });
           
        }
        console.log(this.state);


        const category = await getCategory();
        const categoryData = category.data;

        this.setState({ categoryList: categoryData });

        const unit = await getUnit();
        this.setState({ unitData: unit.data });
        console.log(this.state.unitData);
    }


    handleInputChange = (event) => {
        const value = event.target.value;
        const name = event.target.name;

        this.setState({
            [name]: value
        });
    }
    handleShippingChange = (event) => {
        this.setState({ shippingtype: !this.state.shippingtype });
    }
    handleStockChange = (event) => {
        this.setState({ manageStock: !this.state.manageStock });
    }


    handleInputCategoryChange = async (event) => {
        const value = event.target.value;
        const name = event.target.name;

        this.setState({
            [name]: value
        });
        const sub_category = await getSubcategoryByCategory(value);
        console.log(sub_category.data);
        this.setState({
            subCategoryList: sub_category.data
        });
    }

    handleselectedFile = async (event) => {
        this.setState({
            is_upload: true,
            loading:true
        })
        console.log(event);
        var name= event.target.name
        const data = new FormData()
        data.append('media', event.target.files[0])
        console.log("data", data);
        console.log("--------------------------------------------------");
        const fileupload = await awsFileUpload(data);
        console.log(fileupload);
        console.log("--------------------------------------------------");
        this.setState({
            is_upload: false,
            loading:false,
            [name]: fileupload.file,
            isImage:true,
        })
        console.log("this.state",this.state);
    }

    
    SaveProduct = async () => {

        if(this.state.isImage==false){
            alert("please upload atleast 1 image");
            return false;
        }else{
        const data = Object.assign({}, {
            prodName: this.state.prodName,
            prodDescription: this.state.prodDescription,
            category: this.state.category,
            subCategory: this.state.subCategory,
            prodPrice: this.state.prodPrice,
            salePrice: this.state.salePrice,
            prodStock: this.state.prodStock,
            allowBackOrders: this.state.allowBackOrders,
            stockThreshold: this.state.stockThreshold,
            ScheduleFromDate: this.state.ScheduleFromDate,
            ScheduleToDate: this.state.ScheduleToDate,
            prodStockStatus: this.state.prodStockStatus,
            prodSoldQty: this.state.prodSoldQty,
            unit: this.state.unit,
            prodUnitQuantity: this.state.prodUnitQuantity,
            prodOffer: this.state.prodOffer,
            manageStock:this.state.manageStock,
            prodOffer: this.state.prodOffer,
            height: this.state.height,
            length: this.state.length,
            width: this.state.width,
            weight: this.state.weight, // 
            shippingtype: this.state.shippingtype == false ? 0 : 1,
            shippingCharge: this.state.shippingCharge,
            prodStatus: this.state.prodStatus,
            image :[{
                image:this.state.image1
            },
            {
                image:this.state.image2
            },
            {
                image:this.state.image3
            },
            {
                image:this.state.image4
            },
            {
                image:this.state.image5
            }],
           
           

        });
     
    console.log("data",data);
        var product="";
        if(this.state.is_edit==false){
         product = await saveProduct(data);
        }else{
         product = await updateProduct(this.state.productId,data);
        }

        if (product.status == true) {
           this.setState({
            success:true
           })
        }
    }
    }
    onConfirm = () => {
        this.setState({success: false});
        console.log(this.props.props)
        // this.props.props.history.push('/app/product/list');
    }


    render() {

        const { activeStep, categoryList, subCategoryList, unitData,loading  } = this.state;

        return (
            <ValidatorForm
                ref="form"
                className="col-lg-9 col-lg-offset-2"
                onSubmit={this.SaveProduct}
                onError={errors => console.log(errors)}
            >
                <CardBox styleName="col-lg-12" childrenStyle="d-flex" heading="Add Product ">
                    <div className="w-100">
                        <div id="container">
                            <div className="row">
                                <div className="col-md-12">

                                    <div className="form-group">
                                        <TextValidator
                                            value={this.state.prodName}
                                            id="productname"
                                            label="Product Name"
                                            name="prodName"
                                            autofocus
                                            onChange={this.handleInputChange}
                                            margin="normal"
                                            fullWidth
                                            validators={['required']}
                                            errorMessages={['Product Name field is required']}
                                        />
                                    </div>
                                </div>


                            </div>
                            <div className="row">
                                <div className="col-md-6">
                                    <div className="form-group">
                                        <InputLabel htmlFor="age-helper">Super Category </InputLabel>
                                        <Select
                                            value={this.state.category}
                                            fullWidth
                                            name="category"
                                            onChange={this.handleInputCategoryChange}
                                            input={<Input id="age-helper" />}
                                            validators={['required']}
                                            errorMessages={['Super Category field is required']}
                                        >
                                            {categoryList.map(data => {


                                                return (
                                                    <MenuItem value={data._id}>{data.categoryName}</MenuItem>
                                                )
                                            })}
                                        </Select>
                                    </div>
                                </div>
                                <div className="col-md-6">
                                    <div className="form-group">
                                        <InputLabel htmlFor="sub_cat">Sub Category </InputLabel>
                                        <Select
                                            value={this.state.subCategory}
                                            fullWidth
                                            name="subCategory"
                                            onChange={this.handleInputChange}
                                            input={<Input id="age-helper" />}
                                        >
                                            {subCategoryList.map(data => {

                                                return (
                                                    <MenuItem value={data._id}>{data.subCategoryName}</MenuItem>
                                                )
                                            })}
                                        </Select>
                                    </div>
                                </div>

                            </div>

                            <div className="row">
                                <div className="col-md-6">
                                    <div className="form-group">
                                        <InputLabel htmlFor="age-helper">Product Unit </InputLabel>
                                        <Select
                                            value={this.state.unit}
                                            fullWidth
                                            name="unit"
                                            onChange={this.handleInputChange}
                                            input={<Input id="age-helper" />}
                                        >
                                            {unitData.map(data => {


                                                return (
                                                    <MenuItem value={data._id}>{data.unit_name}</MenuItem>
                                                )
                                            })}
                                        </Select>
                                    </div>
                                </div>
                                <div className="col-md-6">
                                    <div className="form-group">
                                        <TextValidator
                                            type="number"
                                            value={this.state.prodUnitQuantity}
                                            id="prodUnitQuantity"
                                            label="Unit quantity"
                                            margin="normal"
                                            name="prodUnitQuantity"
                                            onChange={this.handleInputChange}
                                            fullWidth
                                            validators={['required']}
                                            errorMessages={['Unit quantity field is required']}
                                        /></div>
                                </div>

                            </div>


                            <div className="row">
                                <div className="col-md-12 border">
                            <TextValidator
                                id="multiline-static"
                                label="Description"
                                multiline
                                rows="6"
                                defaultValue=""
                                value={this.state.prodDescription}
                                name="prodDescription"
                                onChange={this.handleInputChange}
                                fullWidth

                            />


                                    
                                </div>
                            </div>
                            <br />
                        </div>
                        <div>
                            <div className="row">
                                <div className="col-md-5">
                                    <div className="form-group">
                                        <TextValidator
                                            type="number"
                                            validators={['required']}
                                            errorMessages={['Regular Price field is required']}
                                            value={this.state.prodPrice}
                                            id="regularPrice"
                                            label="Regular Price"
                                            margin="normal"
                                            name="prodPrice"
                                            onChange={this.handleInputChange}
                                            fullWidth
                                        /></div>
                                </div>
                                <div className="col-md-5">
                                    <div className="form-group">
                                        <TextValidator
                                            value={this.state.salePrice}
                                            type="number"
                                            validators={['required']}
                                            errorMessages={['Sale Price field is required']}
                                            id="salePrice"
                                            label="Sale Price"
                                            margin="normal"
                                            name="salePrice"
                                            onChange={this.handleInputChange}
                                            fullWidth
                                        />
                                    </div>
                                </div>
                                <div className="col-md-2">

                                    <Button variant="raised" fullWidth color='primary' className="jr-btn  text-white" onClick={this.isSchedule} >Schedule</Button>

                                </div>
                            </div>

                            <div className={this.state.schdule == true ? 'row' : 'hidden'}>
                                <div className="col-md-5">
                                    <div className="form-group">
                                        <div key="basic_day" className="picker">
                                            <TextValidator
                                                type="date"
                                                value={this.state.ScheduleFromDate}
                                                id="fromDate"
                                                label="From Date"
                                                margin="normal"
                                                name="ScheduleFromDate"
                                                onChange={this.handleInputChange}
                                                fullWidth
                                            />
                                        </div></div>
                                </div>
                                <div className="col-md-5">
                                    <div className="form-group">
                                        <div key="basic_day" className="picker">

                                            <TextValidator
                                                value={this.state.ScheduleToDate}
                                                type="date"
                                                id="toDate"
                                                label="To Date"
                                                margin="normal"
                                                name="ScheduleToDate"
                                                onChange={this.handleInputChange}
                                                fullWidth
                                            />
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-2">

                                    <Button variant="raised" fullWidth color='primary' className="jr-btn  text-white" onClick={this.CloseSchedule} >Close</Button>

                                </div>
                            </div>
                        </div>

                        <div className="tab-pane" id="tab2-3">
                            <div className="row">
                                <div className="col-md-12">
                                    <div className="form-group">
                                        <div className="d-flex align-items-center">
                                            <span>Manage Stock </span> <Checkbox   checked={this.state.manageStock} name="manageStock"   onChange={this.handleStockChange} /> <span color="grey">Enable Stock management of Product Level.</span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div className="row">
                                <div className="col-md-6">

                                    <div className="form-group">
                                        <InputLabel htmlFor="sub_cat">Stock Status </InputLabel>
                                        <Select
                                            value={this.state.prodStockStatus}
                                            fullWidth
                                            onChange={this.handleInputChange}
                                            name="prodStockStatus"
                                            input={<Input id="sub_cat" />}
                                        >
                                            <MenuItem value="">
                                                <em>None</em>
                                            </MenuItem>
                                            <MenuItem value={1}>In Stock</MenuItem>
                                            <MenuItem value={2}>Out of Stock</MenuItem>
                                            <MenuItem value={3}>On Loading</MenuItem>
                                        </Select>
                                    </div>

                                </div>
                                <div className="col-md-6">
                                    <div className="form-group">
                                        <InputLabel htmlFor="sub_cat">Allow Backorders </InputLabel>
                                        <Select
                                            value={this.state.allowBackOrders}
                                            fullWidth
                                            name="allowBackOrders"
                                            input={<Input id="sub_cat" />}
                                            onChange={this.handleInputChange}

                                        >
                                            <MenuItem value="">
                                                <em>None</em>
                                            </MenuItem>
                                            <MenuItem value={3}>Do not allow</MenuItem>
                                            <MenuItem value={2}>Allow, but notify custom</MenuItem>
                                            <MenuItem value={1}>Allow</MenuItem>
                                        </Select>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-6">
                                    <div className="form-group">
                                        <TextValidator
                                            validators={['required']}
                                            errorMessages={['Stock Quantity  field is required']}
                                            type="number"
                                            value={this.state.prodStock}
                                            id="Stock_Quantity"
                                            label="Stock Quantity"
                                            margin="normal"
                                            name="prodStock"
                                            onChange={this.handleInputChange}
                                            fullWidth
                                        />
                                    </div>
                                </div>

                                <div className="col-md-6">
                                    <div className="form-group">
                                        <TextValidator

                                            type="number"
                                            value={this.state.stockThreshold}
                                            id="stock_threshold"
                                            label="Stock Threshold"
                                            margin="normal"
                                            name="stockThreshold"
                                            onChange={this.handleInputChange}
                                            fullWidth
                                        />
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div className="tab-pane" id="tab2-4">
                            <div className="row">
                                <div className="col-md-6">
                                    <div className="form-group">
                                        <TextValidator
                                            value={this.state.weight}
                                            id="weight"
                                            label="Weight (Kg)"
                                            margin="normal"
                                            name="weight"
                                            onChange={this.handleInputChange}
                                            fullWidth
                                        />
                                    </div>
                                </div>
                                <div className="col-md-6">
                                    <div className="form-group">
                                        <TextValidator
                                            value={this.state.length}
                                            id="length"
                                            label="Length (cm)"
                                            margin="normal"
                                            name="length"
                                            onChange={this.handleInputChange}
                                            fullWidth
                                        />
                                    </div>
                                </div>

                            </div>


                            <div className="row">
                                <div className="col-md-6">
                                    <div className="form-group">
                                        <TextValidator
                                            validators={['required']}
                                            errorMessages={['Width field is required']}
                                            value={this.state.width}
                                            id="width"
                                            label="Width (cm)"
                                            margin="normal"
                                            name="width"
                                            onChange={this.handleInputChange}
                                            fullWidth
                                        />
                                    </div>
                                </div>
                                <div className="col-md-6">
                                    <div className="form-group">
                                        <TextValidator
                                            validators={['required']}
                                            errorMessages={['Height  field is required']}
                                            value={this.state.height}
                                            id="height(cm)"
                                            label="Height (cm)"
                                            margin="normal"
                                            name="height"
                                            onChange={this.handleInputChange}
                                            fullWidth
                                        />
                                    </div>
                                </div>

                            </div>

                            <div className="d-flex align-items-center">
                                <span>Shipping type </span> &nbsp;&nbsp;
                            <span>Free </span>
                                <Switch
                                    classes={{
                                        checked: 'text-primary',
                                        bar: 'bg-primary',
                                    }}
                                    name="shippingtype"

                                    checked={this.state.shippingtype}
                                    onChange={this.handleShippingChange}
                                />
                                <span>Cost </span>
                            </div>
                            <div className={this.state.shippingtype == true ? 'row' : 'hidden'}>
                                <div className="col-md-6">
                                    <div className="form-group">
                                        <TextValidator
                                            id="cost"
                                            value={this.state.shippingCharge}
                                            label="Shipping Cost (per Kg)"
                                            margin="normal"
                                            name="shippingCharge"
                                            onChange={this.handleInputChange}
                                            fullWidth
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-12 col-12">
                            <h2>Product Image</h2>
                            <p className="red" color="red"> * You should upload atleast 1 image. </p>
                        </div>
                        <br />


                        <div className="row">
                            <div className="col-sm-4 col-md-4">
                                <div className="custom_width">
                                    <div className="">
                                        <input name="image1" id="image1" type="file" accept=".png, .jpg, .jpeg" />
                                        <img className="image" width="260px" src={this.state.image1==""?this.state.dummyImage:this.state.image1} /><br />
                                        <label for="image1" className="custom_file_input">
                                            <span className="pleaseWaitOption" id="image1_icon" >
                                                <b><i className="fa fa-spinner fa-spin"></i> &nbsp;Please wait...</b></span>
                                               </label><br/>

                            <Button
                                variant="raised"
                                component="label"
                                color="primary"
                                className="btn btn-primary"

                            >
                                Upload File
                                <input type="file" name="image1" 
                                         onChange={this.handleselectedFile} 
                                        style={{ display: "none" }}
                                    />
                            </Button>
                                    </div>
                                </div>
                            </div>
                            <div className="col-sm-4 col-md-4 custom_width">
                                <div className="custom_width">
                                    <div className="">
                                        <input name="image2" id="image2" type="file" accept=".png, .jpg, .jpeg" />
                                        <img className="image" width="260px" src={this.state.image2==""?this.state.dummyImage:this.state.image2}/><br />
                                        <label for="image2" className="custom_file_input">
                                            <span className="pleaseWaitOption" id="image2_icon" >
                                                <b> Please wait...</b></span>
                                            </label>
                                            <br/>

                            <Button
                                variant="raised"
                                component="label"
                                color="primary"
                                className="btn btn-primary"

                            >
                                Upload File
                                <input type="file" name="image2" 
                                         onChange={this.handleselectedFile} 
                                        style={{ display: "none" }}
                                    />
                            </Button>

                                    </div>
                                </div>
                            </div>
                            <div className="col-sm-4 col-md-4 custom_width">
                                <div className="custom_width">
                                    <div className="">
                                        <input type="file" accept="image/*" id="image3" name="image3" className="btn btn-sm image_input_style"
                                        />

                                        <img className="image" width="260px" src={this.state.image3==""?this.state.dummyImage:this.state.image3} /><br />
                                        <label  className="custom_file_input">
                                            <span className="pleaseWaitOption" id="photoProof_icon" >
                                                <b><i className="fa fa-spinner fa-spin"></i> &nbsp;Please wait...</b></span>
                                            </label>
                                            <br/>

                            <Button
                                variant="raised"
                                component="label"
                                color="primary"
                                className="btn btn-primary"

                            >
                                Upload File
                                <input type="file" name="image3" 
                                         onChange={this.handleselectedFile} 
                                        style={{ display: "none" }}
                                    />
                            </Button>


                                    </div>


                                </div>
                            </div>
                       


                        <div className="col-sm-4 col-md-4 custom_width">
                        <br/>
                        <br/>
                        <br/>
                            <div className="custom_width">
                                <div className="">
                                    <input type="file" accept="image/*" id="image4" name="image4" className="btn btn-sm image_input_style"
                                    />

                                    <img className="image" width="260px" src={this.state.image4==""?this.state.dummyImage:this.state.image4} /><br />
                                    <label for="image4" className="custom_file_input">
                                        <span className="pleaseWaitOption" id="photoProof_icon" >
                                            <b><i className="fa fa-spinner fa-spin"></i> &nbsp;Please wait...</b></span>
                                       </label>
                                       <br/>

                            <Button
                                variant="raised"
                                component="label"
                                color="primary"
                                className="btn btn-primary"

                            >
                                Upload File
                                <input type="file" name="image4" 
                                         onChange={this.handleselectedFile} 
                                        style={{ display: "none" }}
                                    />
                            </Button>


                                </div>


                            </div>
                        </div>


                        <div className="col-sm-4 col-md-4 custom_width">
                        <br/>
                        <br/>
                        <br/>
                            <div className="custom_width">
                                <div className="">
                                    <input type="file" accept="image/*" id="image5" name="image5" className="btn btn-sm image_input_style"
                                    />

                                    <img className="image" width="260px" src={this.state.image5==""?this.state.dummyImage:this.state.image5} /><br />
                                    <label for="image5" className="custom_file_input">
                                        <span className="pleaseWaitOption" id="photoProof_icon" >
                                            <b><i className="fa fa-spinner fa-spin"></i> &nbsp;Please wait...</b></span>
                                        </label>
                                        <br/>

                            <Button
                                variant="raised"
                                component="label"
                                color="primary"
                                className="btn btn-primary"

                            >
                                Upload File
                                <input type="file" name="image5" 
                                         onChange={this.handleselectedFile} 
                                        style={{ display: "none" }}
                                    />
                            </Button>
                                </div>
                            </div>
                        </div>

                        </div>

                        <div className="col-md-12 col-12">
                        {loading && <CircularProgress/>}
                            <Button variant="raised" type="submit" color="primary" className={this.state.is_upload == true ? "hidden" : "jr-btn  text-white"}>Save Product</Button>
                        </div>
                    </div>
                    <SweetAlert show={this.state.success} success title="Success!" onConfirm={this.onConfirm}>
                        Product Added Sucussfully!!
                    </SweetAlert>
                </CardBox>
            </ValidatorForm>
            
        );
    }
}



export default AddProduct;