import React from 'react';
import {Redirect, Route, Switch} from 'react-router-dom';

import salesReports from './routes/salesReports'
import financialReports from './routes/financialReports'

const Reports = ({match}) => (
   
    <div className="app-wrapper">
        <Switch>
            <Route path={`${match.url}/sales`} component={salesReports}/>
            <Route path={`${match.url}/financial`} component={financialReports}/>
        </Switch>
    </div>
);

export default Reports;