import React from 'react';
import {Redirect, Route, Switch} from 'react-router-dom';

import NewCustomer from './routes/NewCustomer'
import CustomerList from './routes/CustomerList'
import CustomerView from './routes/CustomerView'

const Customer = ({match}) => (
    <div className="app-wrapper">
        <Switch>
            
            <Route path={`${match.url}/add`} component={NewCustomer}/>
            <Route path={`${match.url}/list`} component={CustomerList}/>
            <Route path={`${match.url}/single`} component={CustomerView}/>
           
        </Switch>
    </div>
);

export default Customer;