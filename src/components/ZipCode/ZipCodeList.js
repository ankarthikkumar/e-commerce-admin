import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import keycode from 'keycode';
import Table, {
    TableBody,
    TableCell,
    TableFooter,
    TableHead,
    TablePagination,
    TableRow,
    TableSortLabel,
} from 'material-ui/Table';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';
import Paper from 'material-ui/Paper';
import IconButton from 'material-ui/IconButton';
import Tooltip from 'material-ui/Tooltip';
import DeleteIcon from 'material-ui-icons/Delete';
import CardBox from "components/CardBox/index";
import { addZipCode } from '../../actions/ZipCode'
import { getZipCode } from '../../actions/ZipCode'
import { EditZipCode } from '../../actions/ZipCode'
import Button from 'material-ui/Button';
import TextField from 'material-ui/TextField';
import Dialog, { DialogActions, DialogContent, DialogContentText, DialogTitle, } from 'material-ui/Dialog';
import Menu, { MenuItem } from 'material-ui/Menu';
import SweetAlert from 'react-bootstrap-sweetalert';
// import IconButton from 'material-ui/IconButton';
let counter = 0;

function createData(index, zipId ,code, isDeleted, action) {

    return { id: index + 1,zipId, code, isDeleted, action };
}
const ITEM_HEIGHT = 40;
const options = [
    'Update Data',
    'Detailed Log',
    'Statistics',
    'Clear Data',
];


const columnData = [
    { id: 'SNo', disablePadding: true, label: 'SNo' },
    { id: 'code', disablePadding: false, label: 'Zip Code' },
    { id: 'status', disablePadding: false, label: 'Status' },
    { id: 'action', disablePadding: false, label: 'Actions' },
];

class DataTableHead extends React.Component {
    static propTypes = {
        numSelected: PropTypes.number.isRequired,
        onRequestSort: PropTypes.func.isRequired,
        onSelectAllClick: PropTypes.func.isRequired,
        order: PropTypes.string.isRequired,
        orderBy: PropTypes.string.isRequired,
        rowCount: PropTypes.number.isRequired,
    };

    createSortHandler = property => event => {
        this.props.onRequestSort(event, property);
    };



    render() {
        const { onSelectAllClick, order, orderBy, numSelected, rowCount } = this.props;

        return (
            <TableHead>
                <TableRow>

                    {columnData.map(column => {
                        return (
                            <TableCell
                                margin="10px"
                                key={column.id}
                                padding={column.disablePadding ? 'default' : 'default'}
                            >
                                <Tooltip
                                    title="Sort"
                                    placement={column.numeric ? 'bottom-end' : 'bottom-start'}
                                    enterDelay={300}
                                >
                                    <TableSortLabel
                                        active={orderBy === column.id}
                                        direction={order}
                                        onClick={this.createSortHandler(column.id)}
                                    >
                                        {column.label}
                                    </TableSortLabel>
                                </Tooltip>
                            </TableCell>
                        );
                    }, this)}
                </TableRow>
            </TableHead>
        );
    }
}


let DataTableToolbar = props => {
    const { numSelected } = props;

    return (
        <Toolbar
            className={classNames("table-header", {
                ["highlight-light"]: numSelected > 0,
            })}
        >
            <div className="title">
                {numSelected > 0 ? (
                    <Typography type="subheading">{numSelected} selected</Typography>
                ) : (
                        <Typography type="title">Zipcode List   </Typography>
                    )}
            </div>
            <div className="spacer" />
            <div className="actions">
                {numSelected > 0 && (
                    <Tooltip title="Delete">
                        <IconButton aria-label="Delete">
                            <DeleteIcon />
                        </IconButton>
                    </Tooltip>
                )}
            </div>
        </Toolbar>
    );
};

DataTableToolbar.propTypes = {
    numSelected: PropTypes.number.isRequired,
};


class ZipCodeList extends React.Component {

    handleRequestSort = (event, property) => {
        const orderBy = property;
        let order = 'desc';

        if (this.state.orderBy === property && this.state.order === 'desc') {
            order = 'asc';
        }

        const data =
            order === 'desc'
                ? this.state.data.sort((a, b) => (b[orderBy] < a[orderBy] ? -1 : 1))
                : this.state.data.sort((a, b) => (a[orderBy] < b[orderBy] ? -1 : 1));

        this.setState({ data, order, orderBy });
    };
    handleSelectAllClick = (event, checked) => {
        if (checked) {
            this.setState({ selected: this.state.data.map(n => n.id) });
            return;
        }
        this.setState({ selected: [] });
    };
    handleKeyDown = (event, id) => {
        if (keycode(event) === 'space') {
            this.handleClick(event, id);
        }
    };

    handleChangePage = (event, page) => {
        this.setState({ page });
    };
    handleChangeRowsPerPage = event => {
        this.setState({ rowsPerPage: event.target.value });
    };

    async componentDidMount() {

        this.loadData();
        setInterval(() => this.loadData(), 60000);
    }
    async loadData() {
        const user = await getZipCode();
        const userData = user.data;
        this.setState({
            data: await userData.map((item, index) => createData(index,item._id, item.code, item.isDeleted, 'edit'))
        });
    }
    onOptionMenuSelect = (n,event) => {
        this.setState({ menuState: true, anchorEl: event.currentTarget,currentZipCode:n });
    };

    isSelected = id => this.state.selected.indexOf(id) !== -1;

    constructor(props, context) {
        super(props, context);

        this.state = {
            order: 'desc',
            orderBy: 'id',
            selected: [],
            data: [],
            page: 0,
            rowsPerPage: 10,
            zip_code: '',
            anchorEl: undefined,
            menuState: false,
            is_edit:false,
            zipCodeId:'',
            currentZipCode:[],
            success:false
        };
    }


    handleInputChange = (event) => {
        // console.log(event.target, event.target.value, event.target.name);
        const value = event.target.value;
        const name = event.target.name;
        this.setState({
            [name]: value
        });
    }



    saveZipCode = async () => {

    if(this.state.is_edit==false){
        var data = Object.assign({}, { code: this.state.zip_code })
        var response = await addZipCode(data);
        if (response.status == true) {
         this.setState({success:true})
            this.handleRequestClose();
            this.loadData();
        }
    }else{
        var data = Object.assign({}, { code: this.state.zip_code })
        var response = await EditZipCode(this.state.zipCodeId,data);
        
        console.log("response", response);
        if (response.status == true) {
            this.setState({is_edit:false,success:true});
            this.handleRequestClose();
            this.loadData();
            
        }
    }
    }
    onConfirm = () => {
        this.setState({success: false});
    }

    handleClickOpen = () => {
        this.setState({ open: true });
    };
    handleRequestClose = (event) => {
        this.setState({ open: false,menuState:false,is_edit:false,zip_code:'' });
    };

    handleEdit = (event) => {
        console.log("Edit",this.state.currentZipCode);
        var currentZip = this.state.currentZipCode;
        this.state.zip_code = currentZip.code;
        this.setState({ menuState: false,is_edit:true});
        this.setState({ zipCodeId: currentZip.zipId});
        this.handleClickOpen();
    };

    handleDelete = async (event) => {
        var currentZip = this.state.currentZipCode;
        var action = currentZip.isDeleted ==1 ? 0 :1;
        this.setState({ menuState: false, anchorEl: event.currentTarget});
        var data = Object.assign({}, { isDeleted: action})
        var response = await EditZipCode(currentZip.zipId,data);
        
        if (response.status == true) {
            this.setState({is_edit:false,success:true});
            this.handleRequestClose();
            this.loadData();
            
        }
    };

    render() {
        const { menuState, anchorEl, handleRequestClose, data, order, orderBy, selected, rowsPerPage, page,is_show_id  } = this.state;

        return (

            <div className="dashboard animated slideInUpTiny animation-duration-3"> <br></br>
                <h2>ZipCode Management  <Button variant="raised" className="bg-primary text-white right" onClick={this.handleClickOpen}>Add ZipCode</Button></h2>
                <br />

                <Dialog open={this.state.open} onClose={this.handleRequestClose}>
                    <DialogTitle>Save Zipcode</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            Enter Zipcode which you want to deliver.
                        </DialogContentText>
                        <TextField
                            autoFocus
                            margin="dense"
                            id="name"
                            name="zip_code"
                            value={this.state.zip_code}
                            onChange={this.handleInputChange}
                            label="Zip Code"
                            type="number"
                            fullWidth
                        />
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleRequestClose} color="secondary">
                            Close
                        </Button>
                        <Button onClick={this.saveZipCode} color="primary">
                        Save
                        </Button>
                    </DialogActions>
                </Dialog>
                <Paper>
                    <DataTableToolbar numSelected={selected.length} />
                    <div className="flex-auto">
                        <div className="table-responsive-material">
                            <Table className="">
                                <DataTableHead
                                    numSelected={selected.length}
                                    order={order}
                                    orderBy={orderBy}
                                    onSelectAllClick={this.handleSelectAllClick}
                                    onRequestSort={this.handleRequestSort}
                                    rowCount={data.length}
                                />
                                <TableBody>
                                    {data.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map(n => {
                                        return (
                                            <TableRow
                                                hover

                                                onKeyDown={event => this.handleKeyDown(event, n.id)}

                                                tabIndex={-1}
                                                key={n.id}
                                            >
                                                <TableCell padding="default">
                                                    {n.id}
                                                </TableCell>
                                                <TableCell padding="default">{n.code}</TableCell>
                                                <TableCell padding="default">{n.isDeleted==1?'Deactiveted':'Active'}</TableCell>
                                                <TableCell padding="default">
                                                    <IconButton className="size-30" onClick={this.onOptionMenuSelect.bind(this,n)}>
                                                        <i className="zmdi zmdi-more-vert" /></IconButton>
                                                    <Menu 
                                                        Menu id="long-menu"
                                                        anchorEl={anchorEl}
                                                        open={menuState}
                                                        onClose={handleRequestClose}
                                                        style={{ maxHeight: ITEM_HEIGHT * 4.5 }}
                                                        MenuListProps={{
                                                            style: {
                                                                width: 150,
                                                                paddingTop: 0,
                                                                paddingBottom: 0
                                                            },
                                                        }}> 
                                                            <MenuItem key='Edit'  onClick={this.handleEdit.bind(this,n)}>
                                                              Edit
                                                            </MenuItem>,
                                                            <MenuItem key='Delete'  onClick={this.handleDelete.bind(this)}>
                                                          Change Status
                                                            </MenuItem>
                                                            <MenuItem key='close' onClick={this.handleRequestClose.bind(this)} >
                                                            Close
                                                            </MenuItem>,
                                                    </Menu></TableCell>
                                            </TableRow>
                                        );
                                    })}
                                </TableBody>
                                <TableFooter>
                                    <TableRow>
                                        <TablePagination
                                            count={data.length}
                                            rowsPerPage={rowsPerPage}
                                            page={page}
                                            onChangePage={this.handleChangePage}
                                            onChangeRowsPerPage={this.handleChangeRowsPerPage}
                                        />
                                    </TableRow>
                                </TableFooter>
                            </Table>
                        </div>
                    </div>
                </Paper>
                <SweetAlert show={this.state.success} success title="Success!" onConfirm={this.onConfirm}>
                        Zipcode Saved Sucussfully!!
                    </SweetAlert>    
            </div>

        );
    }
}

export default ZipCodeList;