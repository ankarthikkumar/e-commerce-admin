import React from 'react';
import {Redirect, Route, Switch} from 'react-router-dom';

import Default from './routes/Default'
import ECommerce from './routes/ECommerce'
import News from './routes/News'
import Intranet from './routes/Intranet'

const Dashboard = ({match}) => (
    <div className="app-wrapper">
        <Switch>
            <Redirect exact from={`${match.url}/`} to={`${match.url}/`}/>
            <Route path={`${match.url}/`} component={ECommerce}/>
           
        </Switch>
    </div>
);

export default Dashboard;