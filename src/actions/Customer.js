import axios from "axios";
import { constant } from '../constants';

export const saveCustomer = (request) => {
    return new Promise((resolve, reject) => {
        axios.post(`${constant.API_ENDPOINT}/saveCustomer`,request)
        .then((response) => {
            console.log("saveCustomer",response.data);
            resolve(response.data)
        })
        .catch((error) => reject(error))
    })
}

export const getUser = () => {
    return new Promise((resolve, reject) => {
        axios.get(`${constant.API_ENDPOINT}/getUser`)
        .then((response) => {
            console.log("getUser",response.data);
            resolve(response.data)
        })
        .catch((error) => reject(error))
    })
}

export const getSingleUser = (id) => {
    return new Promise((resolve, reject) => {
        axios.get(`${constant.API_ENDPOINT}getUser/`+id)
        .then((response) => {
            console.log("getUser Single",response.data);
            resolve(response.data)
        })
        .catch((error) => reject(error))
    })
}
export const UpdateCustomer = (id,request) => {
    return new Promise((resolve, reject) => {
        axios.put(`${constant.API_ENDPOINT}getUser/`+id,request)
        .then((response) => {
            console.log("Update User Single",response.data);
            resolve(response.data)
        })
        .catch((error) => reject(error))
    })
}
export const getCountryCodeList = () => {
    return new Promise((resolve, reject) => {
        axios.get(`${constant.API_ENDPOINT}/countryCode`)
        .then((response) => {
            console.log("countryCode",response.data);
            resolve(response.data)
        })
        .catch((error) => reject(error))
    })
}



