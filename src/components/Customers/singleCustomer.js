import React, { Component } from 'react';
import PropTypes from 'prop-types';
import AppBar from 'material-ui/AppBar';
import Tabs, { Tab } from 'material-ui/Tabs';
import CardBox from "components/CardBox/index";
import Select from 'material-ui/Select'
import Input, { InputLabel } from 'material-ui/Input';
import { MenuItem } from 'material-ui/Menu';
import Button from 'material-ui/Button';
import { getSingleUser } from '../../actions/customer';
import { getOrderByUser } from '../../actions/Order';
import SweetAlert from 'react-bootstrap-sweetalert';
import {Link, withRouter} from 'react-router-dom';
import moment from 'moment';
function TabContainer(props) {
    return (
        <CardBox styleName="col-lg-12" childrenStyle="d-flex" >
            <div style={{ padding: 20 }}>
                {props.children}
            </div>
        </CardBox>
    );
}

TabContainer.propTypes = {
    children: PropTypes.node.isRequired,
};

class SingleCustomer extends Component {
    state = {
      customer :[],
        status:false,
        save_status:false,
        is_loading:true,
        value:0,
        customerData:[],
        orderDate :[]
    };
    handleInputChange = (event) => {
        const value = event.target.value;
        const name = event.target.name;

        this.setState({ [name]: value });
    }
    updateCategory = async () => {
        const data  = Object.assign({}, { status:this.state.status});
        console.log(data);
        console.log(this.state.data._id);

        const status  = await UpdateOrderStatus(this.state.data._id,data);
        console.log(status);
        if(status.status==true){
          this.setState({
            save_status:true
          })
        }
    }

    handleChange = (event, value) => {
        console.log(value);
        this.setState({ value });
    };
    async componentDidMount() {
        console.log(this.props.props);
        var id = this.props.props.location.search;
        console.log(id);
        var current_id = id.split('?id=')[1];
        console.log("current_id", current_id);
            var response = await getSingleUser(current_id);
            var CustomerData = response.data;
            console.log("CustomerData",CustomerData);
            var order = await getOrderByUser(current_id);
            console.log("order ",order.data.length);
            for (let index = 0; index < order.data.length; index++) {
                
                order.data[index].created_at= moment(order.data[index].created_at).format('llll');
                const element = order.data[index];
                console.log("element",element);
                
            }
            this.setState({
                customerData : CustomerData,
                orderData : order.data
            })
    }
 


    render() {
        const {value} = this.state;
        return (
            <div>
                <AppBar className="bg-primary" position="static">
                    <Tabs value={value} onChange={this.handleChange} scrollable scrollButtons="on">
                        <Tab className="tab" label="User Details" />
                        <Tab className="tab" label="Orders" />
                    </Tabs>
                </AppBar>
                {value === 0 &&
                    <TabContainer>
                       <div className=""> 
                       <div className=" font-class   col-md-12">
                                <div>Customer Name: &nbsp; {this.state.customerData.full_name}</div>
                            </div><br/>
                            <div className=" font-class   col-md-12">
                                <div>Customer Mobile: &nbsp; {this.state.customerData.mobile_number}</div>
                            </div><br/>
                            <div className=" font-class   col-md-12">
                                <div>Profile picture: <br/><br/><img src={this.state.customerData.profile_picture}/></div>
                            </div>
                        </div>
                    </TabContainer>}
                {value === 1 &&
                    <TabContainer>
                      
                        <div className="row">
                         {this.state.orderData.map((item,index)=>{
                             return(
                                <div className="col-lg-5 border line-space jr-card jr-card-widget card_margin" childrenStyle="d-flex" 
                                >
                                <b>Order  ID </b>:  {item.order_code} <span className="text-white right">
                {item.status==0 &&  <b className="text-white badge badge-info sm">Order Placed</b>} 
                {item.status==1 &&   <b className="text-white badge badge-danger">Order Cancelled</b>} 
                {item.status==2 &&   <b className="text-white badge badge-warning">Order Approved</b>} 
                {item.status==3 &&   <b className="text-white badge badge-warning">Packed</b>}  
                {item.status==4 &&   <b className="text-white badge badge-success lg">Shipped</b>}  
                {item.status==5 &&   <b className="text-white badge badge-success">Delivered</b> } 
                </span> <br/>
                                <b>Order Total</b> : $ {Number(item.total_amount).toFixed(2)}<br/>
                                <b>Total Quantity </b>: {item.total_item_count} Qty<br/>
                                <b>Ordered Date </b>: {item.created_at}<br/>
                                <hr/>
                                <h2><br/>
                                <Link to={`/app/order/single/${item.order_code}`} params={{ id: item.order_code }}> <Button color="primary" className="text-white btn-lg" variant="raised">View Order</Button></Link>
                                </h2>
                                </div>
                             )
                         })}
                        </div>
                    </TabContainer>}
                  
</div>
                  
           
        );
    }
}

export default SingleCustomer;