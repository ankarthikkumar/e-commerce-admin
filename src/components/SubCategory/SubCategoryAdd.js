import React from 'react';
import CardBox from "components/CardBox/index";

import TextField from 'material-ui/TextField';
import Button from 'material-ui/Button';
import Switch from 'material-ui/Switch';
import Input, { InputLabel } from 'material-ui/Input';
import { MenuItem } from 'material-ui/Menu';
import { FormControl, FormHelperText } from 'material-ui/Form';
import Select from 'material-ui/Select';
import { getCategory } from '../../actions/Category'
import { saveSubCategory } from '../../actions/Category'
import { updateSubCategory } from '../../actions/Category'
import { getSingleSubCategory } from '../../actions/Category'
import CircularProgress from '@material-ui/core/CircularProgress';
import { awsFileUpload } from '../../actions/fileUpload';
import SweetAlert from 'react-bootstrap-sweetalert';
import { ValidatorForm, TextValidator,SelectValidator } from 'react-material-ui-form-validator';
// import { getData } from 'path';

function createData(id, name) {
    return { id, name };
}
const styles = theme => ({
    progress: {
      margin: theme.spacing.unit * 2,
    },
  });
  
class SubCategoryAdd extends React.Component {





    onOptionMenuSelect = event => {
        this.setState({ menuState: true, anchorEl: event.currentTarget });
    };
    handleRequestClose = () => {
        this.setState({ menuState: false });
        // const apiRespone = getData();
    };

    handleInputChange = (event) => {
        // console.log(event.target, event.target.value, event.target.name);
        const value = event.target.value;
        const name = event.target.name;

        this.setState({
            [name]: value
        });
    }
    onConfirm = (event) => {
        this.setState({ success: false });
        console.log(this.props.props)
        this.props.props.history.push('/app/sub_category/list');
    }

    handleCategoryStatus = (event) => {
        this.setState({ subCategoryStatus: !this.state.subCategoryStatus });
    }

    subCategorySave = async () => {
        if(this.state.category==""){
            this.state.error="MuiInputBase-root-36 MuiInput-root-23 MuiInput-underline-27 MuiInputBase-error-42 MuiInput-error-28 MuiInputBase-fullWidth-45 MuiInput-fullWidth-30 MuiInputBase-formControl-37 MuiInput-formControl-24";
            return false;
        }else{
        this.setState({loading:true});
        const data = Object.assign({}, { subCategoryImage:this.state.subCategoryImage,category: this.state.category, subCategoryName: this.state.subCategoryName, subCategoryStatus: this.state.subCategoryStatus, subCategoryDescription: this.state.subCategoryDescription})
        console.log("data", data);
        if(this.state.is_edit==false){
            var response = await saveSubCategory(data);
        }else{
            var response = await updateSubCategory(this.state.subCategoryId,data);  
        }
        console.log("response", response);
        if (response.status == true) {
            this.setState({
                success:true
            })
        }
        this.setState({loading:false});
    }
    }


    constructor() {
        super();
        this.state = {
            anchorEl: undefined,
            menuState: false,
            data: [],
            category: '',
            subCategoryImage: '',
            subCategoryName: '',
            subCategoryDescription: '',
            subCategoryStatus: false,
            is_upload: false,
            is_edit:false,
            subCategoryId:'',
            loading:false,
            error:'',
            success:false
        }
    }

    
    async componentDidMount() {

        console.log(this.props.props);
        var id = this.props.props.location.search;
        var current_id = id.split('&id=')[1];
        console.log("current_id", current_id);
        if (current_id != undefined) {
            var response = await getSingleSubCategory(current_id);
            var categoryData = response.data[0];
            console.log("categoryData", categoryData)
            this.setState({ is_edit: true, subCategoryId: current_id, subCategoryName: categoryData.subCategoryName, category: categoryData.category, subCategoryDescription: categoryData.subCategoryDescription, subCategoryImage: categoryData.subCategoryImage, subCategoryStatus: categoryData.subCategoryStatus });

        }
        console.log(this.state);

        const category = await getCategory();
        console.log("category in component", category.data);
        const catData = category.data;
        this.setState({
            data: await catData.map((item, index) => createData(item._id, item.categoryName))
        });
        console.log("data ", this.state.data);
    }

    handleselectedFile = async event => {
        this.setState({
            is_upload: true,
            loading:true
        })
        console.log(event.target.value);
        const data = new FormData()
        data.append('media', event.target.files[0])
        console.log("data", data);
        console.log("--------------------------------------------------");
        const fileupload = await awsFileUpload(data);
        console.log(fileupload);
        console.log("--------------------------------------------------");
        this.setState({
            is_upload: false,
            loading:false,
            subCategoryImage: fileupload.file
        })
    }

    render() {
        // alert("Add Category")
        const { anchorEl, menuState, data ,loading} = this.state;
        return (
            <div className="dashboard animated slideInUpTiny animation-duration-3">
                <h2>Sub Category</h2>
                <CardBox styleName="col-lg-12" childrenStyle="d-flex" heading="Add Sub Category ">
                <ValidatorForm
                        ref="form"
                        className="col-lg-12"
                        entype="multipart/form-data"
                        onSubmit={this.subCategorySave}
                        onError={errors => console.log(errors)}
               
                    >
                        <div className="col-md-12 col-12">
                            <InputLabel htmlFor="Category">Super Category </InputLabel>
                            <Select
                                value={this.state.category}
                                fullWidth
                                input={<Input id="Category" />}
                                name="category"
                                className={this.state.error}
                                onChange={this.handleInputChange}

                            >
                                {data.map(selData => {


                                    return (
                                        <MenuItem value={selData.id}>{selData.name}</MenuItem>
                                    )
                                })}
                            </Select>
                        </div>

                        <div className="col-md-12 col-12">
                            <TextValidator
                                id="subCategoryName"
                                label="Sub Category Name"
                                name="subCategoryName"
                                value={this.state.subCategoryName}
                                margin="normal"
                                onChange={this.handleInputChange}
                                fullWidth
                                validators={['required']}
                                errorMessages={['Please Enter Subcategory Name']}
                            />
                        </div>

                        <div className="col-md-12 col-12">

                            <TextField
                                id="multiline-static"
                                label="Description"
                                multiline
                                rows="4"
                                defaultValue=""
                                margin="normal"
                                value={this.state.subCategoryDescription}
                                name="subCategoryDescription"
                                onChange={this.handleInputChange}
                                fullWidth
                            />
                        </div>
                        <div className="col-md-12 col-12">
                            <label className="mtop">Sub Category Status</label>
                            <Switch
                                classes={{
                                    checked: 'text-primary',
                                    bar: 'bg-primary',
                                }}
                                label="Sub Category Status"

                                checked={this.state.subCategoryStatus}
                                aria-label="Super Category Status"
                                onChange={this.handleCategoryStatus}
                            />
                        </div>
                        <br />
                        <div className="col-md-8 col-12">
                            <label>Sub Category  Featured Image</label>
                            <Button
                                variant="raised"
                                component="label"
                                color="primary"
                                className="custom-btn"

                            >
                                Upload File
                                <input type="file" name="subCategoryImage" 
                                        onChange={this.handleselectedFile} 
                                        style={{ display: "none" }}
                                        validators={['required']}
                                        errorMessages={['this field is required']} />
                            </Button>
                            <br />
                          
                        </div>
                        <div className="col-md-4">
                        <img src={this.state.subCategoryImage} height="100px;" className="col-md-12" /> 
                        </div>

                        {/* <div className="col-md-12"> */}
                            {loading && <CircularProgress/>}
                            <Button variant="raised" type="submit" color="primary" className={this.state.is_upload == true ? "hidden" : "jr-btn  text-white"}>Save</Button>

                        {/* </div> */}

                    </ValidatorForm>
                </CardBox>
                <SweetAlert show={this.state.success} success title="Success!" onConfirm={this.onConfirm}>
                        Sub Category Saved Sucussfully
                    </SweetAlert>

            </div>
        )
    }
}
export default SubCategoryAdd;