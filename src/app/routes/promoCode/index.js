import React from 'react';
import {Redirect, Route, Switch} from 'react-router-dom';

import AddPromo from './routes/AddPromo'
import CodeList from './routes/CodeList'

const PromoCode = ({match}) => (
    
   
    <div className="app-wrapper">
        <Switch>
          
            <Route path={`${match.url}/add`} component={AddPromo}/>
            <Route path={`${match.url}/list`} component={CodeList}/>
           
        </Switch>
    </div>
);

export default PromoCode;