import React from 'react';
import AddCustomer from "components/Customers/AddCustomer";

import TextField from 'material-ui/TextField';
import Button from 'material-ui/Button';
import Switch from 'material-ui/Switch';
class NewCustomer extends React.Component {

    onOptionMenuSelect = event => {
        this.setState({ menuState: true, anchorEl: event.currentTarget });
    };
    handleRequestClose = () => {
        this.setState({ menuState: false });
    };

    constructor() {
        super();
        this.state = {
            anchorEl: undefined,
            menuState: false,
        }
    }

    render() {
        const { anchorEl, menuState } = this.state;
        return (
            <div style={{width: '100%', height: '100%'}}>
            <AddCustomer props={this.props} />
            </div>
                )
    }
}
export default NewCustomer;