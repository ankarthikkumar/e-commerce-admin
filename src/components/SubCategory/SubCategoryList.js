import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import keycode from 'keycode';
import {Link, withRouter} from 'react-router-dom';
import SweetAlert from 'react-bootstrap-sweetalert';
import Table, {
    TableBody,
    TableCell,
    TableFooter,
    TableHead,
    TablePagination,
    TableRow,
    TableSortLabel,
} from 'material-ui/Table';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';
import Paper from 'material-ui/Paper';
import IconButton from 'material-ui/IconButton';
import Tooltip from 'material-ui/Tooltip';
import DeleteIcon from 'material-ui-icons/Delete';
import {ButtonDropdown, ButtonGroup, DropdownItem, DropdownMenu, DropdownToggle} from 'reactstrap';
import Button from 'material-ui/Button';
import {getSubCategory, updateSubCategory} from '../../actions/Category'
import Menu, { MenuItem } from 'material-ui/Menu';
let counter = 0;
const ITEM_HEIGHT = 40;
function createData(index,cat_id, c_id,name, category, prod_cnt, image,status,action) {
    return {id: index+1, cat_id, c_id,name, category, prod_cnt, image,status,action};
}

const columnData = [
    {id: 'SNo',  disablePadding: true, label: 'SNo'},
    {id: 'Subcategory Id',  disablePadding: false, label: 'Subcategory Id'},
    {id: 'Name',  disablePadding: false, label: 'Name'},
    {id: 'Category',  disablePadding: false, label: 'Category'},
    {id: 'Product count',  disablePadding: false, label: 'Product count'},
    {id: 'image',  disablePadding: false, label: 'image'},
    {id: 'status',  disablePadding: false, label: 'Status'},
    {id: 'action',  disablePadding: false, label: 'Actions'},
];

class DataTableHead extends React.Component {
    static propTypes = {
        numSelected: PropTypes.number.isRequired,
        onRequestSort: PropTypes.func.isRequired,
        onSelectAllClick: PropTypes.func.isRequired,
        order: PropTypes.string.isRequired,
        orderBy: PropTypes.string.isRequired,
        rowCount: PropTypes.number.isRequired,
    };

    createSortHandler = property => event => {
        this.props.onRequestSort(event, property);
    };

    render() {
        const {onSelectAllClick, order, orderBy, numSelected, rowCount} = this.props;

        return (
            <TableHead>
                <TableRow>
                    
                    {columnData.map(column => {
                        return (
                            <TableCell
                           margin="10px"
                                key={column.id}
                                padding={column.disablePadding ? 'default' : 'default'}
                            >
                                <Tooltip
                                    title="Sort"
                                    placement={column.numeric ? 'bottom-end' : 'bottom-start'}
                                    enterDelay={300}
                                >
                                    <TableSortLabel
                                        active={orderBy === column.id}
                                        direction={order}
                                        onClick={this.createSortHandler(column.id)}
                                    >
                                        {column.label}
                                    </TableSortLabel>
                                </Tooltip>
                            </TableCell>
                        );
                    }, this)}
                </TableRow>
            </TableHead>
        );
    }
}


let DataTableToolbar = props => {
    const {numSelected} = props;

    return (
        <Toolbar
            className={classNames("table-header", {
                ["highlight-light"]: numSelected > 0,
            })}
        >
            <div className="title">
                {numSelected > 0 ? (
                    <Typography type="subheading">{numSelected} selected</Typography>
                ) : (
                    <Typography type="title">Sub Categories</Typography>
                )}
            </div>
            <div className="spacer" />
            <div className="actions">
                {numSelected > 0 && (
                    <Tooltip title="Delete">
                        <IconButton aria-label="Delete">
                            <DeleteIcon />
                        </IconButton>
                    </Tooltip>
                )}
            </div>
        </Toolbar>
    );
};

DataTableToolbar.propTypes = {
    numSelected: PropTypes.number.isRequired,
};


class SubCategoryList extends React.Component {
    handleRequestSort = (event, property) => {
        const orderBy = property;
        let order = 'desc';

        if (this.state.orderBy === property && this.state.order === 'desc') {
            order = 'asc';
        }

        const data =
            order === 'desc'
                ? this.state.data.sort((a, b) => (b[orderBy] < a[orderBy] ? -1 : 1))
                : this.state.data.sort((a, b) => (a[orderBy] < b[orderBy] ? -1 : 1));

        this.setState({data, order, orderBy});
    };
    handleSelectAllClick = (event, checked) => {
        if (checked) {
            this.setState({selected: this.state.data.map(n => n.id)});
            return;
        }
        this.setState({selected: []});
    };
    handleKeyDown = (event, id) => {
        if (keycode(event) === 'space') {
            this.handleClick(event, id);
        }
    };
    
    handleChangePage = (event, page) => {
        this.setState({page});
    };
    handleChangeRowsPerPage = event => {
        this.setState({rowsPerPage: event.target.value});
    };

    async componentDidMount() {
         this.loadData();     
    }

    loadData = async() =>{
        const sub_category = await getSubCategory();
        console.log("sub category in component",sub_category.data);
        const categoryData = sub_category.data;
        this.setState({ 
            // createData('6hjhdsjdjsbsjb', "Pepri", "Drinks",11,'https://cdn4.iconfinder.com/data/icons/drinks-solid-icons-vol-1/72/35-512.png','Active','Edit'),
            data: await categoryData.map((item, index) => createData(index,item.sub_category_code, item._id, item.subCategoryName, item.category.categoryName,item.product_count,item.subCategoryImage,item.isDeleted,'Edit')) 
        });
        console.log("data ",this.state.data);  
    }

    onOptionMenuSelect = (n,event) => {
        console.log(n)
        this.setState({ menuState: true, anchorEl: event.currentTarget,subCatId:n.c_id });
    };
   

    
    DisableSubCategory = async (data) => {
        console.log(data);
        var action = data.status ==1 ? 0 :1;
        var dbUpdate = Object.assign({}, { isDeleted: action})
        console.log(dbUpdate);
        var response = await updateSubCategory(data.c_id,dbUpdate);
        if (response.status == true) {
            this.setState({success:true});
            this.loadData();
        }
    };

    onConfirm = (event) => {
        this.setState({ success: false });
    }


    handleRequestClose = (event) => {
        console.log("handleRequestClose");
        this.setState({ open: false,menuState:false, anchorEl: event.currentTarget });
    };


    isSelected = id => this.state.selected.indexOf(id) !== -1;

    constructor(props, context) {
        super(props, context);

        this.state = {
            order: 'asc',
            orderBy: 'calories',
            selected: [],
            data: [],
            page: 0,
            rowsPerPage: 10,
            subCatId:'',
            success:false
        };
    }
    

    render() {
        const {data, order, orderBy, selected, rowsPerPage, page,anchorEl ,menuState,handleRequestClose } = this.state;

        return (
            <div className="dashboard animated slideInUpTiny animation-duration-3">
            <h2>SubCategory List < Link to="/app/sub_category/add" ><Button variant="raised" className="bg-primary text-white right" >Add Subcategory</Button></Link></h2>
            <br/>
            <Paper>
                <DataTableToolbar numSelected={selected.length} />
                <div className="flex-auto">
                    <div className="table-responsive-material">
                        <Table className="">
                            <DataTableHead
                                numSelected={selected.length}
                                order={order}
                                orderBy={orderBy}
                                onSelectAllClick={this.handleSelectAllClick}
                                onRequestSort={this.handleRequestSort}
                                rowCount={data.length}
                            />
                            <TableBody>
                                {data.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map(n => {
                                    return (
                                        <TableRow
                                            hover
                                            tabIndex={-1}
                                            key={n.id}
                                        >

                                            <TableCell padding="default">
                                            {n.id}
                                            </TableCell>
                                            <TableCell padding="default">{n.cat_id}</TableCell>
                                            <TableCell  padding="default">{n.name}</TableCell>
                                            <TableCell  padding="default">{n.category}</TableCell>
                                            <TableCell  padding="default">{n.prod_cnt}</TableCell>
                                            <TableCell  padding="default"><img src={n.image} width="25px" height="25px"></img></TableCell>
                                            <TableCell  padding="default">{n.status==true?'Deactivated':'Active'}</TableCell>
                                            <TableCell  padding="default">  
                                                    <Link to={`/app/sub_category/add?action=edit&id=${n.c_id}`} params={{ id: n.c_id,action:"edit" }}> <IconButton className="size-30" >
                                                        <i className="zmdi custom_icon_size zmdi-edit" /></IconButton></Link>
                                                        &nbsp;
                                                        <IconButton className="size-30" onClick = {()=>this.DisableSubCategory(n)} >
                                                        <i className="zmdi custom_icon_size zmdi-lock" /></IconButton>

                                                    </TableCell>
                                            
                                        </TableRow>
                                    );
                                })}
                            </TableBody>
                            <TableFooter>
                                <TableRow>
                                    <TablePagination
                                        count={data.length}
                                        rowsPerPage={rowsPerPage}
                                        page={page}
                                        onChangePage={this.handleChangePage}
                                        onChangeRowsPerPage={this.handleChangeRowsPerPage}
                                    />
                                </TableRow>
                            </TableFooter>
                        </Table>
                    </div>
                </div>

            </Paper>
            <SweetAlert show={this.state.success} success title="Success!" onConfirm={this.onConfirm}>
                        Sub Category Status Updated
                    </SweetAlert>
            </div>
        );
    }
}

export default SubCategoryList;