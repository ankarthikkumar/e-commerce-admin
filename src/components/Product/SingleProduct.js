import React, { Component } from 'react';
import PropTypes from 'prop-types';
import AppBar from 'material-ui/AppBar';
import Tabs, { Tab } from 'material-ui/Tabs';
import CardBox from "components/CardBox/index";
import { getProductbyId } from '../../actions/Product';
function TabContainer(props) {
    console.log("props", props);
    return (
        <CardBox styleName="col-lg-12" childrenStyle="d-flex" >
            <div style={{ padding: 20 }}>
                {props.children}
            </div>
        </CardBox>
    );
}

TabContainer.propTypes = {
    children: PropTypes.node.isRequired,
};

class SingleProduct extends Component {
    state = {
        value: 0,
        data: [],
        category: [],
        subCategory: [],
        unit: [],
        productImage: []
    };

    handleChange = (event, value) => {
        console.log(value);
        this.setState({ value });
    };
    async componentDidMount() {
        const product = await getProductbyId(this.props.id);
        console.log("single product in component", product.data);
        this.setState({
            data: product.data,
            category: product.data.category,
            subCategory: product.data.subCategory,
            unit: product.data.unit,
            productImage: product.data.productImage
        });
        console.log("productImage", this.state.productImage)
    }


    render() {
        // alert();
        const { data, value, category, subCategory, unit, productImage } = this.state;

        return (

            <div>

                <AppBar className="bg-primary" position="static">
                    <Tabs value={value} onChange={this.handleChange} scrollable scrollButtons="on">
                        <Tab className="tab" label="General" />
                        <Tab className="tab" label="Pricing" />
                        <Tab className="tab" label="Inventry" />
                        <Tab className="tab" label="Image" />
                    </Tabs>
                </AppBar>
                {value === 0 &&
                    <TabContainer>
                        <h1>Product Name  : {data.prodName}</h1>
                        <hr/>
                        <div className="row">
                        <hr/>
                            <div className=" font-class   border1 col-md-12">
                                <h1>Category</h1>
                                <div>Category Name : {category.categoryName}</div>
                                <br />

                            </div>
                            <hr/>
                            <div className=" font-class  border1 col-md-12">
                                <h1> Sub Category</h1>
                                <div>Sub Category Name : {subCategory.subCategoryName}</div>
                                <br />
                            </div>
                            <hr/>
                            <div className=" font-class  border1 col-md-12">
                                <h1>Unit</h1>
                              
                                <div>Unit Name : {unit.unit_name}</div>  <br />
                                <div>Unit  : {unit.unit}</div>
                                <br />

                            </div>

                            <div className=" font-class  border1 col-md-12">
                                <h1>Product Description</h1>
                              
                                <br /><b> {data.prodDescription} </b>
                                <br />

                            </div>
                        </div>
                      

                    </TabContainer>}
                {value === 1 &&
                    <TabContainer>
                        <div className="row">
                            <div className="font-class  col-md-12">

                                <div>Regular Price : {data.prodPrice}</div>
                                <br />
                                <div>Sale Price : {data.salePrice}</div>
                                <br />

                                <div>Stock Status : {data.prodStockStatus}</div>
                                <br />
                                <div>Allow Backorders : {data.allowBackOrders=='1' && <b>Allow</b> }  {data.allowBackOrders=='2' && <b>Don't Allow</b> }  {data.allowBackOrders=='3' && <b>Allow but Notify Custom</b> }</div>
                                <br></br>
                                <div>Product Stock :  {data.prodStock=='1' && <b>In Stock</b> }  {data.prodStock=='2' && <b>Out of Stock</b> }  {data.prodStock=='3' && <b>On Loading</b> } </div>
                                <br />
                                <div>stock Threshold : {data.stockThreshold}</div>
                                <br />

                            </div>
                        </div>
                    </TabContainer>}
                {value === 2 &&
                    <TabContainer>
                        <div className="font-class  col-md-12">

                            <div>Weight : {data.weight} Kg</div>
                            <br />
                            <div>Length : {data.length} cm</div>
                            <br />

                            <div>Width: {data.width} cm</div>
                            <br />
                            <div>Height : {data.height} cm</div>
                            <br />

                            <div>Shipping Type : {data.shippingtype == 1 ? 'cost' : 'free'} </div>
                            <br />

                            <div>Shipping Cost : {data.shippingCharge} </div>
                            <br />

                        </div>
                    </TabContainer>}


                {value === 3 &&
                    <TabContainer>
                        <h1>Product Images</h1>
                        <div className="row">
                            {productImage.map((img) => {

                                return (
                                    <div className="col-md-4">
                                        <img className="single-product-image " src={img.image}></img>    <br />
                                        <br />
                                    </div>

                                )
                            })}
                        </div>
                    </TabContainer>}



            </div>

        );
    }
}

export default SingleProduct;