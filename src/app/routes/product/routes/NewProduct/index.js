import React from 'react';
import AddProduct from "components/Product/AddProduct";

import TextField from 'material-ui/TextField';
import Button from 'material-ui/Button';
import Switch from 'material-ui/Switch';
class NewProduct extends React.Component {

    onOptionMenuSelect = event => {
        this.setState({ menuState: true, anchorEl: event.currentTarget });
    };
    handleRequestClose = () => {
        this.setState({ menuState: false });
    };

    constructor() {
        super();
        this.state = {
            anchorEl: undefined,
            menuState: false,
        }
    }

    render() {
        const { anchorEl, menuState } = this.state;
        return (
            <div style={{width: '100%', height: '100%'}}>
            <AddProduct props={this.props}  />
            </div>
                )
    }
}
export default NewProduct;